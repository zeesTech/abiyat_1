$(document).ready(function () {
    $('#is_free').on('change', function () {
        console.log($(this).val());
        if ($(this).val() == 0) {
            $('#delivery_cost').fadeIn();
        } else {
            $('#delivery_cost').fadeOut();
        }
    });
    $('.select2').select2({
        placeholder: 'Select'
    });
    $('.table').DataTable({
        'info' : false,
        'paging' : false,
        'searching' : false,
        'columnDefs' : [
            {
                'orderable': false, 'targets' : -1
            }
        ],
        'sorting' : []
    });



  $('#myaccclick').click(function () {
    // Using an if statement to check the class
    if ($("#myacc").hasClass('hidme')) {
      // The box that we clicked has a class of bad so let's remove it and add the good class
     $("#myacc").removeClass('hidme');
     
    } else {
      // The user obviously can't follow instructions so let's alert them of what is supposed to happen next
    //   alert("You can proceed!");
    }
  });


});

