@extends('layouts.public.app')
<style>
.new-address-info{
    margin-bottom: 100px;

}
.new-address-msg{
    margin-top: 60px;
}

.collection{
    background: rgb(255, 255, 255);
    border: 1px solid rgb(226, 229, 241);
    box-shadow: rgb(0 0 0 / 4%) 0px 3px 2px 0px;
    border-radius: 2px;
    height: 100%!important;

 
}

.addressColumn{
        color: #404553;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    @media (max-width:767.98px){
        .addressColumn{
            flex-direction: row;
        }   
    }
.addressColumnLeft{
    /* margin-right: 40px; */
    color: rgb(126, 133, 155);
}

.address-heading{
    border-bottom: 1px solid rgb(226, 229, 241);
    padding: 10px 15px 9px;
    background: rgb(252, 251, 244);
    font-weight: bold;
    font-size: 1.08333rem;
}
.boxHeader{
    font-size: 1.08333rem;
    color: #404553;

}
.collator-body{
    padding: 14px 15px 16px;

}

.boxHeader-trash{
    margin-top: -27px;
    font-size: 1.08333rem;
    color: #404553;

}
.addNewAddress.jsx-1676968614 {
    background: rgb(255, 255, 255);
    border: 1px solid rgb(226, 229, 241);
    padding: 14px 15px 16px;
    box-shadow: rgb(0 0 0 / 4%) 0px 3px 2px 0px;
    border-radius: 2px;
    text-align: center;
    font-size: 1.16666rem;
    width: 100%;
    box-sizing: border-box;
    height: 95%;

}



.address-box {
    background-color: rgb(255, 255, 255);
    padding: 25px;
    /* padding: 50px 0 0 0; */
    border-radius: 2px;
    border: 1px solid rgb(226, 229, 241);
    text-align: center;
    display:none;
}
.location-div{
	    border: 1px solid rgb(226, 229, 241);
    margin: 20px 0px;
    border-radius: 5px;
        padding: 24px;

}
.information-div{
	 border: 1px solid rgb(226, 229, 241);
    margin: 20px 0px;
    border-radius: 5px;
        padding: 24px;
}
.form-bottom-border{
	    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
}
#billing_addressw{
    display:none;
}
.selected-shipping{
    border: 2px solid #005e00;
}
.shipping-lables{
    width:100%;
    cursor: pointer;

}
.verificationStatus{
    float:right;
}
.payment-card-box {
    background-color: rgb(255, 255, 255);
    padding: 25px;
    border-radius: 2px;
    border: 1px solid rgb(226, 229, 241);
        text-align: left;

}

.payment-methods .payment-card-box{
    color: #005e00;
        text-align: center !important;

 }

 .card-input{
 	    background-color: rgb(255, 255, 255);
    padding: 25px;
    border-radius: 2px;
    border: 1px solid rgb(226, 229, 241);
    text-align: left;
 }
.payment-methods{
    color: #005e00;

 }
 .ship-to{
 	    background-color: #f7f7fa;
    padding: 8px 24px 8px;
    font-size: 13px;
    margin-top: 10px;
    margin-bottom: 11px;
    color: darkgray;
 }
 .special-h5{
 	font-weight: bold;
    letter-spacing: 0.21px;
 }
 .delete-address{
    background: transparent !important;
    border: transparent !important;
    color: #dc3545 !important;
 }
 .relocate-me
 {
    background-color: #217111 !important;
    color: #ffffff !important;
    border-color: transparent #217111 transparent transparent;
    padding: 4px 15px 4px 15px;
    border-radius: 30px;
 }
</style>
@section('content')
<br><br>
<div class="container">
@include('front.checkout.partials.steps')

<div style="text-align: center;padding-top: 70px; display:none;" id="loading-icon">

<img src="{{asset('loading.gif')}}" >

</div>

    <div class="product-in-cart-list">
   
  
            <div class="row">
         
                <div class="col-md-12 content">
                    <div class="box-body">
                        @include('layouts.errors-and-messages')
                    </div>
                    @if(count($addresses) > 0)
                       
                        <h4 id="shipping-heading"><b>Shipping Address</b></h4>
                        @include('front.checkout.partials.shipping-address')

   
                    
                    @else


                        @include('front.checkout.partials.no-shipping')


                    @endif
                </div>
            </div>
    

        @include('front.checkout.partials.address-form')

       
    </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">

        
        function selectShipTo(addressId, status)
        {
            $('.shipping-lables').removeClass('selected-shipping');
            $('#select-ship-to-'+addressId).addClass('selected-shipping');
            $('#shipping-verification').val(addressId);

        }

        function addressMapForm(){
            $('#addNewAddress').hide();
            $('#addressMapForm').show();
             initMap()

            
        }


        function setTotal(total, shippingCost) {
            let computed = +shippingCost + parseFloat(total);
            $('#total').html(computed.toFixed(2));
        }

        function setShippingFee(cost) {
            el = '#shippingFee';
            $(el).html(cost);
            $('#shippingFeeC').val(cost);
        }

        function setCourierDetails(courierId) {
            $('.courier_id').val(courierId);
        }


        $("#add-new-address").click(function(){
            $('#addNewAddress').hide();
            $('#all-address').hide();
            $('#loading-icon').show()
            $('#continue-to-payment').hide()
            setTimeout(
                function() 
                {
                    $('#addressMapForm').show();  
                    $('#loading-icon').hide()
                                 initMap()

                }, 1000);
           
        });




       

    // $("#address_1").bind('blur keyup',function(e) {  
     //     if (e.type === 'blur' || e.keyCode === 13)  {
     //     var address1 = $(this).val();
     //       $("#showAddressText").text(address1);
     //       $("#addressMap").hide();
     //       $("#addressBox").show();
     //     }
    // }); 


    function confirm_location()
    {
                  var address1 = $('#address_1').val();
                  if(!address1)
                  {
                      alert('select location')
                  }else{
                       $("#showAddressText").text(address1);
                        $("#addressMap").hide();
                        $("#addressBox").show();
                  }

    }

    $(document).ready(function () {
          
            let clicked = false;

            $('#sameDeliveryAddress').on('change', function () {
                clicked = !clicked;
                if (clicked) {
                    $('#sameDeliveryAddressRow').show();
                } else {
                    $('#sameDeliveryAddressRow').hide();
                }
            });

            let billingAddress = 'input[name="billing_addressw"]';
            $(billingAddress).on('change', function () {
                let chosenAddressId = $(this).val();
                $('.address_id').val(chosenAddressId);
                $('.delivery_address_id').val(chosenAddressId);
            });

            let deliveryAddress = 'input[name="delivery_address"]';
            $(deliveryAddress).on('change', function () {
                let chosenDeliveryAddressId = $(this).val();
                $('.delivery_address_id').val(chosenDeliveryAddressId);
            });

            let courier = 'input[name="courier"]';
            $(courier).on('change', function () {
                let shippingCost = $(this).data('cost');
                let total = $('#total').data('total');

                setCourierDetails($(this).val());
                setShippingFee(shippingCost);
                setTotal(total, shippingCost);
            });

            if ($(courier).is(':checked')) {
                let shippingCost = $(courier + ':checked').data('cost');
                let courierId = $(courier + ':checked').val();
                let total = $('#total').data('total');

                setShippingFee(shippingCost);
                setCourierDetails(courierId);
                setTotal(total, shippingCost);
            }

             
    });
   
    var map;
    var geocoder;   

    function initMap() {
        var input = document.getElementById('address_1');
        const autocomplete =  new google.maps.places.Autocomplete(input);
        
        var mapOptions = { center: new google.maps.LatLng(0.0, 0.0), zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP };
        var myOptions = {
                center: new google.maps.LatLng(36.835769, 10.247693 ),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
 
            geocoder = new google.maps.Geocoder();
            var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
                autocomplete.bindTo("bounds", map);
                const infowindow = new google.maps.InfoWindow();
                
                autocomplete.addListener("place_changed", () => {
                 //infowindow.close();
                    const marker = new google.maps.Marker({
                    map,
                    anchorPoint: new google.maps.Point(0, -29),
                    });
                    const place = autocomplete.getPlace();

                    if (!place.geometry || !place.geometry.location) {
                    
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                    }
                
                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                    } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                    }
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);
                
                    //infowindow.open(map, marker);
                });

                infoWindow = new google.maps.InfoWindow();
                 const locationButton = document.getElementById("locationButton");
        //   locationButton.textContent = "Pan to Current Location";
        //   locationButton.classList.add("custom-map-control-button");
        //   map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);

                locationButton.addEventListener("click", () => {
        //map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
                const marker = new google.maps.Marker({
                            map,
                            anchorPoint: new google.maps.Point(0, -29),
                        });


                        if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            infoWindow.setPosition(pos);
                            //infoWindow.setContent("Location found.");
                            marker.setPosition(pos);
                                marker.setVisible(true);
                            // infoWindow.open(map);
                            //map.setCenter(pos);

                            map.setCenter(pos);
                            map.setZoom(17);
                            placeMarker(pos);
                            //console.log(positionlatLng)
                            },
                            () => {
                            handleLocationError(true, infoWindow, map.getCenter());
                            }
                        );
                        } else {
                        // Browser doesn't support Geolocation
                        handleLocationError(false, infoWindow, map.getCenter());
                        }
                    });
                
            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
            });

            var marker;
            



        function placeMarker(location) {
            if(marker){ 
                marker.setPosition(location); 
            }else{
                marker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
            }
            //document.getElementById('lat').value=location.lat();
            //document.getElementById('lng').value=location.lng();
            
            getAddress(location);
            
        }

      function getAddress(latLng) {
        geocoder.geocode( {'latLng': latLng},
          function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
              if(results[0]) {
                document.getElementById("address_1").value = results[0].formatted_address;
              }
              else {
                document.getElementById("address_1").value = "No results";
              }
            }
            else {
              document.getElementById("address_1").value = status;
            }
          });
        }

        google.maps.event.addDomListener(window, 'load', initMap);
      }
      
    </script>
@endpush