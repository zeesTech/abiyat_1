@extends('layouts.public.app')
<style>

</style>
@section('content')
<br><br>
<div class="container">
@include('front.checkout.partials.steps')
<div style="text-align: center;padding-top: 70px; display:none;" id="loading-icon"></div>

<div class="container-fluid pt-3 pb-3 bg-lite1">
            	<div class="container">
            		<h4><b>Verify Phone Number</b></h4>
            		<div class="row">
            			<div class="col-md-12 mt-3">
            				<div class="phone-box">
            					<h5 class="p-2 m-0">Please verify your mobile number to continue</h5>
            					<p class="p-2 m-0">To proceed to checkout, use the OTP & verify your mobile number. We’ve sent the OTP to {{ $billingAddress->phone }}
            					</p>
								<form method="post" action="{{ route('verify-phone-code') }}" id="verify-phone-form" class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="off">
								@csrf
            					<div class="enterNumber">
            						<input type="text" id="digit-1" name="digit-1" data-next="digit-2" required>
            						<input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-2" required>
            						<input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-3" required>
            						<input type="text" id="digit-4" name="digit-4" data-previous="digit-4" required>
									<input type="hidden" value="{{ $billingAddress->id }}" name="addressId">
            						<button type="submit" class="btn btn-primary" id="code-submit" style="display:none;">Submit & Verify Phone</button>
									<div class="box no-border" id="error-msg" style="display:none;">
									<div class="box-tools">
										<p class="alert alert-danger alert-dismissible">
											Invalid OTP, Try again
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										</p>
									</div>
								</div>
            					</div>
								</form>
            					<hr class="ml-4 mr-4">
            					<div class="row">
            						<div class="col-md-6 text-center">
            							<a href="#" class="pl-4">Change Phone Number</a>
            						</div>
            						<div class="col-md-6">
            							<div class="text-center">
            								<p class="pr-4">Didn't receive OTP? Resend <a href="#">now</a></p>
            							</div>
            						</div>
            					</div>
            				</div>
            			</div>
            		</div>
            	</div>

            	
            </div>
            
@endsection

@push('scripts')
    <script type="text/javascript">


$('.digit-group').find('input').each(function() {
	$(this).attr('maxlength', 1);
	$(this).on('keyup', function(e) {
		  $('#error-msg').hide()
		var parent = $($(this).parent());
		
		if(e.keyCode === 8 || e.keyCode === 37) {
			var prev = parent.find('input#' + $(this).data('previous'));
			
			if(prev.length) {
				$(prev).select();
				$('#code-submit').hide()
			}
		} else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
			var next = parent.find('input#' + $(this).data('next'));
			
			if(next.length) {
				$(next).select();
				$('#code-submit').hide()
			} else {
				$('#code-submit').show(200)
			}
		}
	});
});



$('#verify-phone-form').submit(function(event) {
    event.preventDefault(); // Prevent the form from submitting via the browser
    var form = $(this);
    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize()
    }).done(function(data) {
      // Optionally alert the user of success here...
	  if(data.iscorrect == "no")
	  {
		  $('#error-msg').show()
		  $('#code-submit').hide()
	  }else{
		  window.location.reload()
	  }
    }).fail(function(data) {
      // Optionally alert the user of an error here...
    });
  });
    </script>
@endpush