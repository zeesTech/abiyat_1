@extends('layouts.public.app')
<style>

</style>
@section('content')
<br><br>
<div class="container">
@include('front.checkout.partials.steps')
<br><br>
<div class="jumbotron text-center">
  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>Your Order was completed successfully</strong>  <hr>
  <p style="display:inline-flex"> 
    <i class="fa fa-envelope" style="font-size: 80px;"></i> <b style="    text-align: left;
    margin-left: 15px;
    margin-top: 20px;">An email receipt including the details about your order has been <br>sent to the email address provided. Please keep it for yours records. </b>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="{{ route('accounts', ['tab' => 'orders']) }}" style="    padding: 12px;
    font-size: 20px;
    font-weight: 600;">Go to Orders</a>
  </p>
</div>

@endsection