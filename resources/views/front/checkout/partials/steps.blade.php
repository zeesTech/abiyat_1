<div class="row">
            		<div class="col-md-12">
            			<div class="threeSteps">
            				<div class="siteWidthContainer text-center">
            					<ul class="steps">
            						<li class="step">

            							<span class="active">1</span>
            							<div class="text">Shipping Address</div>
            						</li>
            						<li class="step">
            							<span @if($step < 4 && $step > 1) class="active" @endif>2</span><div class="text">Payment</div>
            						</li>
            						<li class="step"><span @if($step == 3) class="active" @endif>3</span><div class="text">Order Placed</div></li>
            					</ul>
            				</div>
            			</div>
            		</div>
            	</div>