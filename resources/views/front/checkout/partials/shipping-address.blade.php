@if(isset($addresses))

<div class="row" id="all-address">
@foreach($addresses as $key => $address)

                                                      
<div class="col-md-3">
<label class="@if($billingAddress->id == $address->id) selected-shipping @endif shipping-lables" id="select-ship-to-{{$address->id}}">
                <input type="radio" value="{{ $address->id }}" name="billing_addressw" id="billing_addressw" onclick="selectShipTo({{$address->id}}, {{$address->phone_verified}})"  @if($billingAddress->id == $address->id) checked="checked"  @endif>
<div class="card-box">

    <div class="row">
<div class="col-md-12">
        <div class=" innerContainer collection">

    <div class=" addressDetailsContainer">
        <div class="address-heading">
        <h2 class=" boxHeader"> Address# {{$key+1}}</h2> 
      

        <form method="post" action="{{ route('customer.address.destroy', [auth()->user()->id, $address->id]) }}" class="form-vertical" style="float: right;margin-top: -32px;">
                                 <div class="btn-group">
                                    <input type="hidden" name="_method" value="delete">
                                    {{ csrf_field() }}
                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger delete-address"> <i class="fa fa-trash"></i> </button>
                                 </div>
                              </form>
        
        
    </div>
<div class="collator-body">

        <div class=" addressRow"><span class=" addressColumn addressColumnLeft">Name:</span>
        </div>
        <div class=" addressRow">
        <span class=" addressColumn fullName" id="name-{{$address->id}}">{{ $address->alias }}</span>
        </div>
        <div class=" addressRow">
        <span class=" addressColumn addressColumnLeft">Address:</span>
        </div>
        <div class=" addressRow">
            <span class=" addressColumn fullAddress" id="address-{{$address->id}}">{{ $address->address_1 }}</span>
        </div>
        <div class=" addressRow">
        <span class=" addressColumn addressColumnLeft" >Phone Number</span>
        </div>

            <div class=" addressColumn phoneNoWrapper verifiedLabel"><span class="phoneNumber" id="phone-{{$address->id}}">{{ $address->phone }}</span>
               
               @if($address->phone_verified == 0)
                <div class=" verificationStatus"><span class="badge badge-danger">Not Verified</span>
                </div>
                @else

                <div class=" verificationStatus"><span class="badge badge-success">Verified</span>
                </div>
                @endif
            </div>
        </div>
    </div>
    </div>
</div>
</div>                                
    </div>
    </label>
</div>

@endforeach
<div class="col-md-3">
<button aria-label="Add a new address" class="jsx-1676968614 addNewAddress" id="add-new-address">Add a new address</button>
</div>

</div>
<div class="col-md-12 mt-2 p-0 d-flex justify-content-center">
<form action="" method="GET">
<input type="hidden" name="shipping-verification" id="shipping-verification" value="{{$billingAddress->id}}">
<button type="submit" class="btn btn-success mb-3 float-left" id="continue-to-payment">CONTINUE</button>
</form>

</div>
</div>

    
@endif