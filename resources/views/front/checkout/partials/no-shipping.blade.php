

         <div class="container-fluid pt-3 pb-3 bg-lite1" id="addNewAddress">
            	<div class="container">
            		
            		<div class="row">
            			<div class="col-md-12 text-center new-address-info">
            		
            			<img src="https://k.nooncdn.com/s/app/com/noon/images/account/addresses-empty.svg" >
            				
            	            <h4 class="new-address-msg">There are no addresses saved</h4>
            	            <p>Add an address now we can start cracking on delivery!</p>
                            <button type="button" class="btn btn-primary mb-3" onclick="addressMapForm()">ADD NEW ADDRESS</button>
                            
                  <p class="new-address-msg">Our checkout is safe and secure. Your personal and payment information is securely transmitted via 128-bit encryption. We do not store any payment card information on our systems</p>
                   
                    
                    
            				</div>
            			</div>
            		</div>
            	</div>

            	
            </div>