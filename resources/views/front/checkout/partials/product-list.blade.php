	@if(!empty($products) && !collect($products)->isEmpty())
    <div class="row">
            		
            			<div class="col-md-8">
            				<div class="payment-card-box">
            				        @foreach($products as $product)

            					<div class="row">
            						<div class="col-md-1">
                                     @if(isset($product->cover))
                                        <img src="{{ asset("$product->cover") }}" alt="{{ $product->name }}"     width="40" class="img-bordered img-responsive">
                                    @else
                                        <img src="https://placehold.it/263x330" alt="{{ $product->name }}" width="40" class="img-bordered img-responsive" />
                                    @endif
                                    
            						</div>
                                    <div class="col-md-9">
                                    {{ $product->name }}
            						</div>
                                    <div class="col-md-2">
                                    <b>{{ config('cart.currency') }} {{ number_format($product->price, 2) }}</b>
                                    
                                    <br>
                                    QTY {{ $product->qty }}
            						</div>
                                     
            						
            					</div>
                                @endforeach
            				</div>
            			
            			</div>
                        </div>
            	
            </div>
  @endif