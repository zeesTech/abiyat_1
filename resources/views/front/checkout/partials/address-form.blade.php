<div class="container-fluid pt-3 pb-3 bg-lite1" id="addressMapForm" style="display:none">
            	<div class="container">
            		<div class="row mb-3">
            			<div class="col-md-12">
	            			<a href="" class="back_btn">
	            				<i class="fas fa-chevron-left"></i> Back to Addresses
	            			</a>
							<a href="javascript:void(0)" class="back_btn float-right relocate-me" id="locationButton">
	            				<i class="fa fa-map-marker"></i> Relocate me
	            			</a>
	            		</div>
            		</div>
                    <form action="{{ route('customer.address.store', $customer->id) }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="status" value="1">

            		<div class="row">
                    {{ csrf_field() }}
            			<div class="col-md-12 EditAddress" id="addressMap">
						    <div id="map_canvas"style="height: 500px;"></div>

            				 <div class="search-location">
            				 	<div class="form-group">
								    <input type="text" class="form-control" id="address_1" name="address_1" aria-describedby="emailHelp" placeholder="Unnamed Road - International City - Dubai" required="">
								    
								  </div>
            				 </div>
            				 <div class="name w-100 float-left pt-2">
            				 	<button type="button" class="btn btn-primary float-left" onclick="confirm_location()">Confirm location</button>
            				 </div>
            			</div>
            		</div>

                    <div class="address-box" id="addressBox">
            					<div class="row">
            						<div class="col-md-6 text-left ">
            							<h4>Location Information</h4>
            							 <p class="pr-4 location-div" id="showAddressText"></p>

            						</div>
            						<div class="col-md-6 text-left ">
            							<h4>Personal Information</h4>
									  <div class="form-group">
									    <input type="text" class="form-control form-bottom-border" id="phone" name="phone" placeholder="Phone Number">
									  </div>
									  <div class="form-group">
									    <input type="text" class="form-control form-bottom-border" id="first" name="alias" placeholder="Full Name">
									  </div>
									

									   <div class="form-group ">
								 <button type="submit" class="btn btn-primary mb-3 float-right">SAVE ADDRESS</button>

									  </div>
            						</div>
            					</div>
            				</div>
                  
            	</div>
                </form>
            </div>