@extends('layouts.public.app')
<style>

.verificationStatus{
    float:right;
}
.payment-card-box {
    background-color: rgb(255, 255, 255);
    padding: 25px;
    border-radius: 2px;
    border: 1px solid rgb(226, 229, 241);
        text-align: left;

}

.payment-methods .payment-card-box{
    color: #005e00;
        text-align: center !important;

 }

 .card-input{
 	    background-color: rgb(255, 255, 255);
    padding: 25px;
    border-radius: 2px;
    border: 1px solid rgb(226, 229, 241);
    text-align: left;
 }
.payment-methods{
    color: #005e00;

 }
 .ship-to{
 	    background-color: #f7f7fa;
    padding: 8px 24px 8px;
    font-size: 13px;
    margin-top: 10px;
    margin-bottom: 11px;
    color: darkgray;
 }
 .special-h5{
 	font-weight: bold;
    letter-spacing: 0.21px;
 }



 .selected-card{
    border: 2px solid #005e00;
}
.payment-card-box form img {
    width: 100%;
}
</style>
@section('content')
<br><br>
<div class="container">
@include('front.checkout.partials.steps')

<div class="container-fluid pt-3 pb-3 pl-0 pr-0 bg-lite1" id="payment-grid" >
            	<div class="container">
            		<h2><b>Payment</b></h2>
            		<p><b>PAYMENT METHOD</b></p>
            		<div class="row">
            			<div class="col-md-8">
            				<div class="row">
            		
            			<div class="col-md-6">
            				<a href="javascript:void(0)" class="payment-methods" onclick="selectMethod('payment-card')">
            				<div class="payment-card-box selected-card" id="payment-card">
            				
            					<div class="row">
            						<div class="col-md-12">
            							<i class="fa fa-credit-card" aria-hidden="true"></i> Pay with card
            						</div>
            						
            					</div>
            				</div>
            				</a>
            			</div>
            				<div class="col-md-6">
            					<a href="javascript:void(0)" class=" payment-methods" onclick="selectMethod('payment-cash')">
            				<div class="payment-card-box" id="payment-cash" >
            				
            					<div class="row">
            						<div class="col-md-12">
            							<i class="fa fa-money" aria-hidden="true"></i> Pay with cash
            						</div>
            						
            					</div>
            				</div>
            				</a>
            			</div>
            		</div>
            		<br>
            		<div class="payment-card-box" >

                            <div id="credit-card">
 
						  <div class="form-group">
						    <label for="pwd">Card Number:</label>
						    <input type="text" class="form-control card-input" id="card_no" minlength="16" maxlength="16" placeholder="0125 6780 4567 9909">
						  </div>
						  <div class="form-group">
						      <div class="row">
						         <div class="col-6"><label>Expiry date:</label> <input class="form-control card-input" placeholder="YY/MM"> </div>
						         <div class="col-6"><label>CVV:</label> <input id="cvv" class="form-control card-input" placeholder="Code"> </div>
						        </div> 
						  </div>

 
                    </div>
                    
                    <form action="{{ route('checkout.cod') }}" method="post" style="display:none;" id="on-cash" class="text-center">
                                                @csrf
                                                <div >
                                                <img src="{{asset('public_assets/cod-delivery-services.png')}}" >
                                                
                                                </div>
        <br>
                                                <input type="hidden" name="" class="delivery_address_id">
                                                <input type="hidden" name="billing_address" class="address_id" value="{{ $billingAddress->id }}">
                                                <input type="hidden" name="courier_id" class="courier_id">
                                                <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-dollar" aria-hidden="true"> Cash on Delivery </i></button>
                            </form>
            				</div>
            			</div>
            			<div class="col-md-4 mt-3">
            				<div class="payment-card-box">
            					<h5 class="special-h5">ORDER SUMMARY</h5>
            					<br>
            				    <div class="row lower">
                        <div class="col text-left">Subtotal</div>
                        <div class="col text-right"><span>BDT</span> {{$subtotal}}</div>
                    </div>
                    <div class="row lower">
                        <div class="col text-left">Delivery</div>
                        <div class="col text-right">Free</div>
                    </div>
                    <hr>
                    <div class="row lower">
                        <div class="col text-left"><b>Total to pay</b></div>
                        <div class="col text-right"><b><span>BDT</span> {{$subtotal}}</b></div>
                    </div>
                   
                   	 <div class="row lower">
                        <div class="col text-left ship-to">	<b>SHIP TO</b>  <a href="{{ route('checkout.index') }}" class="float-right">change<a></div>
                    </div>
                    
                    <div class="collator-body">

        
                    <div class="addressRow">
                    <span class=" addressColumn fullName"><b id="showSelectedName">{{ $billingAddress->alias }}</b></span>
                    </div>
                
                    <div class="addressRow">
                        <span class=" addressColumn fullAddress" id="showSelectedAddress">{{ $billingAddress->address_1 }}</span>
                    </div>
                    <div class="addressRow">
                        <div class="addressColumn phoneNoWrapper verifiedLabel"><span class="phoneNumber" id="showSelectedPhone">{{ $billingAddress->phone }}</span>
                        </div>
                    </div>
            				</div>
            			</div>
            		</div>
            	</div>

                 @include('front.checkout.partials.product-list', compact('products'))

</div>
@endsection



@push('scripts')
    <script type="text/javascript">
function selectMethod(ptype){
$('.payment-card-box').removeClass('selected-card');
$('#'+ptype).addClass('selected-card');

    if(ptype == 'payment-card'){
    $('#credit-card').show()
    $('#on-cash').hide()
    }else{
        $('#credit-card').hide()
        $('#on-cash').show()
        
    }
}
           
 
    </script>
@endpush