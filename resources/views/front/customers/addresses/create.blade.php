<link href="{{ asset('css/hide_header.css') }}" rel="stylesheet">
@extends('layouts.public.app')
@section('content')
<style>
.relocate-me
 {
    background-color: #217111 !important;
    color: #ffffff !important;
    border-color: transparent #217111 transparent transparent;
    padding: 4px 15px 4px 15px;
    border-radius: 30px;
 }
</style>
<div class="container"> 

    <div class="row pt-3 pb-5 bg-light">
        <div class="col-md-3 d-none d-md-block">
            <nav>
                <h2 class="acc-prof-wlcm">Welcome, {{$customer->name}}</h2>
                <a href="{{ route('logout') }}" class="acc-sgnot">Sign Out</a>
                <div class="nav nav-tabs mayaccount mt-4" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-order-tab" href="{{ route('accounts', ['tab' => 'orders']) }}"><span><i class="las la-clipboard-list"></i></span>Orders</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start active" id="nav-address-tab" href="{{ route('accounts', ['tab' => 'addresses']) }}"><span><i class="las la-map-marked-alt"></i></span>Addresses</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-profile-tab" href="{{ route('accounts', ['tab' => 'payment']) }}"><span><i class="las la-credit-card"></i></span>Payments</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-credit-tab"  href="{{ route('accounts', ['tab' => 'credits']) }}"><span><i class="las la-coins"></i></span>Abiyat Credits</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-returns-tab" href="{{ route('accounts', ['tab' => 'returns']) }}"><span><i class="las la-undo-alt"></i></span>Returns</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-profile-tab" href="{{ route('accounts', ['tab' => 'claims']) }}"><span><i class="las la-user-cog"></i></span>Claims</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-contact-tab" href="{{ route('accounts', ['tab' => 'preferences']) }}"><span><i class="las la-id-card-alt"></i></span>Preferences</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-home-tab" href="{{ route('accounts', ['tab' => 'profile']) }}"><span><i class="las la-user"></i></span> Profile</a>
                </div>
            </nav>
        </div>
        <div class="col-md-9 p-0">
            <div class="tab-content p-0" id="nav-tabContent">
            <div class="tab-pane fade active show" id="Account-addresses" role="tabpanel" aria-labelledby="nav-address-tab">
                    
                    <div class="container" id="addressMapForm">
                    <a href="{{ route('accounts', ['tab' => 'addresses']) }}"><i class="fas fa-chevron-left"></i>Back to Addresses</a>
                    <a href="javascript:void(0)" class="back_btn float-right relocate-me" id="locationButton"><i class="fa fa-map-marker"></i> Relocate me</a>
                        <br>
                        <br>
                        <div class="main-body">

                            <div class="card">
                                <div class="card-header d-flex justify-content-between flex-wrap">
                                    <h6 class="mb-0">Add a New Address Set</h6>
                                    <div class="cntct-check mt-3 mt-sm-0">
                                        <input type="checkbox" class="subscribe-newsletter" id="subscribe" name="subscribe" value="Yes">
                                        <label for="subscribe" class="newsletter-label">Set as default address</label>
                                    </div>
                                </div>
                                <div class="card-body">
                                <form action="{{ route('customer.address.store', $customer->id) }}" method="post" class="form" enctype="multipart/form-data">
                                <input type="hidden" name="status" value="1">
                                <div class="row">
                                    {{ csrf_field() }}
                                    <div class="col-md-12 EditAddress" id="addressMap">
                                        <div id="map_canvas"style="height: 500px;"></div>

                                        <div class="search-location">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="address_1" name="address_1" aria-describedby="emailHelp" placeholder="Unnamed Road - International City - Dubai" required="">
                                                
                                            </div>
                                        </div>
                                        <div class="name w-100 float-left pt-2">
                                            <button type="button" class="btn btn-primary float-left" onclick="confirm_location()">Confirm location</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="addressBox" style="display:none;">
                                        <div class="col-12 col-md-6">
                                            <h6 class="mt-3"><strong>Location Information</strong></h6>
                                            <div class="d-flex align-items-center justify-content-between mt-3 p-2 border rounded">
                                                <div>
                                                    <span class=" d-flex align-items-center">
                                                        <i class="las la-map-marked-alt"></i>
                                                        <span>Set from map</span>
                                                    </span>
                                                    <h6 class="mt-2"><strong><span id="showAddressText"></span></strong></h6>
                                                </div>
                                                <a href="#">
                                                    <img src="public_assets/images/address-map.png" class="account-address-map" alt="">
                                                    <button type="button" class="btn btn-secondary btn-sm d-flex justify-content-center w-100">Edit</button>
                                                </a>
                                            </div>
                                            <div class="mt-5">
                                                <div class="form-group mt-5">
                                                    <label for="firstName">Additional Address Details <span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="address_2" name="address_2" value placeholder="Additional Address Details" required="">
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <h6>Address Label <br> (Optional)</h6>
                                                <div class="d-flex align-items-center">
                                                    <div class="custom-control custom-radio mr-3">
                                                        <input type="radio" class="custom-control-input" id="checkHome" name="defaultExampleRadios">
                                                        <label class="custom-control-label" for="checkHome">Home</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="checkWork" name="defaultExampleRadios">
                                                        <label class="custom-control-label" for="checkWork">Work</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 mt-5 mt-md-0">
                                            <h6 class="mt-3"><strong>Personal Information</strong></h6>
                                            <div class="mt-5">
                                                <div class="form-group m-0">
                                                    <label for="firstName">Mobile Number <span class="text-danger">*</span></label>
                                                    <input type="tel" class="contct-select" name="phone" id="phone" value="{{ old('phone') }}" placeholder="+966-51- 0xx-xxx-xxxx" required="">
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="form-group m-0">
                                                    <label for="firstName">Name<span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="alias" name="alias" value placeholder="Name" required="">
                                                </div>
                                            </div>
                                            <!-- <div class="mt-1">
                                                <div class="form-group m-0">
                                                    <label for="firstName">Last Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="aditionallast" name="aditional" value placeholder="Last" required="">
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary btn-lg mr-2">Save Address</button>
                                            <button type="button" class="btn btn-outline-secondary btn-lg">Back</button>
                                        </div>
                                   </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js') }}"></script>
    <script type="text/javascript">

        function findProvinceOrState(countryId) {
            $.ajax({
                url : '/api/v1/country/' + countryId + '/province',
                contentType: 'json',
                success: function (res) {
                    if (res.data.length > 0) {
                        let html = '<label for="province_id">Provinces </label>';
                        html += '<select name="province_id" id="province_id" class="form-control select2">';
                        $(res.data).each(function (idx, v) {
                            html += '<option value="'+ v.id+'">'+ v.name +'</option>';
                        });
                        html += '</select>';

                        $('#provinces').html(html).show();
                        $('.select2').select2();

                        findCity(countryId, 1);

                        $('#province_id').change(function () {
                            var provinceId = $(this).val();
                            findCity(countryId, provinceId);
                        });
                    } else {
                        $('#provinces').hide().html('');
                        $('#cities').hide().html('');
                    }
                }
            });
        }

        function findCity(countryId, provinceOrStateId) {
            $.ajax({
                url: '/api/v1/country/' + countryId + '/province/' + provinceOrStateId + '/city',
                contentType: 'json',
                success: function (data) {
                    let html = '<label for="city_id">City </label>';
                    html += '<select name="city_id" id="city_id" class="form-control select2">';
                    $(data.data).each(function (idx, v) {
                        html += '<option value="'+ v.id+'">'+ v.name +'</option>';
                    });
                    html += '</select>';

                    $('#cities').html(html).show();
                    $('.select2').select2();
                },
                errors: function (data) {
                    console.log(data);
                }
            });
        }
        
        function findUsStates() {
            $.ajax({
                url : '/country/' + countryId + '/state',
                contentType: 'json',
                success: function (res) {
                    if (res.data.length > 0) {
                        let html = '<label for="state_code">States </label>';
                        html += '<select name="state_code" id="state_code" class="form-control select2">';
                        $(res.data).each(function (idx, v) {
                            html += '<option value="'+ v.state_code+'">'+ v.state +'</option>';
                        });
                        html += '</select>';

                        $('#provinces').html(html).show();
                        $('.select2').select2();

                        findUsCities('AK');

                        $('#state_code').change(function () {
                            let state_code = $(this).val();
                            findUsCities(state_code);
                        });
                    } else {
                        $('#provinces').hide().html('');
                        $('#cities').hide().html('');
                    }
                }
            });
        }

        function findUsCities(state_code) {
            $.ajax({
                url : '/state/' + state_code + '/city',
                contentType: 'json',
                success: function (res) {
                    if (res.data.length > 0) {
                        let html = '<label for="city">City </label>';
                        html += '<select name="city" id="city" class="form-control select2">';
                        $(res.data).each(function (idx, v) {
                            html += '<option value="'+ v.name+'">'+ v.name +'</option>';
                        });
                        html += '</select>';

                         $('#cities').html(html).show();
                         $('.select2').select2();

                        $('#state_code').change(function () {
                            let state_code = $(this).val();
                            findUsCities(state_code);
                        });
                    } else {
                        $('#provinces').hide().html('');
                        $('#cities').hide().html('');
                    }
                }
            });
        }

        let countryId = +"{{ env('SHOP_COUNTRY_ID') }}";

        $(document).ready(function () {
            
            if (countryId === 226) {
                findUsStates(countryId);
            } else {
                findProvinceOrState(countryId);
            }

            $('#country_id').on('change', function () {
                countryId = +$(this).val();
                if (countryId === 226) {
                    findUsStates(countryId);
                } else {
                    findProvinceOrState(countryId);
                }

            });

            $('#city_id').on('change', function () {
                cityId = $(this).val();
                findProvinceOrState(countryId);
            });

            $('#province_id').on('change', function () {
                provinceId = $(this).val();
                findProvinceOrState(countryId);
            });
        });
        
    </script>
@endsection
