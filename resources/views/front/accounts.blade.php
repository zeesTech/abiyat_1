<link href="{{ asset('css/hide_header.css') }}" rel="stylesheet">
@extends('layouts.public.app')
@section('content')
<div class="container">

    <div class="row pt-3 pb-5 bg-light">
        <div class="col-md-3 d-none d-md-block">
            <nav>
                <h2 class="acc-prof-wlcm">Welcome, {{$customer->name}}</h2>
                <a href="{{ route('logout') }}" class="acc-sgnot">Sign Out</a>
                <div class="nav nav-tabs mayaccount mt-4" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'orders') active @endif" id="nav-order-tab" data-toggle="tab"
                        href="#Account-orders" role="tab" aria-controls="Account-orders" aria-selected="false"><span><i
                                class="las la-clipboard-list"></i></span>Orders</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'addresses') active @endif" id="nav-address-tab"
                        data-toggle="tab" href="#Account-addresses" role="tab" aria-controls="nav-profile"
                        aria-selected="false"><span><i class="las la-map-marked-alt"></i></span>Addresses</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'payment') active @endif" id="nav-profile-tab"
                        data-toggle="tab" href="#Account-payments" role="tab" aria-controls="nav-profile"
                        aria-selected="false"><span><i class="las la-credit-card"></i></span>Payments</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'credits') active @endif" id="nav-credit-tab" data-toggle="tab"
                        href="#Account-credits" role="tab" aria-controls="nav-profile" aria-selected="false"><span><i
                                class="las la-coins"></i></span>Abiyat Credits</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'returns') active @endif" id="nav-returns-tab"
                        data-toggle="tab" href="#Account-returns" role="tab" aria-controls="nav-profile"
                        aria-selected="false"><span><i class="las la-undo-alt"></i></span>Returns</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'claims') active @endif" id="nav-profile-tab"
                        data-toggle="tab" href="#Account-claims" role="tab" aria-controls="nav-profile"
                        aria-selected="false"><span><i class="las la-user-cog"></i></span>Claims</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'preferences') active @endif" id="nav-contact-tab"
                        data-toggle="tab" href="#Account-preferences" role="tab" aria-controls="nav-contact"
                        aria-selected="false"><span><i class="las la-id-card-alt"></i></span>Preferences</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start @if(request()->input('tab') == 'profile') active @endif" id="nav-home-tab"
                        data-toggle="tab" href="#Account-profile" role="tab" aria-controls="nav-home"
                        aria-selected="true"><span><i class="las la-user"></i></span> Profile</a>
                </div>
            </nav>
        </div>
        <div class="col-md-9 p-0">
            <div class="tab-content p-0" id="nav-tabContent">
                <div class="tab-pane fade @if(request()->input('tab') == 'profile') active show @endif" id="Account-profile" role="tabpanel"
                    aria-labelledby="nav-home-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Profile</h3>
                            <p>Manage your profile details</p>
                            @include('layouts.errors-and-messages')
                            <div class="card shadow-sm mb-4">
                                <div class="card-header">
                                    General Information
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('front.customers.update', $customer->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="inout-wraps">
                                            <div class="form-group">
                                                <label for="firstName">Name</label>
                                                <input type="text" class="contct-select" id="iname" name="name" value="{{$customer->name}}"
                                                    placeholder="First Name" required="">
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="firstName">Last Name</label>
                                                <input type="text" class="contct-select" id="iname" name="iname"
                                                    placeholder="Last Name" required="">
                                            </div> -->
                                            <div class="form-group">
                                                <label for="firstName">Email</label>
                                                <input type="email" class="contct-select" id="email" name="email" value="{{$customer->email}}"
                                                    placeholder="forexample@xyz.com" required=""
                                                    pattern="[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b">
                                            </div>
                                            <div class="form-group">
                                                <label for="firstName">Phone</label>
                                                <input type="text" class="contct-select" id="phone" name="phone" value="{{$customer->phone}}"
                                                    placeholder="forexample@xyz.com" required="">
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="firstName">Preferred language</label>
                                                <select class="contct-select custom-select" id="select" name="select">
                                                    <option disabled="" selected="">English</option>
                                                    <option>English</option>
                                                </select>
                                            </div> -->
                                        </div>
                                        <div class="cntct-btnwrap">
                                            <button type="submit" class="cntct-submit-btn">SAVE</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    Security
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('front.customers.update', $customer->id) }}" method="post">
                                    {{ csrf_field() }}
                                        <div class="inout-wraps secure">
                                            
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" class="contct-select" id="password"
                                                    name="password" placeholder="********" required="">
                                            </div>

                                        </div>
                                        <div class="cntct-btnwrap">
                                            <!-- Button trigger modal -->
                                            <a href="#" class="cntct-submit-btn" data-toggle="modal"
                                                data-target="#exampleModal">
                                                Change Password
                                            </a>

                                            <!-- Modal Popup-->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="form-group">
                                                                <h5 class="modal-title text-center"
                                                                    id="exampleModalLabel">Change Password</h5>
                                                                <p class="text-center">Enter your current password and
                                                                    new password to change the password</p>
                                                            </div>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <!-- <label for="firstName">Curent Password</label> -->
                                                                <input type="password" class="contct-select"
                                                                    id="password" name="password"
                                                                    placeholder="Curent Password" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <!-- <label for="firstName">New Password</label> -->
                                                                <input type="password" class="contct-select"
                                                                    id="password" name="password"
                                                                    placeholder="New Password" required="">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save
                                                                changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade @if(request()->input('tab') == 'orders') active show @endif" id="Account-orders" role="tabpanel" aria-labelledby="nav-order-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Orders</h3>
                            <p>See how your orders are managed and check the latest status on your order</p>

                            

                            @if(!$orders->isEmpty())
                            @foreach ($orders as $order)
                            <!-- If Orders placed Section start -->
                            <!-- card 1 -->
                            <div class="card mt-3 mb-3">    
                                <a href="{{ route('order.show',$order['id']) }}" class="card-header orders-card-header">
                                    <p>Orders <span>{{$order['id']}}</span><br><span>Placed on {{ date('M d, Y', strtotime($order['created_at'])) }}</span></p>
                                    
                                    <i class="fas fa-chevron-right" style="margin-top: 20px;"></i>
                                </a>
                                <div class="card-body">
                                @foreach ($order['products'] as $product)

                                    <!-- cart item open -->
                                    <div class="container bg-white">
                                        <div class="row border-bottom pb-3">
                                            <div class=" col-4 col-md-3 ">
                                                <img src="{{ asset("storage/".$product['cover']) }}"
                                                    class="img-fluid"
                                                    alt="Quasi nobis sunt et odit sint nihil quae laudantium.">
                                            </div>
                                            <div class="col-6 col-md-6 ">
                                                <div class="product_description">
                                                    <div class="company_name mt-3">{{$product['name']}}</div>
                                                    <div class="product_name">Quasi nobis sunt et odit sint nihil quae
                                                        laudantium.</div>
                                                </div>
                                                <div class="deliver-description">
                                                    <p class="order-status-text">{{$order['status']['name']}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- cart item close -->
                                    <!-- If Orders placed Section end -->
                                @endforeach
                                </div>
                            </div>
                            @endforeach
                            @else

                                <!-- If Orders Empty start -->
                            <div class="main-empty-cart coount">
                                <div class="empty-img">
                                    <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
                                </div>
                                <div class="empty-text">
                                    <h3>You don't have any orders yet</h3>
                                    <p>What are you waiting for? Start shopping!</p>
                                </div>
                                <a href="{{ route('home') }}" class="btn btn-primary btn-lg">Continue Shopping</a>
                            </div>
                            <!-- Orders Empty end -->
                            
                            @endif

                            
                        </div>
                    </div>
                </div>
                <!-- ///////////// -->
                <div class="tab-pane fade @if(request()->input('tab') == 'addresses') active show @endif" id="Account-addresses" role="tabpanel" aria-labelledby="nav-address-tab">
                    <div class="container">
                        <div class="main-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div>
                                    <h3>Addresses</h3>
                                    <p>Add, remove and select preferred addresses</p>
                                </div>
                                <a href="{{ route('customer.address.create', auth()->id()) }}" class="btn btn-primary">ADD A NEW ADDRESS</a>
                            </div>
                            <!-- Saved address open -->
                            <!-- <div class="card mt-3 mb-3">
                                <div class="card-header">
                                    <h6 class="mb-0">Primary Address</h6>
                                </div>
                                <div class="card-body">
                                   <div class="row border-bottom">
                                        <div class="col-12 mb-3">
                                           <div class="d-flex align-items-center">
                                                <h6 class="mb-0 mr-2"><strong>Home</strong></h6>
                                                <i class="las la-map-marked-alt"></i>
                                           </div>
                                        </div>
                                        <div class="col-12 col-sm-2 mt-5 mt-md-0">
                                            <span><strong>Name</strong></span>
                                            <p>Client Name</p>
                                        </div>
                                        <div class="col-12 col-sm-5 mt-5 mt-md-0">
                                            <span><strong>Address</strong></span>
                                            <p>PM59+M7 - Al Olaya - Ar-Riyad - Riyadh Province - 11564 Riyadh</p>
                                        </div>
                                        <div class="col-12 col-sm-5 mt-5 mt-md-0">
                                            <span><strong>Phone Number</strong></span>
                                            <p>043123432393945</p>
                                        </div>
                                   </div>
                                </div>
                            </div> -->

                            
                            <div class="card mt-3 mb-3">
                                <div class="card-header">
                                    <h6 class="mb-0">Other Adresses</h6>
                                </div>
                                <div class="card-body">
                                @if(!$addresses->isEmpty())
                                    @foreach($addresses as $address)

                                    
                                   <div class="row border-bottom mb-4">
                                        <div class="col-12">
                                           <div class="d-flex align-items-center">
                                                <h6 class="mb-0 mr-2"><strong>Home</strong></h6>
                                                <i class="las la-map-marked-alt"></i>
                                           </div>
                                        </div>
                                        <div class="col-12 col-sm-2 mt-5 mt-md-0">
                                            <span><strong>Name</strong></span>
                                            <p>{{$address['alias']}}</p>
                                        </div>
                                        <div class="col-12 col-sm-4 mt-5 mt-md-0">
                                            <span><strong>Address</strong></span>
                                            <p>{{$address['address_1']}}</p>
                                        </div>
                                        <div class="col-12 col-sm-2 mt-5 mt-md-0">
                                            <span><strong>Phone Number</strong></span>
                                            <p>{{$address['phone']}}</p>
                                        </div>
                                        <div class="col-12 col-sm-4">
                                        <form method="post" style="text-align:right;"
                                            action="{{ route('customer.address.destroy', [auth()->user()->id, $address->id]) }}"
                                            class="form-horizontal">
                                            <div class="btn-group">
                                                <input type="hidden" name="_method" value="delete">
                                                {{ csrf_field() }}
                                                <a href="{{ route('customer.address.edit', [auth()->user()->id, $address->id]) }}"
                                                    class="btn btn-primary mr-3 hide"> <i class="fa fa-pencil"></i> Edit</a>
                                                <button onclick="return confirm('Are you sure?')" type="submit"
                                                    class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                        </div> 
                                   </div>

                                   
                                    @endforeach
                                @endif
                                   
                                </div>
                            </div>



                            <!-- <div class="card">
                                <div class="card-header d-flex justify-content-between flex-wrap">
                                    <h6 class="mb-0">Add a New Address Set</h6>
                                    <div class="cntct-check mt-3 mt-sm-0">
                                        <input type="checkbox" class="subscribe-newsletter" id="subscribe" name="subscribe" value="Yes">
                                        <label for="subscribe" class="newsletter-label">Set as default address</label>
                                    </div>
                                </div>
                                <div class="card-body">
                                   <div class="row">
                                        <div class="col-12 col-md-6">
                                            <h6 class="mt-3"><strong>Location Information</strong></h6>
                                            <div class="d-flex align-items-center justify-content-between mt-3 p-2 border rounded">
                                                <div>
                                                    <span class=" d-flex align-items-center">
                                                        <i class="las la-map-marked-alt"></i>
                                                        <span>Set from map</span>
                                                    </span>
                                                    <h6 class="mt-2"><strong>PM59+M7 - Al Olaya - Ar-Riyad - Riyadh Province - 11564 Riyadh</strong></h6>
                                                </div>
                                                <a href="#">
                                                    <img src="public_assets/images/address-map.png" class="account-address-map" alt="">
                                                    <button type="button" class="btn btn-secondary btn-sm d-flex justify-content-center w-100">Edit</button>
                                                </a>
                                            </div>
                                            <div class="mt-5">
                                                <div class="form-group mt-5">
                                                    <label for="firstName">Additional Address Details <span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="aditional" name="aditional" value placeholder="Additional Address Details" required="">
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <h6>Address Label <br> (Optional)</h6>
                                                <div class="d-flex align-items-center">
                                                    <div class="custom-control custom-radio mr-3">
                                                        <input type="radio" class="custom-control-input" id="checkHome" name="defaultExampleRadios">
                                                        <label class="custom-control-label" for="checkHome">Home</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="checkWork" name="defaultExampleRadios">
                                                        <label class="custom-control-label" for="checkWork">Work</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 mt-5 mt-md-0">
                                            <h6 class="mt-3"><strong>Personal Information</strong></h6>
                                            <div class="mt-5">
                                                <div class="form-group m-0">
                                                    <label for="firstName">Mobile Number <span class="text-danger">*</span></label>
                                                    <input type="tel" class="contct-select" id="aditional" name="aditionalnumber" value placeholder="+966-51- 0xx-xxx-xxxx" required="">
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="form-group m-0">
                                                    <label for="firstName">First Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="aditionalfirst" name="aditional" value placeholder="First" required="">
                                                </div>
                                            </div>
                                            <div class="mt-1">
                                                <div class="form-group m-0">
                                                    <label for="firstName">Last Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="contct-select" id="aditionallast" name="aditional" value placeholder="Last" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="button" class="btn btn-primary btn-lg mr-2" disabled>Save Address</button>
                                            <button type="button" class="btn btn-outline-secondary btn-lg">Back</button>
                                        </div>
                                   </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if(request()->input('tab') == 'payments') active show @endif" id="Account-payments" role="tabpanel" aria-labelledby="nav-payments-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Payments</h3>
                            <p>Manage all your saved cards</p>
                            <div class="main-empty-cart coount">
                                <div class="empty-img">
                                    <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
                                </div>
                                <div class="empty-text">
                                    <h3>You don’t have any saved payment methods</h3>
                                    <p>And there in for the smoothest checkout experience!</p>
                                </div>
                                <button type="button" class="btn btn-primary btn-lg">Continue Shoping</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade @if(request()->input('tab') == 'credits') active show @endif" id="Account-credits" role="tabpanel" aria-labelledby="nav-credit-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Abiyat Credits</h3>


                        </div>
                    </div>
                </div>

                <div class="tab-pane fade @if(request()->input('tab') == 'returns') active show @endif" id="Account-returns" role="tabpanel" aria-labelledby="nav-returns-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Abiyat Reutrns</h3>


                        </div>
                    </div>
                </div>


                <div class="tab-pane fade @if(request()->input('tab') == 'claims') active show @endif" id="Account-claims" role="tabpanel" aria-labelledby="nav-claims-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Claims</h3>
                            <div class="main-empty-cart coount">
                                <div class="empty-img">
                                    <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
                                </div>
                                <div class="empty-text">
                                    <h3>We don't see any claims requested</h3>
                                    <p>Need to submit a claim request? Just click on the button below!</p>
                                </div>
                                <button type="button" class="btn btn-primary btn-lg">File A Claim Request</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if(request()->input('tab') == 'preferences') active show @endif" id="Account-preferences" role="tabpanel"
                    aria-labelledby="nav-Preferences-tab">
                    <div class="container">
                        <div class="main-body">
                            <h3>Preferences</h3>
                            <p>Choose your account's preferences to personalise your shopping experience</p>

                        </div>
                    </div>
                </div>

                <!-- deprecated section open 1 -->
                <div class="tab-pane fade " id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="container">
                        <div class="main-body">
                            <div class="row gutters-sm">
                                <div class="col-md-4 mb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-column align-items-center text-center">
                                                <img src="https://bootdey.com/img/Content/avatar/avatar7.png"
                                                    alt="Admin" class="rounded-circle" width="150">
                                                <div class="mt-3">
                                                    <h4>{{$customer->name}}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mt-3">
                                        <!-- <ul class="list-group list-group-flush">
                              <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe mr-2 icon-inline"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>Website</h6>
                                <span class="text-secondary">https:/abiyat.com</span>
                              </li>
                              
                              <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter mr-2 icon-inline text-info"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>Twitter</h6>
                                <span class="text-secondary">@zzz</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram mr-2 icon-inline text-danger"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg>Instagram</h6>
                                <span class="text-secondary">zzz</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook mr-2 icon-inline text-primary"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg>Facebook</h6>
                                <span class="text-secondary">zeee</span>
                              </li>
                              </ul> -->
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="card mb-3">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <h6 class="mb-0">Full Name</h6>
                                                </div>
                                                <div class="col-sm-9 text-secondary">
                                                    {{$customer->name}}
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <h6 class="mb-0">Email</h6>
                                                </div>
                                                <div class="col-sm-9 text-secondary">
                                                    {{$customer->email}}
                                                </div>
                                            </div>
                                            <!-- <hr>
                                 <div class="row">
                                   <div class="col-sm-3">
                                     <h6 class="mb-0">Phone</h6>
                                   </div>
                                   <div class="col-sm-9 text-secondary">
                                     (111) 222-333
                                   </div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                   <div class="col-sm-3">
                                     <h6 class="mb-0">Mobile</h6>
                                   </div>
                                   <div class="col-sm-9 text-secondary">
                                     (111) 222-333
                                   </div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                   <div class="col-sm-3">
                                     <h6 class="mb-0">Address</h6>
                                   </div>
                                   <div class="col-sm-9 text-secondary">
                                     Lahore
                                   </div>
                                 </div> -->
                                        </div>
                                    </div>
                                    <!-- <div class="row gutters-sm">
                           <div class="col-sm-12 mb-3">
                              <div class="card h-100">
                                 <div class="card-body">
                                    <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">Order</i> Status</h6>
                                    <small>Total Order</small>
                                    <div class="progress mb-3" style="height: 5px">
                                       <div class="progress-bar bg-primary" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <small>Pending Order</small>
                                    <div class="progress mb-3" style="height: 5px">
                                       <div class="progress-bar bg-info" role="progressbar" style="width: 72%" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <small>Cancel order</small>
                                    <div class="progress mb-3" style="height: 5px">
                                       <div class="progress-bar bg-danger" role="progressbar" style="width: 89%" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="table-responsive">
                        @if(!$orders->isEmpty())
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Date</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                <tr>
                                    <td>
                                        <a data-toggle="modal" data-target="#order_modal_{{$order['id']}}"
                                            title="Show order"
                                            href="javascript: void(0)">{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</a>
                                        <!-- Button trigger modal -->
                                        <!-- Modal -->
                                        <div class="modal fade" id="order_modal_{{$order['id']}}" tabindex="-1"
                                            role="dialog" aria-labelledby="MyOrders">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Reference
                                                            #{{$order['reference']}}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <thead>
                                                                <th>Address</th>
                                                                <th>Payment Method</th>
                                                                <th>Total</th>
                                                                <th>Status</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <address>
                                                                            <strong>{{$order['address']->alias}}</strong><br />
                                                                            {{$order['address']->address_1}}
                                                                            {{$order['address']->address_2}}<br>
                                                                        </address>
                                                                    </td>
                                                                    <td>{{$order['payment']}}</td>
                                                                    <td>{{ config('cart.currency_symbol') }}
                                                                        {{$order['total']}}</td>
                                                                    <td>
                                                                        <p class="text-center"
                                                                            style="color: #ffffff; background-color: {{ $order['status']->color }}">
                                                                            {{ $order['status']->name }}</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <hr>
                                                        <p>Order details:</p>
                                                        <table class="table">
                                                            <thead>
                                                                <th>Name</th>
                                                                <th>Quantity</th>
                                                                <th>Price</th>
                                                                <th></th>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($order['products'] as $product)
                                                                <tr>
                                                                    <td>{{$product['name']}}</td>
                                                                    <td>{{$product['pivot']['quantity']}}</td>
                                                                    <td>{{ config('cart.currency_symbol') }}
                                                                        {{$product['price']}}</td>
                                                                    <td>{{ config('cart.currency_symbol') }}
                                                                        {{$product['price']*$product['pivot']['quantity']}}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td><span
                                            class="label @if($order['total'] != $order['total_paid']) label-danger @else label-success @endif">{{ config('cart.currency') }}
                                            {{ $order['total'] }}</span></td>
                                    <td>
                                        <p class="text-center"
                                            style="color: #ffffff; background-color: {{ $order['status']->color }}">
                                            {{ $order['status']->name }}</p>
                                    </td>
                                </tr>
                                @endforeach
                                <!-- <tr>
                        <td>office</td>
                        <td>gulberg</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>PAKISTAN</td>
                        <td>54000</td>
                        <td>09000</td>
                        <td>
                           <form method="post" action="http://65.0.71.248/customer/14/address/10" class="form-horizontal">
                              <input type="hidden" name="_method" value="delete">
                              <input type="hidden" name="_token" value="a3y7IgUQq7ZVHFhTjVqvyfuA7z6xtmr1hD7RrRIz">
                              <a href="http://65.0.71.248/customer/14/address/10/edit" class="btn btn-primary btn-sm"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                           </form>
                        </td>
                     </tr> -->
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                        @else
                        <br />
                        <p class="alert alert-warning">No Orders.</p>
                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <div class="table-responsive">
                        <!-- new -->
                        @if(!$addresses->isEmpty())
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <th>Alias</th>
                                <th>Address 1</th>
                                <th>Address 2</th>
                                <th>City</th>
                                @if(isset($address->province))
                                <th>Province</th>
                                @endif
                                <th>State</th>
                                <th>Country</th>
                                <th>Zip</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach($addresses as $address)
                                <tr>
                                    <td>{{$address->alias}}</td>
                                    <td>{{$address->address_1}}</td>
                                    <td>{{$address->address_2}}</td>
                                    <td>{{$address->city}}</td>
                                    @if(isset($address->province))
                                    <td>{{$address->province->name}}</td>
                                    @endif
                                    <td>{{$address->state_code}}</td>
                                    <td></td>
                                    <td>{{$address->zip}}</td>
                                    <td>{{$address->phone}}</td>
                                    <td>
                                        <form method="post"
                                            action="{{ route('customer.address.destroy', [auth()->user()->id, $address->id]) }}"
                                            class="form-horizontal">
                                            <div class="btn-group">
                                                <input type="hidden" name="_method" value="delete">
                                                {{ csrf_field() }}
                                                <a href="{{ route('customer.address.edit', [auth()->user()->id, $address->id]) }}"
                                                    class="btn btn-primary"> <i class="fa fa-pencil"></i> Edit</a>
                                                <button onclick="return confirm('Are you sure?')" type="submit"
                                                    class="btn btn-danger"> <i class="fa fa-trash"></i> Delete</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <br />
                        <p class="alert alert-warning">No address created yet.</p>
                        @endif
                        <!-- new -->
                        <!-- <table class="table table-striped">
                        <thead class="thead-dark">
                           <tr>
                              <th>Alias</th>
                              <th>Address 1</th>
                              <th>Address 2</th>
                              <th>City</th>
                              <th>State</th>
                              <th>Country</th>
                              <th>Zip</th>
                              <th>Phone</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>office</td>
                              <td>gulberg</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>PAKISTAN</td>
                              <td>54000</td>
                              <td>09000</td>
                              <td>
                                 <form method="post" action="http://65.0.71.248/customer/14/address/10" class="form-horizontal">
                                       @csrf
                                    <a href="http://65.0.71.248/customer/14/address/10/edit" class="btn btn-primary btn-sm"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                                 </form>
                              </td>
                           </tr>
                           
                        </tbody>
                     </table> -->
                    </div>
                </div>
                <!-- deprecated section close 1 -->
            </div>
        </div>
    </div>

    @endsection