@extends('layouts.public.app')

@section('content')
<div class="container">
            	<div class="row">
            		<div class="col-md-12">
            			<div class="threeSteps">
            				<div class="siteWidthContainer">
            					<ul class="steps">
            						<li class="step">

            							<span class="active">1</span>
            							<div class="text">Shipping Address</div>
            						</li>
            						<li class="step">
            							<span>2</span><div class="text">Payment</div>
            						</li>
            						<li class="step"><span>3</span><div class="text">Order Placed</div></li>
            					</ul>
            				</div>
            			</div>
            		</div>
            	</div>
            	
</div>
            <div class="container-fluid p-0 pt-3 pb-3 bg-lite1">
            	<div class="container p-0">
            		<div class="row mb-3">
            			<div class="col-md-12">
	            			<a href="#" class="back_btn">
	            				<i class="fas fa-chevron-left"></i> Back to Addresses
	            			</a>
	            		</div>
            		</div>
            		<div class="row">
            			<div class="col-md-12 EditAddress p-0">
            				<div class="name p-2">Edit Address</div>
            				 <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" width="100%" height="400" frameborder="0" style="border:0"></iframe>
            				 <div class="search-location">
            				 	<div class="form-group">
								    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Unnamed Road - International City - Dubai">
								    
								  </div>
            				 </div>
            				 <div class="name w-100 float-left p-2">
            				 	<button class="btn btn-primary float-right">Confirm location</button>
            				 </div>
            			</div>
            		</div>
            	</div>
            	
            </div>
@endsection
@push('scripts')
    
@endpush