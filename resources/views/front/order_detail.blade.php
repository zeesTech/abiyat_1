<link href="{{ asset('css/hide_header.css') }}" rel="stylesheet">
@extends('layouts.public.app')
@section('content')
<div class="container">

    <div class="row pt-3 pb-5 bg-light">
        <div class="col-md-3 d-none d-md-block">
            <nav>  
                <h2 class="acc-prof-wlcm">Welcome, {{$customer->name}}</h2>
                <a href="{{ route('logout') }}" class="acc-sgnot">Sign Out</a>
                <div class="nav nav-tabs mayaccount mt-4" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link bg-light d-flex align-items-start active" id="nav-order-tab" href="{{ route('accounts', ['tab' => 'orders']) }}"><span><i class="las la-clipboard-list"></i></span>Orders</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-address-tab" href="{{ route('accounts', ['tab' => 'addresses']) }}"><span><i class="las la-map-marked-alt"></i></span>Addresses</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-profile-tab" href="{{ route('accounts', ['tab' => 'payment']) }}"><span><i class="las la-credit-card"></i></span>Payments</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-credit-tab"  href="{{ route('accounts', ['tab' => 'credits']) }}"><span><i class="las la-coins"></i></span>Abiyat Credits</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-returns-tab" href="{{ route('accounts', ['tab' => 'returns']) }}"><span><i class="las la-undo-alt"></i></span>Returns</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-profile-tab" href="{{ route('accounts', ['tab' => 'claims']) }}"><span><i class="las la-user-cog"></i></span>Claims</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-contact-tab" href="{{ route('accounts', ['tab' => 'preferences']) }}"><span><i class="las la-id-card-alt"></i></span>Preferences</a>
                    <a class="nav-item nav-link bg-light d-flex align-items-start" id="nav-home-tab" href="{{ route('accounts', ['tab' => 'profile']) }}"><span><i class="las la-user"></i></span> Profile</a>
                </div>
            </nav>
        </div>
        <div class="col-md-9 p-0">
            <div class="tab-content p-0" id="nav-tabContent">
               

                <div class="tab-pane fade active show" id="Account-orders" role="tabpanel" aria-labelledby="nav-order-tab">

                    <!-- Orders detials section start-->
                    <div class="container">
                        <a href="{{ route('accounts', ['tab' => 'orders']) }}"><i class="fas fa-chevron-left"></i>Back to Orders</a>

                        <!-- Shipping address details -->
                        <div class="card mt-3 mb-3">
                            <div class="card-header orders-card-header">
                                <h6><span>Order {{$order['id']}}</span> Placed on: {{ date('M d, Y', strtotime($order['created_at'])) }}</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class=" col-4 pl-4 pr-4">
                                                <div>
                                                    <p class="cart-summary-heading">Shipping Address</p>
                                                    <p> {{$order->address['address_1']}}
                                                        
                                                    </p>
                                                    <!-- <a href="#" class="mt-2">Change Address</a> -->
                                                </div>
                                            </div>
                                            <div class=" col-4 offset-md-2 pl-4 pr-4">
                                                <div>
                                                    <p class="cart-summary-heading">Mobile Number</p>
                                                    <p class="mb-0">{{$order->address['phone']}}</p>
                                                    <div class=" verificationStatus">
                                                    @if ($order->address['phone_verified'] == 1) 
                                                    <span
                                                            class="badge badge-success">Verified</span>
                                                            
                                                    @else
                                                    <span
                                                            class="badge badge-success">Note Verified</span>
                                                    @endif        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- MAin Order Detail -->
                        <div class="card mt-3 mb-3">
                            <div class="order-details-progress">
                                <span class="green"></span>
                                <span class="grey"></span>
                                <span class="grey"></span>
                                <span class="grey"></span>
                            </div>
                            <div class="card-body">
                                <div class="container bg-white">

                                @foreach ($items as $product)
                                    <div class="row ">
                                        <div class=" col-4 col-md-3 ">
                                            <img src="{{ asset("storage/".$product['cover']) }}"
                                                class="img-fluid" alt="Sequi quod inventore aliquam sunt consectetur.">
                                        </div>
                                        <div class="col-6 col-md-6 ">
                                            <div class="product_description">
                                                <div class="company_name mt-5"></div>
                                                <div class="product_name">{{$product['name']}}
                                                </div>
                                            </div>
                                            <div class="deliver-description">
                                                <p class="cart-deliver-message">Delivered by
                                                    <strong class="cart-deliver-date">Mon, Nov 16</strong><br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-3 form-inline">
                                            <div class="price-div form-inline ml-5 mr-5">
                                                <p class="cart-currency">{{config('cart.currency_symbol')}} </p>
                                                <p class="cart-price">@if($product['sale_price']) {{$product['sale_price']}} @else {{$product['price']}} @endif</p>
                                            </div>
                                            <div class="ml-5 pb-3">
                                                <p class="quantity"><span>{{ $product->pivot->quantity }}</span> unit</p>
                                            </div>
                                            <div class="ml-5 pb-3">
                                                <img src="public_assets/images/PRODUCT EX.png" class="image-tag-ordrs"
                                                    alt="">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>

                        <!-- Pay,ent Method -->
                        <div class="card mt-3 mb-3">
                            <div class="card-body">
                                <div>
                                        <div style="display:inline-block">
                                            <p class="cart-summary-heading">Payment Method</p>
                                        </div>
                                        <div style="display:inline-block; float:right;">
                                        <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-dollar" aria-hidden="true"> Cash on Delivery </i></button>
                                        </div>    
                                    </div>
                                </div>
                        </div>

                        <!-- Order Summry -->
                        <div class="card mt-3 mb-3">
                            <div class="card-body">
                                <div>
                                    <div>
                                        <p class="cart-summary-heading">Order Summary</p>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <div class=" form-inline">
                                            <p class="cart-subtotal">Subtotal</p>
                                            <p class="cart-subtotal-item">( <span>1</span>itmes )</p>
                                        </div>
                                        <div>
                                            <p class="cart-subtotal">{{config('cart.currency_symbol')}} {{$order['total']}}</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between border-bottom">
                                        <div class=" form-inline">
                                            <p class="cart-subtotal">Shipping</p>
                                        </div>
                                        <div>
                                            <p class="shipping-free">@if($order['total_shipping'] != '' && $order['total_shipping'] > 0.00) $order['total_shipping']  @else FREE @endif</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <div class=" form-inline">
                                            <p class="cart-total">Total</p>
                                            <p class="cart-total-inclusiv">( Inclusive of VAT )</p>
                                        </div>
                                        <div>
                                            <p class="cart-subtotal">{{config('cart.currency_symbol')}} {{$order['total']}}</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <div class=" form-inline">
                                            <p class="cart-total">VAT</p>
                                        </div>
                                        <div>
                                        @if($order['tax'] != '' && $order['tax'] > 0.00)
                                            <p class="cart-subtotal">BDT 1.00</p>
                                        @endif    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Orders detials section end-->
                </div>
                
            </div>
        </div>
    </div>
    <br>
    <br>
    @endsection