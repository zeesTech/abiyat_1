@extends('layouts.public.login')

@section('content')
<style>
    .super_container{
      height: 100%!important;
      overflow: scroll;
   }
</style>
<div class="logincontinaer">

    <div class="loginwrap">

        <div id="logo" class="text-center"><a href="{{route('home')}}" class="text-white">Abiyat</a></div>
        <h4 class="text-center login-main-text">Login</h4>
        <form action="{{ route('login') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <!-- <input type="hidden" name="_token" value="UtrTedQ0oaPpnzlfeirP5CQN3COorq5pDpEELF50"> -->
            <div class="form-group icon-login">
                <label for="email">Email/Phone</label>
                <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control"
                    placeholder="Email/Phone" autofocus>
                <i class="fa fa-envelope-o"></i>
            </div>
            <div class="form-group icon-login">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" value="" class="form-control" placeholder="*********">
                <i class="fa fa-lock"></i>
            </div>
            <div class="row">
                <div class="col-12">
                <a href="{{route('password.request')}}" class="passforgot">Forgot Password? </a><br>
                </div>
            </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                    <button class="btn btn-default btn-login" type="submit">Login</button>
                    </div>
                </div>
            
        </form>
        <div class="row">
            <div class="col-md-12 text-center">
                <span>Dont have account? <a href="{{route('register')}}" class="text-center"> <br> Register</a></span>
            </div>

        </div>
    </div>
</div>



@endsection