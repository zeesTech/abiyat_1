@extends('layouts.public.login')
@section('content')
<style>
   .hide{
   display: none !important;
   }
   .super_container{
      height: 100%!important;
      overflow: scroll;
   }
   .login-logo{
      width:60%;
   }
</style>
<div class="logincontinaer">
   <div class="loginwrap">
      <div id="logo" class="text-center"><a href="{{ route('home') }}" class="text-white" ><img src="{{ asset('public_assets/images/abiyat-png-mob.png')}}" class="login-logo" alt=""></a></div>
      <h4 class="text-center">Register</h4>
      <div class="form-group phoneF text-center" style="padding: 2px;"><a style="display: block;cursor: pointer;
         color: #007bff;" onclick="showFrom('phone')">Register with phone number</a></div>
      <div class="form-group emailF  hide" style="display: block;padding: 8px;"><a style="display: block;cursor: pointer;
         float: right;color: #007bff;" onclick="showFrom('email')">Register with email</a></div>
      
      <form class="form-horizontal emailForm " role="form" method="POST" action="{{ route('register') }}">
         {{ csrf_field() }}
         <input type="hidden" name="register_with" value="email" />
         <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
            @if ($errors->has('name'))
            <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
         </div>
         <div class=" icon-login form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
            <i class="fa fa-envelope-o"></i>
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
         </div>
         <div class="icon-login form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password" placeholder="********">
            <i class="fa fa-lock"></i>
            @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
         </div>
         <div class="icon-login form-group">
            <label for="password-confirm" class="control-label">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="********">
            <i class="fa fa-lock"></i>
         </div>
         <div class=".row">
            <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-login">
            Register
            </button>
            </div>
         </div>
      </form>
      
      <!-- phone -->
      <form class="form-horizontal phoneForm hide" role="form" method="POST" action="{{ route('register') }}">
         {{ csrf_field() }}
         <input type="hidden" name="register_with" value="phone" />
         <input type="hidden" id="sended_code" name="sended_code" value="" />
         <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
            @if ($errors->has('name'))
            <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
         </div>
         <div class=" icon-login form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="control-label">Phone</label>
            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
            <button style="font-size: 13px;
    top: 24px;
    position: absolute;
    line-height: 0;
    right: 5px;
    height: 30px;
    border-radius: 32%;" type="button" class="btn btn-info" id="reSend" onclick="sendCode()">Send</button>
            <!-- <i class="fa fa-envelope-o"></i> -->
            @if ($errors->has('phone'))
            <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
            </span>
            @endif
         </div>
         <div id="vCode" class="hide icon-login form-group{{ $errors->has('code') ? ' has-error' : '' }}">
            <label for="code" class="control-label">Code</label>
            <input id="code" type="text" onblur="matchCode()" class="form-control" name="code" require>
            <i class="fa fa-lock"></i>
            @if ($errors->has('code'))
            <span class="help-block">
            <strong>{{ $errors->first('code') }}</strong>
            </span>
            @endif
         </div>
         <div class="showP hide icon-login form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password">
            <i class="fa fa-lock"></i>
            @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
         </div>
         <div class="showP hide icon-login form-group">
            <label for="password-confirm" class="control-label">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            <i class="fa fa-lock"></i>
         </div>
         <div class="row">
            <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-login">
            Register
            </button>
            </div>
         </div>
      </form>
      <div class="row">
         <div class="col-md-12 text-center">
           <span class="text-center">Already have account? <a href="{{route('login')}}"  >Login</a></span>
         </div>
      </div>
   </div>
</div>
@endsection

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
   function showFrom(params) {
       if(params == "phone"){
           $(".emailForm").addClass('hide');
           $(".phoneForm").removeClass('hide');
           $(".phoneF").addClass('hide');
           $(".emailF").removeClass('hide');
       }else{
           $(".emailForm").removeClass('hide');
           $(".phoneForm").addClass('hide');
           $(".phoneF").removeClass('hide');
           $(".emailF").addClass('hide');
       }
       
   }
   function sendCode() {
      
      // return false;
      var ph = $('#phone').val();
      if($.trim(ph) == ""){
         return false;
         
      }
      var xmlhttp = new XMLHttpRequest();
      
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
           if (xmlhttp.status == 200) {
              $('#sended_code').val(xmlhttp.responseText);
              $('#vCode').removeClass('hide');
              $('.showP').removeClass('hide');
              $('#reSend').html('Re-send');
              $('#reSend').css('background', '#2f7d41');
            }
           else if (xmlhttp.status == 400) {
              
         }
         else {
            // alert('something else other than 200 was returned');
         }
      }
   };
   
   xmlhttp.open("get", "sendSMS?phone="+ph, true);
   xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   xmlhttp.send();
}
function matchCode() {
   var a =  $('#sended_code').val();
   var b = $('#code').val();
   if(a !== b){
      $('.showB').addClass('hide');
      //alert("Invalid Code!")
   }else{
      $('.showB').removeClass('hide');

   }
}
</script>