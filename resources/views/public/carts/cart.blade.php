@extends('layouts.public.app')
@section('content')
<style>
    .super_container{
        background-color: #E4EEE4;
    }
    .cart-heading{
        font-size: 18px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
        color: #000;
        margin-bottom: 0px;
    }
    .cart-deliver-to{
        color:blue;
        font-weight: 600;
        margin: 0px;
        margin-left: 2px;
    }
    .cart-heading-items{
        font-size: 16px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
        margin-bottom: 0px;
        margin-left: 5px;
    }
    .product_description {
        padding-left: 0px;

    }
    .company_name{
        font-size: 12px;
        font-family: 'Montserrat', sans-serif;
        line-height: 0px;
        margin-bottom: 5px;
    }
    .product_name {
        font-size: 13px;
        font-weight: 900;
        margin-top: 0px;
        font-family: 'Montserrat', sans-serif;
    }
    .cart-deliver-date{
        font-size: 10px;
        font-family: 'Montserrat', sans-serif;

    }
    .cart-deliver-message{
        margin-top: 2%;
        font-size: 10px;
        line-height:10px
    }
    .deliver-description{
        padding-left: 0px;
    }
    .cart-iten-delete{
        font-size: 12px;
        font-family: 'Montserrat', sans-serif;
        line-height: 0px;
        margin-top: 10px;
    }
    .price-div{
        /* margin-top: 40%; */
    }
    .cart-currency{
        font-size: 14px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
        margin-right: 4px;
    }
    .cart-price{
        font-size: 16px;
        font-family: 'Montserrat', sans-serif;
        color: #000;
        font-weight: 400;
        line-height: 2px;

    }
    .price-time{
        font-size: 10px;
        padding-bottom: 0%;
        margin-bottom: 0%;
        font-family: 'Montserrat', sans-serif;
    }
    .quantity{
    font-size: 12px;
    padding-bottom: 0%;
    margin-bottom: 0%;
    font-family: 'Montserrat', sans-serif;
    }
    .item-counter{
        max-width: 100px;
        height: 40px;
        padding-left: 10px;
    }
        .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.40rem
    }
    .card {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: 0px
    }
    .coupon {
     border-radius: 1px;
     font-family: 'Montserrat', sans-serif;
     /* background-color: #005e00; */
    }
    .coupon-btn{
        background-color: #005e00;
        color: #fff;
    }
   
    .cart-subtotal{
        font-size: 14px;
        font-family: 'Montserrat', sans-serif;
        color: #000;
        margin: 0px; 
    }    
    .cart-subtotal-item{
        font-size: 13px;
        margin:0px;
        margin-left: 2px;
        font-family: 'Montserrat', sans-serif;
        
    }
    .cart-total{
        font-size: 14px;
        font-family: 'Montserrat', sans-serif;
        color: #000;
        margin: 0px;
    }    
    .cart-total-inclusiv{  
        font-size: 10px;
        margin:0px;
        margin-left: 2px;
        font-family: 'Montserrat', sans-serif;
    }
    .checkout-btn{
        font-size: 12px;
        padding: 10px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 600;
        background-color: #005e00;
        color: #fff !important;
        width: 100%;
        height: 40px;
        cursor: pointer;
    }
    .checkout-btn:hover{
        color: black !important;
    }
    .countinue-shoping{
        font-size: 14px;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
        color: lightslategrey;
    }
    .cart-banner{
        width: inherit;
        /* height: 130px; */
    }
    .shipping-free{
        color: #005e00;
        font-weight: 600;
    }
    .bank-select-option-heading{
        font-size: 12;
        font-family: 'Montserrat', sans-serif;
        color:#000;
    }
    
</style>

@if(!empty($cartCount))
<div class="container mt-3">
    <div class="row">
        <div class="col-sm-8">
            <img src="{{asset('public_assets/images/Bannernew.png')}}" class="cart-banner img-fluid d-none d-md-block" alt="">
            <div class="d-flex justify-content-between">
                <div class="form-inline m-2 d-none d-md-block">
                    <p class="cart-heading"><b>My Cart</b></p>
                    <p class="cart-heading-items">(<span>{{ $cartCount }}</span>items)</p>
                </div>
            <!-- <a href="#" style="text-decoration: none;">
                <div class="form-inline">
                    <span>Deliver to: </span>
                    <p class="cart-deliver-to">Dubai </p> 
                    <i class="fas fa-chevron-right ml-1"></i>
                </div>
            </a> -->
            </div>
            <!-- cart item open -->
            @foreach($cartItems as $cartItem)
            <div class="container bg-white">
                <div class="row border-bottom">
                <div class=" col-4 col-md-3 ">
                @if(isset($cartItem->cover))
                    <img src="{{$cartItem->cover}}" class="img-fluid" alt="{{ $cartItem->name }}">
                @else
                <img src="https://placehold.it/120x120" alt="">
                @endif
                </div>
                <div class="col-6 col-md-6 ">
                    <div class="product_description">
                        <div class="company_name mt-5">ACER</div>
                        <div class="product_name">{{ $cartItem->name }}</div>
                    </div>
                    <div class="deliver-description">
                        <p class="cart-deliver-message">Delivered by 
                            <strong class="cart-deliver-date">Mon, Nov 16</strong><br>
                            <span>when you order in 10 hrs 37 mins</span>
                        </p>
                    </div> 
                    <div class="deliver-description">
                    <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="far fa-trash-alt mr-2" aria-hidden="true"></i>Delete</button>
                                            </form>
                    </div>
                </div>
                <div class="col-12 col-md-3 form-inline">
                    <div class="price-div form-inline ">
                        <p class="cart-currency ">{{config('cart.currency')}}</p>
                        <p class="cart-price">{{ number_format($cartItem->sale_price, 2) }}</p>
                    </div>
                    <div class="pb-3">
                        <p class="quantity">quantity:</p>
                    <form action="{{ route('cart.update', $cartItem->rowId) }}" class="form-inline" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="input-group">
                            <input type="number" name="quantity" min="1" value="{{ $cartItem->qty }}" class="form-control input-sm" />
                            <span class="input-group-btn"><button class="btn btn-default btn-sm">Update</button></span>
                        </div>
                    </form>
                    </div>


                

                </div>
                </div>
            </div>
            @endforeach
            <!-- cart item close -->
        
            
            <!-- cart item close -->
            <div class="m-2">
                <a href="{{route('home')}}" class="countinue-shoping">CONTINUE SHOPPING</a>
            </div>

        </div>
        <div class="col-sm-4">
            <div class="card mb-3">
                <div class="card-body ">
                    <form>
                        <!-- <div class="form-group"> <label class="coupon">Have coupon?</label> -->
                            <div class="input-group"> 
                                <input type="text" class="form-control coupon" name="" placeholder="Coupon code"> 
                                <span class="input-group-append"> 
                                    <button class="btn btn-apply coupon-btn">Apply</button> 
                                </span> 
                            </div>
                    </form>
                </div>
                <div class="pl-4 pr-4">
                    <div>
                        <p class="cart-summary-heading">Order Summary</p>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class=" form-inline">
                            <p class="cart-subtotal">Subtotal</p>
                            <p class="cart-subtotal-item">( <span>{{ $cartCount }}</span>itmes )</p>
                        </div>
                        <div>
                            <p class="cart-subtotal">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between border-bottom">
                        <div class=" form-inline">
                            <p class="cart-subtotal">Shipping</p>
                        </div>
                        <div>
                            <p class="shipping-free">FREE</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between pb-3">
                        <div class=" form-inline">
                            <p class="cart-total">Total</p>
                            <p class="cart-total-inclusiv">( Inclusive of VAT )</p>
                        </div>
                        <div>
                            <p class="cart-subtotal">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between pb-3">
                        <div class="form-inline">
                        <P><input type="checkbox" id="terms_condition">
                       I accept the <a href="{{ URL('return_policy') }}" target="_blank">Return Policy</a> <a href="{{ URL('terms_condition') }}" target="_blank">Terms of Use</a> and <a href="{{ URL('privacy_policy') }}" target="_blank">Privacy Policy</a>
                        </p>
                        </div>
                       
                    </div>    
                    <a class="btn btn-apply checkout-btn mb-3" href="javascript:;"  >CHECKOUT NOW</a>
                </div>
            </div>
            <div class="form-inline text-center ml-5 mr-5">
                <span> 
                    <p class="bank-select-option-heading p-0 m-0"><strong> Buy Now, Pay Later With abiyat EMI</strong></p>
                    <span class="text-muted"> Available when you spend AED 500 with select cards from the banks below. <a href="#">Find out more</a> </span>
                </span>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (0).jpg')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (1).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (2).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (4).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (3).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (5).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (7).png')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (8).jpg')}}" class="img-fluid m-1" alt="">
                        </div>
                        <div class="col-sm-4 col-4">
                            <img src="{{asset('public_assets/images/bank/image (9).jpg')}}" class="img-fluid m-1" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@else
<div class="container-fluid p-0 bg-light">
    <div class="main-empty-cart">
            <div class="empty-img">
                <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
            </div>
            <div class="empty-text">
                <h3>Your shopping cart looks empty</h3>
                <p>What are you waiting for?</p>
            </div>
            <a href="{{route('home')}}" class="btn btn-primary btn-lg" id="start_shopping">Start Shopping</a>
    </div>
</div>
@endif
@endsection