@extends('layouts.public.app')
@section('content')
<div class="container-fluid p-0 bg-light">
    <div class="main-empty-cart">
            <div class="empty-img">
                <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
            </div>
            <div class="empty-text">
                <h3>Your shopping cart looks empty</h3>
                <p>What are you waiting for?</p>
            </div>
            <button type="button" class="btn btn-primary btn-lg">Start Shopping</button>
    </div>
    
</div>


@endsection