<!DOCTYPE html>
<html lang="en-AE" dir="ltr" data-version="v8.10.18">
<!-- Mirrored from www.abiyat.com/uae-en/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Sep 2020 06:54:48 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#feee00"/>
    <title class="next-head">abiyat.com - Online Shopping in UAE | Fashion, electronics, beauty, baby products and
        more</title>
    <style id="__jsx-522397250">.container.jsx-522397250 {
        position: relative;
        height: 38px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    i.countryFlag.jsx-522397250 {
        width: 25px;
        height: 15px;
        background: url(assets/images/sprite.png) no-repeat -2000px 0 / 187px auto;;
    }

    i.countryFlag.sa.jsx-522397250 {
        background-position: 0 -168px;
    }

    i.countryFlag.ae.jsx-522397250 {
        background-position: -27px -168px;
    }

    i.countryFlag.eg.jsx-522397250 {
        background-position: -54px -168px;
    }

    .countryWrapper.jsx-522397250 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        cursor: pointer;
        -webkit-transition: opacity ease-in 0.2s;
        transition: opacity ease-in 0.2s;
    }

    .countryWrapper.jsx-522397250:hover {
        opacity: 0.6;
    }

    .countryWrapper.jsx-522397250 i.countryFlag.jsx-522397250 {
        margin-right: 8px;
    }

    html[dir='rtl'] .countryWrapper.jsx-522397250 i.countryFlag.jsx-522397250 {
        margin-left: 8px;
        margin-right: 0;
    }

    .shippingCountry.jsx-522397250 {
        padding-right: 8px;
        font-weight: bold;
    }

    html[dir='rtl'] .shippingCountry.jsx-522397250 {
        padding-right: 0px;
        padding-left: 8px;
    }

    .dropdownContainer.jsx-522397250 {
        background-color: #fff;
        color: #404553;
        box-shadow: 0 5px 4px 0 rgba(0, 0, 0, 0.18);
        border-radius: 2px;
        position: absolute;
        top: 38px;
        left: -15px;
        z-index: 1003;
        padding: 8px 0px;
        white-space: nowrap;
    }

    .dropdownContainer.jsx-522397250:after {
        content: '';
        bottom: 100%;
        left: 20px;
        border: 7px solid transparent;
        border-bottom-color: #fff;
        height: 0;
        width: 0;
        position: absolute;
    }

    html[dir='rtl'] .dropdownContainer.jsx-522397250 {
        right: -15px;
        left: auto;
    }

    html[dir='rtl'] .dropdownContainer.jsx-522397250:after {
        right: 20px;
        left: auto;
    }

    .option.jsx-522397250 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding: 10px 25px 10px 15px;
        width: 100%;
    }

    .option.jsx-522397250 i {
        font-size: 18px;
    }

    .option.jsx-522397250 i.countryFlag.jsx-522397250 {
        margin-left: 12px;
    }

    .option.jsx-522397250 .optionName.jsx-522397250 {
        margin-left: 8px;
    }

    .option.jsx-522397250 .optionName.selected.jsx-522397250 {
        font-weight: bold;
    }

    .option.jsx-522397250:not([disabled]):hover {
        background: #f7f7fa;
    }

    html[dir='rtl'] .option.jsx-522397250 {
        padding: 10px 15px 10px 25px;
    }

    html[dir='rtl'] .option.jsx-522397250 i.countryFlag.jsx-522397250 {
        margin-right: 12px;
        margin-left: 0;
    }

    html[dir='rtl'] .option.jsx-522397250 .optionName.jsx-522397250 {
        margin-right: 8px;
        margin-left: 0;
    }</style>
    <style id="__jsx-696087597">.iconMenuContainer.jsx-696087597 {
        padding: 16px 15px 12px;
        background-color: #fff;
        border-top: 1px solid #f7f7fa;
        border-bottom: 1px solid #e2e5f1;
        margin-top: -1px;
    }

    .iconMenuContainer.jsx-696087597 ul.jsx-696087597 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    .iconMenuContainer.jsx-696087597 li.jsx-696087597 {
        margin: 0 15px;
    }

    .iconMenuContainer.jsx-696087597 li.jsx-696087597 a.jsx-696087597, .iconMenuContainer.jsx-696087597 li.jsx-696087597 button.jsx-696087597 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .iconMenuContainer.jsx-696087597 li.jsx-696087597 i {
        background-color: #feee00;
        width: 38px;
        height: 38px;
        border-radius: 10px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        margin-bottom: 6px;
        font-size: 14px;
    }

    .iconMenuContainer.jsx-696087597 .menuTitle.jsx-696087597 {
        font-weight: 600;
        font-size: 0.91666rem;
        white-space: nowrap;
    }

    .counterIcon.jsx-696087597 {
        position: relative;
    }

    .iconMenuContainer.jsx-696087597 .counter.jsx-696087597 {
        position: absolute;
        top: -6px;
        right: -6px;
        width: 16px;
        height: 16px;
        border-radius: 100%;
        line-height: 1;
        background-color: #ffffff;
        color: #fff;
        font-size: 10px;
        font-weight: 600;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    html[dir='rtl'] .iconMenuContainer.jsx-696087597 .counter.jsx-696087597 {
        left: -6px;
        right: auto;
    }

    .iconMenuContainer.jsx-696087597 .counter.secondary.jsx-696087597 {
        background-color: #7e859b;
    }</style>
    <style id="__jsx-409027264">.container.jsx-409027264 {
        background-color: #fff;
        margin-top: 8px;
        padding: 16px 15px 0;
        border-bottom: 1px solid #e2e5f1;
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.04);
    }

    p.title.jsx-409027264 {
        color: #7e859b;
        text-transform: uppercase;
        font-weight: 700;
        -webkit-letter-spacing: 0.01em;
        -moz-letter-spacing: 0.01em;
        -ms-letter-spacing: 0.01em;
        letter-spacing: 0.01em;
        font-size: 0.83333rem;
    }

    li.jsx-409027264 + li.jsx-409027264 {
        border-top: 1px solid #e2e5f1;
    }

    li.jsx-409027264 a.jsx-409027264, li.jsx-409027264 button.jsx-409027264 {
        padding: 14px 0;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        font-size: 1.166666rem;
        font-weight: 600;
        width: 100%;
        box-sizing: border-box;
    }

    li.jsx-409027264 a.saleCategory.jsx-409027264 {
        color: #f6583d;
    }

    html[dir='rtl'] li.jsx-409027264 i.icon {
        -webkit-transform: scale(-1, 1);
        -ms-transform: scale(-1, 1);
        transform: scale(-1, 1);
        display: inline-block;
    }

    button.returnCta.jsx-409027264 {
        margin-bottom: 8px;
        width: 100%;
        text-align: left;
    }

    button.returnCta.jsx-409027264 i.icon {
        margin-right: 8px;
        font-size: 0.833333rem;
    }

    [dir='rtl'] button.returnCta.jsx-409027264 i.icon {
        margin-right: unset;
        margin-left: 8px;
        -webkit-transform: scale(-1, 1);
        -ms-transform: scale(-1, 1);
        transform: scale(-1, 1);
        display: inline-block;
    }

    [dir='rtl'] button.returnCta.jsx-409027264 {
        text-align: right;
    }</style>
    <style id="__jsx-2715901464">.optionsPanelContainer.jsx-2715901464 {
        position: fixed;
        top: 0;
        bottom: 0;
        right: -100%;
        width: 100%;
        -webkit-transition: right ease-in-out 0.4s;
        transition: right ease-in-out 0.4s;
        z-index: 100;
    }

    html[dir='rtl'] .optionsPanelContainer.jsx-2715901464 {
        left: -100%;
        right: auto;
        -webkit-transition: left ease-in-out 0.4s;
        transition: left ease-in-out 0.4s;
    }

    .optionsPanelContainer.open.jsx-2715901464 {
        right: 0;
    }

    html[dir='rtl'] .optionsPanelContainer.open.jsx-2715901464 {
        left: 0;
        right: auto;
    }

    .optionsPanelContainer.jsx-2715901464 .scrim.jsx-2715901464 {
        position: fixed;
        background-color: rgba(0, 0, 0, 0.2);
        top: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: opacity ease-in-out 0.6s;
        transition: opacity ease-in-out 0.6s;
    }

    .optionsPanelContainer.open.jsx-2715901464 .scrim.jsx-2715901464 {
        opacity: 1;
        pointer-events: auto;
    }

    .optionsPanel.jsx-2715901464 {
        background-color: #fff;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        width: 320px;
        box-shadow: 0 0 6px rgba(0, 0, 0, 0.1);
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    html[dir='rtl'] .optionsPanel.jsx-2715901464 {
        left: 0;
        right: auto;
    }

    header.jsx-2715901464 {
        background-color: #f7f7fa;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding: 14px 15px 13px;
        font-size: 1.25rem;
    }

    h3.jsx-2715901464 {
        font-weight: 700;
        font-size: 1.3333rem;
    }

    .searchFilterWrapper.jsx-2715901464 {
        -webkit-flex: 0 0 100%;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        box-sizing: border-box;
        color: #7e859b;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        border: 1px solid #e2e5f1;
        background-color: #fff;
        padding: 8px 10px;
        margin: 12px 0 3px;
    }

    .searchFilterWrapper.jsx-2715901464 input.jsx-2715901464 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        border: 0;
        margin-right: 15px;
    }

    html[dir='rtl'] .searchFilterWrapper.jsx-2715901464 input.jsx-2715901464 {
        margin-left: 15px;
        margin-right: 0;
    }

    .optionsPanel.jsx-2715901464 ul.jsx-2715901464 {
        padding: 0 0 20px;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        overflow: auto;
        -webkit-overflow-scrolling: touch;
    }

    .optionsPanel.jsx-2715901464 li.jsx-2715901464 button.jsx-2715901464, .optionsPanel.jsx-2715901464 li.jsx-2715901464 .noDataLabel.jsx-2715901464 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        width: 100%;
        font-size: 1.08333rem;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding: 12px 15px 13px;
        box-sizing: border-box;
        color: #7e859b;
    }

    .optionsPanel.jsx-2715901464 li.jsx-2715901464 button.jsx-2715901464:hover {
        background-color: #F7F9FE;
    }

    .optionsPanel.jsx-2715901464 li.jsx-2715901464 button.selected.jsx-2715901464 {
        font-weight: 600;
        color: #404553;
    }

    .optionsPanel.jsx-2715901464 li.jsx-2715901464 button.jsx-2715901464 i {
        font-size: 1.5rem;
        color: #b2bbd2;
    }

    .optionsPanel.jsx-2715901464 li.jsx-2715901464 button.selected.jsx-2715901464 i {
        color: #ffffff;
    }

    .optionsPanel.jsx-2715901464 .optionWrapper.jsx-2715901464 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .optionsPanel.jsx-2715901464 .optionWrapper.jsx-2715901464 span.jsx-2715901464 {
        margin-right: 10px;
    }</style>
    <style id="__jsx-1773613676">.supportMenuContainer.jsx-1773613676 {
        margin-top: 8px;
        box-shadow: 0 0 1px rgba(0, 0, 0, 0.04);
    }

    .supportMenuContainer.jsx-1773613676 ul.jsx-1773613676 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        background-color: #fff;
    }

    .supportMenuContainer.jsx-1773613676 li.jsx-1773613676 {
        border-bottom: 1px solid #e2e5f1;
        -webkit-flex: 0 0 100%;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
    }

    .supportMenuContainer.jsx-1773613676 li.half.jsx-1773613676 {
        -webkit-flex: 0 0 50%;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
    }

    .supportMenuContainer.jsx-1773613676 .half.jsx-1773613676 + .half.jsx-1773613676 a.jsx-1773613676 {
        border-left: 1px solid #e2e5f1;
    }

    html[dir='rtl'] .supportMenuContainer.jsx-1773613676 .half.jsx-1773613676 + .half.jsx-1773613676 a.jsx-1773613676 {
        border-right: 1px solid #e2e5f1;
        border-left: 0;
    }

    .supportMenuContainer.jsx-1773613676 a.jsx-1773613676, .supportMenuContainer.jsx-1773613676 button.jsx-1773613676 {
        padding: 14px 15px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        font-size: 1.083333rem;
    }

    .supportMenuContainer.jsx-1773613676 button.jsx-1773613676 {
        width: 100%;
        box-sizing: border-box;
    }

    .supportMenuContainer.jsx-1773613676 button.jsx-1773613676 > i.icon, .supportMenuContainer.jsx-1773613676 a.jsx-1773613676 > i.icon {
        margin-right: 8px;
        width: 24px;
        height: 24px;
        border-radius: 100%;
        background-color: #F7F9FE;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        font-size: 10px;
        color: #7e859b;
    }

    [dir='rtl'] .supportMenuContainer.jsx-1773613676 button.jsx-1773613676 > i.icon, [dir='rtl'] .supportMenuContainer.jsx-1773613676 a.jsx-1773613676 > i.icon {
        -webkit-transform: scale(-1, 1);
        -ms-transform: scale(-1, 1);
        transform: scale(-1, 1);
        margin-left: 8px;
        margin-right: 0;
    }

    .menuTitle.jsx-1773613676 {
        font-weight: 600;
        color: #7e859b;
    }

    .currentOption.jsx-1773613676 {
        margin-left: auto;
        font-weight: 600;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    [dir='rtl'] .currentOption.jsx-1773613676 {
        margin-left: unset;
        margin-right: auto;
    }

    i.countryFlag.jsx-1773613676 {
        width: 25px;
        height: 15px;
        margin-left: 8px;
        margin-top: -1px;
        background: url(assets/images/sprite.png) no-repeat -2000px 0 / 187px auto;;
    }

    [dir='rtl'] i.countryFlag.jsx-1773613676 {
        margin-left: unset;
        margin-right: 8px;
    }

    i.countryFlag.sa.jsx-1773613676, i.countryFlag.saudi.jsx-1773613676 {
        background-position: 0 -168px;
    }

    i.countryFlag.ae.jsx-1773613676, i.countryFlag.uae.jsx-1773613676 {
        background-position: -27px -168px;
    }

    .language.jsx-1773613676 {
        font-family: Cairo, sans-serif;
    }

    html[dir='rtl'] .language.jsx-1773613676 {
        font-family: Proxima Nova, Helvetica Neue, Arial, sans-serif;
    }

    .supportMenuContainer.jsx-1773613676 .signOut.jsx-1773613676 {
        padding: 10px 15px 20px;
    }</style>
    <style id="__jsx-1079652234">.menuTrigger.jsx-1079652234 {
        font-size: 1.916666rem;
        padding: 0 20px 0 0;
        line-height: 65px;
        position: relative;
        top: 1px;
        display: block;
    }

    html[dir='rtl'] .menuTrigger.jsx-1079652234 {
        right: -10px;
        left: auto;
    }

    .menuPanelContainer.jsx-1079652234 {
        position: fixed;
        top: 0;
        left: -100%;
        bottom: 0;
        width: 100%;
        -webkit-transition: all ease-in-out 0.4s;
        transition: all ease-in-out 0.4s;
        z-index: 100;
    }

    html[dir='rtl'] .menuPanelContainer.jsx-1079652234 {
        right: -100%;
        left: auto;
    }

    .menuPanelContainer.open.jsx-1079652234 {
        left: 0;
    }

    html[dir='rtl'] .menuPanelContainer.open.jsx-1079652234 {
        right: 0;
    }

    .menuPanelContainer.jsx-1079652234 .scrim.jsx-1079652234 {
        position: fixed;
        background-color: rgba(0, 0, 0, 0.2);
        top: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: opacity ease-in-out 0.6s;
        transition: opacity ease-in-out 0.6s;
    }

    .menuPanelContainer.open.jsx-1079652234 .scrim.jsx-1079652234 {
        opacity: 1;
        pointer-events: auto;
    }

    .menuPanel.jsx-1079652234 {
        background-color: #f7f7fa;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        overflow-y: auto;
        width: 335px;
    }

    html[dir='rtl'] .menuPanel.jsx-1079652234 {
        right: 0;
        left: auto;
    }

    header.jsx-1079652234 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: flex-start;
        -webkit-box-align: flex-start;
        -ms-flex-align: flex-start;
        align-items: flex-start;
        padding: 12px 15px;
        background-color: #fff;
        border-bottom: 1px solid #e2e5f1;
    }

    .topCtr.jsx-1079652234 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: base;
        -webkit-box-align: base;
        -ms-flex-align: base;
        align-items: base;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        box-sizing: border-box;
        width: 100%;
    }

    .topCtr.jsx-1079652234 .leftCtr.jsx-1079652234 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .logoContainer.jsx-1079652234 {
        height: 28px;
        margin: 0 0 2px 10px;
    }

    p.greeting.jsx-1079652234 {
        font-size: 1.6666667rem;
        font-weight: bold;
    }

    .email.jsx-1079652234 {
        display: block;
        font-weight: normal;
        font-size: 1rem;
        color: #7e859b;
    }

    .panelClose.jsx-1079652234 {
        font-size: 1.6666667rem;
    }

    @media only screen and (min-width: 992px) {
        .menuTrigger.jsx-1079652234 {
            display: none;
        }
    }</style>
    <style id="__jsx-3997850843">.inputContainer.jsx-3997850843 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        background-color: #fff;
        position: relative;
        z-index: 50;
    }

    input.jsx-3997850843 {
        width: 100%;
        height: 36px;
        padding: 12px 15px;
        border: 0;
        background-color: transparent;
        color: black;
    }

    .store.jsx-3997850843 input.jsx-3997850843 {
        background-color: #f3f3f4;
        border-radius: 4px;
    }

    .searchTrigger.jsx-3997850843 {
        color: #404553;
        font-size: 16px;
        padding: 0 15px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .store.jsx-3997850843 .searchTrigger.jsx-3997850843 {
        position: absolute;
        right: 0;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    html[dir='rtl'] .store.jsx-3997850843 .searchTrigger.jsx-3997850843 {
        right: unset;
        left: 0;
    }

    button.searchTrigger.jsx-3997850843:hover {
        color: #b2bbd2;
    }

    .suggestionsContainer.jsx-3997850843 {
        background-color: #fff;
        position: absolute;
        overflow: visible;
        top: 45px;
        right: 0;
        left: 0;
        bottom: auto;
        font-size: 1.083333rem;
        z-index: 40;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        box-shadow: 0 5px 4px 0 rgba(0, 0, 0, 0.18);
        border-radius: 0 0 2px 2px;
    }

    .suggestionsContainer.jsx-3997850843:after, .suggestionsContainer.jsx-3997850843:before {
        bottom: 100%;
        left: 20px;
        border: solid transparent;
        content: ' ';
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        top: -14px;
    }

    .suggestionsContainer.jsx-3997850843:after {
        border-color: rgba(255, 255, 255, 0);
        border-bottom-color: #ffffff;
        border-width: 7px;
        margin-right: -7px;
        z-index: 16;
    }

    .suggestionBox.jsx-3997850843 {
        -webkit-flex: auto;
        -ms-flex: auto;
        flex: auto;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .suggestionBox.suggestionlist.jsx-3997850843 .matchedWrapper.jsx-3997850843 {
        display: inline-block;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-flex: 2;
        -ms-flex: 2;
        flex: 2;
    }

    .suggestionBox.jsx-3997850843 + .suggestionBox.jsx-3997850843 {
        -webkit-flex: 2;
        -ms-flex: 2;
        flex: 2;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .suggestionsContainer.jsx-3997850843 ul.jsx-3997850843 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    .suggestionsContainer.jsx-3997850843 li.jsx-3997850843 {
        padding: 10px 15px;
    }

    .keyText.jsx-3997850843 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        border-bottom: 1px solid #f6f6f6;
        padding: 8px 0px 8px 10px;
        text-indent: 4px;
        width: 100%;
    }

    a.catLink.jsx-3997850843 {
        color: #ffffff;
        font-weight: bold;
        text-transform: capitalize;
    }

    .suggestionsContainer.jsx-3997850843 li.selected.jsx-3997850843, .suggestionsContainer.jsx-3997850843 li.jsx-3997850843:hover {
        background-color: #fcfbf4;
    }

    .suggestionsContainer.jsx-3997850843 li.jsx-3997850843:focus {
        background-color: #fcfbf4;
    }

    .suggestionsContainer.jsx-3997850843 div.groupTitle.jsx-3997850843 {
        font-weight: 700;
        font-size: 1rem;
        text-transform: uppercase;
        color: #b2bbd2;
        border-bottom: 0;
        padding: 10px 15px;
    }

    .suggestionsContainer.jsx-3997850843 li.jsx-3997850843 a.jsx-3997850843 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .matchedWrapper.jsx-3997850843 {
        display: none;
    }

    @media only screen and (min-width: 992px) {
        .matchedWrapper.jsx-3997850843 {
            display: block;
        }
    }

    ul.textSuggestions.jsx-3997850843 li.jsx-3997850843 a.jsx-3997850843 em {
        font-weight: bold;
    }

    ul.productSuggestions.jsx-3997850843 {
        border-right: none;
        border-left: 1px solid #e2e5f1;
    }

    ul.productSuggestions.jsx-3997850843 li.jsx-3997850843 a.jsx-3997850843 {
        -webkit-box-pack: start;
        -webkit-justify-content: flex-start;
        -ms-flex-pack: start;
        justify-content: flex-start;
    }

    ul.productSuggestions.jsx-3997850843 .imageContainer.jsx-3997850843 {
        width: 40px;
        border: 1px solid #e2e5f1;
        background-color: #fff;
    }

    ul.productSuggestions.jsx-3997850843 img.jsx-3997850843 {
        display: block;
        max-width: 100%;
    }

    ul.productSuggestions.jsx-3997850843 .detailsContainer.jsx-3997850843 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        margin-left: 20px;
        font-size: 1rem;
    }

    html[dir='rtl'] ul.productSuggestions.jsx-3997850843 .detailsContainer.jsx-3997850843 {
        margin-right: 20px;
        margin-left: 0;
    }

    ul.productSuggestions.jsx-3997850843 .brand.jsx-3997850843 {
        color: #7e859b;
        font-size: 0.916666rem;
        margin-bottom: 2px;
    }

    @media only screen and (min-width: 992px) {
        ul.productSuggestions.jsx-3997850843 {
            border-right: 1px solid #e2e5f1;
        }
    }

    .recentSearchListItem.jsx-3997850843 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .recentSearchCross.jsx-3997850843 {
        cursor: pointer;
    }

    html[dir='rtl'] .recentSearchCross {
        margin-left: 0;
        margin-top: 0;
    }

    .recentSearchLink.jsx-3997850843 {
        width: 90%;
        margin-top: -2px;
    }

    .recentSearchClearAllContainer.jsx-3997850843 {
        font-size: 75%;
        text-align: right;
        margin-top: -14px;
    }

    html[dir='rtl'] .recentSearchClearAllContainer.jsx-3997850843 {
        text-align: left;
    }

    .recentSearchClearAll.jsx-3997850843 {
        cursor: pointer;
    }

    .recentSearchArrowIcon.jsx-3997850843 {
        -webkit-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        transform: rotate(-45deg);
        cursor: pointer;
    }

    html[dir='rtl'] .recentSearchArrowIcon.jsx-3997850843 {
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    span.catHighlight.jsx-3997850843 {
        color: #ffffff;
        font-weight: bold;
        display: initial;
        padding-left: 5px;
    }

    .catLink.jsx-3997850843 {
        cursor: pointer;
    }</style>
    <style id="__jsx-3230834092">.loaderWrapper.jsx-3230834092 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .loaderWrapper.jsx-3230834092 .loader.jsx-3230834092 .loaderImage.jsx-3230834092 {
        max-width: 100%;
        display: block;
    }</style>
    <style id="__jsx-150789449">.menuTrigger.jsx-150789449 {
        font-size: 1.166666rem;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        font-weight: bold;
        padding: 0 20px;
        border-right-width: 1px;
        border-style: solid;
        border-color: #e2e5f1;
        text-transform: uppercase;
        position: relative;
        color: #000000;
        width: 216px;
        box-sizing: border-box;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        height: 44px;
    }

    html[dir='rtl'] .menuTrigger.jsx-150789449 {
        border-left-width: 1px;
        border-right-width: 0;
    }

    .menuTrigger.jsx-150789449 i {
        font-size: 1rem;
    }

    .menuTrigger.fauxTrigger.jsx-150789449 {
        border-color: #fcfbf4;
        color: #b2bbd2;
    }

    .topLevelWrapper.jsx-150789449 {
        background-color: #fcfbf4;
        width: 216px;
        padding-bottom: 10px;
    }

    .categoryWrapper.jsx-150789449 {
        font-size: 1.166666rem;
        padding: 10px 20px;
        width: 100%;
        text-align: initial;
        display: block;
    }

    .categoryWrapper.isActive.jsx-150789449 {
        background-color: #fff;
        font-weight: bold;
    }

    .dropDownWrapper.jsx-150789449 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        position: absolute;
        top: 0;
        box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.18);
    }

    .dropDownWrapper.subMenuActive.jsx-150789449 {
        width: 100%;
        max-width: 768px;
    }

    .subCategoryCtr.jsx-150789449 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        background: #fff;
    }</style>
    <style id="__jsx-1187918855">.mainCategories.jsx-1187918855 {
        height: 44px;
        overflow: hidden;
        padding-left: 15px;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .mainCategories.jsx-1187918855 li.item.jsx-1187918855 {
        display: inline-block;
    }

    .mainCategories.jsx-1187918855 .menuTrigger.jsx-1187918855 {
        text-transform: uppercase;
        padding: 0 15px;
        height: 44px;
        font-size: 1.16666rem;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        font-weight: 700;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .mainCategories.jsx-1187918855 li.item.jsx-1187918855:hover .menuTrigger.jsx-1187918855 {
        background-color: #fff;
        box-shadow: 0 5px 4px 0 rgba(0, 0, 0, 0.18);
    }

    .item.jsx-1187918855 .categoryFocusMenu.jsx-1187918855 {
        display: none;
        position: absolute;
        left: 0;
        top: 44px;
        max-width: 1490px;
        width: 100%;
        box-sizing: border-box;
        padding: 20px 20px 25px;
        background-color: #fff;
        box-shadow: 0 5px 4px 0 rgba(0, 0, 0, 0.18);
        z-index: 9999999;
    }

    .item.jsx-1187918855:hover .categoryFocusMenuShow.jsx-1187918855 {
        display: block;
    }

    .contentWrapper.jsx-1187918855 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-left: -30px;
    }

    .contentCol.jsx-1187918855 {
        padding-left: 30px;
        box-sizing: border-box;
    }

    .categoriesColumn.jsx-1187918855 {
        -webkit-flex: 0 0 17%;
        -ms-flex: 0 0 17%;
        flex: 0 0 17%;
    }

    .brandsColumn.jsx-1187918855 {
        -webkit-flex: 0 0 33%;
        -ms-flex: 0 0 33%;
        flex: 0 0 33%;
    }

    .bannersColumn.jsx-1187918855 {
        -webkit-flex: 0 0 50%;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
    }

    p.title.jsx-1187918855 {
        font-size: 1.0833333rem;
        font-weight: bold;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        margin-bottom: 10px;
    }

    .categoriesColumn.jsx-1187918855 li.jsx-1187918855 + li.jsx-1187918855 {
        margin-top: 15px;
    }

    .categoriesColumn.jsx-1187918855 li.jsx-1187918855:hover {
        color: #ffffff;
    }

    .categoriesColumn.jsx-1187918855 a.jsx-1187918855 {
        text-align: initial;
    }</style>
    <style id="__jsx-127861163">.siteHeader.jsx-127861163 {
        background: #005E00;
        position: relative;
        z-index: 60;
        color: white;
    }

    .headroom--unfixed {
        position: static;
        margin-top: 0;
    }

    .headroom {
        top: -200px;
        left: 0;
        right: 0;
        z-index: 60;
    }

    .headroom--unpinned {
        position: fixed;
        -webkit-transition: top ease-in-out 0.3s;
        transition: top ease-in-out 0.3s;
    }

    .headroom--pinned {
        position: fixed;
        top: 0;
        -webkit-transition: top ease-in-out 0.3s;
        transition: top ease-in-out 0.3s;
        margin-top: -30px;
    }

    .bgDrop.jsx-127861163 {
        background-color: #404553;
        opacity: 0.6;
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        z-index: 15;
    }

    .supportBar.jsx-127861163 {
        background-color: #005E00;
        height: 38px;
        margin-bottom: -8px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .supportBar.jsx-127861163 > .siteWidthContainer.jsx-127861163 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .localeSettings.jsx-127861163 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin-top: -1px;
    }

    .languageSelectLink.jsx-127861163 {
        font-size: 1rem;
        font-weight: bold;
        padding-right: 15px;
        border-right: 1px solid white;
        font-family: Cairo, sans-serif;
    }

    .languageSelectLink.jsx-127861163 .languageButton.jsx-127861163 {
        -webkit-transition: opacity ease-in 0.2s;
        transition: opacity ease-in 0.2s;
    }

    .languageSelectLink.jsx-127861163 .languageButton.jsx-127861163:hover {
        opacity: 0.6;
    }

    html[dir='rtl'] .languageSelectLink.jsx-127861163 {
        padding-right: 0px;
        padding-left: 15px;
        border-right-width: 0;
        border-left: 1px solid rgba(64, 69, 83, 0.2);
        font-family: Proxima Nova, Helvetica Neue, Arial, sans-serif;
    }

    .countryWrapper.jsx-127861163 {
        padding: 0 15px;
    }

    .uspIcon.jsx-127861163 {
        height: 17px;
        background: url(assets/images/sprite.png) no-repeat -2000px 0 / 187px auto;;
        display: block;
        margin-right: 8px;
        margin-top: -1px;
        -webkit-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
    }

    .uspIcon.truck.jsx-127861163 {
        background-position: 0 -313px;
        width: 22px;
    }

    .uspIcon.returns.jsx-127861163 {
        background-position: -24px -313px;
        width: 17px;
    }

    .uspIcon.secure.jsx-127861163 {
        background-position: -43px -313px;
        width: 18px;
    }

    html[dir='rtl'] .uspIcon.jsx-127861163 {
        margin-left: 8px;
        margin-right: 0;
    }

    .usps.jsx-127861163 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .usps.jsx-127861163 li.jsx-127861163 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 0.833333rem;
        -webkit-letter-spacing: 0.05em;
        -moz-letter-spacing: 0.05em;
        -ms-letter-spacing: 0.05em;
        letter-spacing: 0.05em;
    }

    .usps.jsx-127861163 li.jsx-127861163 + li.jsx-127861163 {
        margin-left: 25px;
    }

    .usps.jsx-127861163 .helper.jsx-127861163 {
        margin-left: 3px;
        -webkit-letter-spacing: 0.025em;
        -moz-letter-spacing: 0.025em;
        -ms-letter-spacing: 0.025em;
        letter-spacing: 0.025em;
        font-weight: normal;
        margin-top: -2px;
    }

    html[dir='rtl'] .usps.jsx-127861163 li.jsx-127861163 + li.jsx-127861163 {
        margin-right: 25px;
        margin-left: 0;
    }

    .container.jsx-127861163 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        height: 65px;
    }

    .logoContainer.jsx-127861163 {
        height: 30px;
        display: block;
    }

    html[dir='rtl'] .logoContainer.jsx-127861163 {
        height: 28px;
    }

    .searchWrapper.jsx-127861163 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        margin: 0 20px;
    }

    @media only screen and (min-width: 992px) {
        .searchWrapper.jsx-127861163 {
            margin: 0 30px;
        }
    }

    @media only screen and (min-width: 1200px) {
        .searchWrapper.jsx-127861163 {
            margin: 0 40px;
        }
    }

    .counterLoader.jsx-127861163, .counter.jsx-127861163 {
        position: absolute;
        top: -8px;
        right: -9px;
        width: 18px;
        height: 18px;
        border-radius: 100%;
        line-height: 1;
        background-color: #ffffff;
        color: #fff;
        font-size: 10px;
        font-weight: 600;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    .counterLoader.secondary.jsx-127861163, .counter.secondary.jsx-127861163 {
        background-color: #7e859b;
    }

    html[dir='rtl'] .counterLoader.jsx-127861163, html[dir='rtl'] .counter.jsx-127861163 {
        left: -9px;
        right: auto;
    }

    .userWrapper.jsx-127861163 {
        padding-right: 15px;
    }

    html[dir='rtl'] .userWrapper.jsx-127861163 {
        padding-left: 15px;
        padding-right: 0;
    }

    .userLoading.jsx-127861163 {
        position: relative;
    }

    .userLoading.jsx-127861163 i {
        font-size: 2rem;
    }

    .container.jsx-127861163 .cartLink, .container.jsx-127861163 .wishlistLink {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding: 0 15px;
        border-left: 1px solid rgba(64, 69, 83, 0.2);
        -webkit-transition: opacity ease-in-out 0.2s;
        transition: opacity ease-in-out 0.2s;
    }

    .container.jsx-127861163 .cartLink:hover, .container.jsx-127861163 .wishlistLink:hover {
        opacity: 0.6;
    }

    html[dir='rtl'] .container.jsx-127861163 .cartLink, html[dir='rtl'] .container.jsx-127861163 .wishlistLink {
        border-right: 1px solid rgba(64, 69, 83, 0.2);
        border-left: 0;
    }

    .container.jsx-127861163 .cartLink {
        padding-right: 0;
    }

    html[dir='rtl'] .container.jsx-127861163 .cartLink {
        padding-left: 0;
        padding-right: 15px;
    }

    .container.jsx-127861163 .wishlistLink {
        display: none;
    }

    @media only screen and (min-width: 992px) {
        .container.jsx-127861163 .wishlistLink {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }
    }

    .cartTitle.jsx-127861163 {
        margin-right: 8px;
        display: none;
        font-size: 1.0833333rem;
        font-weight: bold;
    }

    @media only screen and (min-width: 992px) {
        .cartTitle.jsx-127861163 {
            display: block;
        }
    }

    html[dir='rtl'] .cartTitle.jsx-127861163 {
        margin-left: 8px;
        margin-right: 0;
    }

    .fullOptions.jsx-127861163 .cartTitle.jsx-127861163 {
        display: none;
    }

    @media only screen and (min-width: 1366px) {
        .fullOptions.jsx-127861163 .cartTitle.jsx-127861163 {
            display: block;
        }
    }

    .cartIconContainer.jsx-127861163 {
        position: relative;
    }

    .cartIconContainer.jsx-127861163 i.cartIcon {
        font-size: 2rem;
    }

    .cartIconContainer.jsx-127861163 i.wishlistIcon {
        font-size: 1.75rem;
    }

    html[dir='rtl'] .cartIconContainer.jsx-127861163 i.cartIcon {
        -webkit-transform: scaleX(-1);
        -ms-transform: scaleX(-1);
        transform: scaleX(-1);
        display: inline-block;
    }

    .container.jsx-127861163 .languageSelectLink.jsx-127861163, .container.jsx-127861163 .countryWrapper.jsx-127861163 {
        display: none;
    }

    @media only screen and (min-width: 992px) {
        .container.jsx-127861163 .languageSelectLink.jsx-127861163, .container.jsx-127861163 .countryWrapper.jsx-127861163 {
            display: block;
            position: relative;
        }

        .container.jsx-127861163 .languageSelectLink.jsx-127861163:after, .container.jsx-127861163 .countryWrapper.jsx-127861163:after {
            content: '';
            position: absolute;
            top: 50%;
            margin-top: -11px;
            right: 0;
            height: 23px;
            width: 1px;
            background-color: rgba(64, 69, 83, 0.2);
        }

        .container.jsx-127861163 .countryWrapper.jsx-127861163 {
            margin-right: 15px;
        }

        html[dir='rtl'] .container.jsx-127861163 .languageSelectLink.jsx-127861163:after, html[dir='rtl'] .container.jsx-127861163 .countryWrapper.jsx-127861163:after {
            left: 0;
            right: auto;
        }

        html[dir='rtl'] .container.jsx-127861163 .countryWrapper.jsx-127861163 {
            margin-right: 0;
            margin-left: 15px;
        }
    }

    .desktopNavContainer.jsx-127861163 {
        background: #fcfbf4;
        border-bottom: 1px solid #e2e5f1;
        display: none;
        position: relative;
    }

    .desktopNav.jsx-127861163 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        position: relative;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        height: 44px;
    }

    @media only screen and (min-width: 992px) {
        .desktopNavContainer.jsx-127861163 {
            display: block;
        }
    }</style>
    <style id="__jsx-785064993">.headerContainer.jsx-785064993 {
        padding: 28px 15px 0;
    }

    .slide.jsx-785064993 > a.jsx-785064993 {
        display: block;
    }

    .slide.jsx-785064993 img.jsx-785064993 {
        width: 100%;
    }

    .container.jsx-785064993 .swiper-control-next, .container.jsx-785064993 .swiper-control-prev {
        width: 54px;
        height: 58px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        margin-top: -41px;
        position: absolute;
        top: 50%;
        background-color: #fff;
        opacity: 0.5;
        -webkit-transition: opacity ease-in-out 0.2s;
        transition: opacity ease-in-out 0.2s;
        z-index: 1;
        cursor: pointer;
    }

    .container.jsx-785064993 .swiper-control-next:before, .container.jsx-785064993 .swiper-control-prev:before {
        font-family: 'icomoon';
        font-size: 28px;
        color: #7e859b;
    }

    .container.jsx-785064993 .swiper-control-next:hover, .container.jsx-785064993 .swiper-control-prev:hover {
        opacity: 1;
    }

    .container.jsx-785064993 .swiper-control-prev {
        left: 0;
    }

    .container.jsx-785064993 .swiper-control-prev:before {
        content: '\e904';
        padding-right: 6px;
    }

    .container.jsx-785064993 .swiper-control-next {
        right: 0;
    }

    .container.jsx-785064993 .swiper-control-next:before {
        content: '\e905';
        padding-left: 6px;
    }

    html[dir='rtl'] .container.jsx-785064993 .swiper-control-prev {
        right: 0;
        left: auto;
    }

    html[dir='rtl'] .container.jsx-785064993 .swiper-control-prev:before {
        content: '\e905';
        padding-left: 6px;
        padding-right: 0;
    }

    html[dir='rtl'] .container.jsx-785064993 .swiper-control-next {
        left: 0;
        right: auto;
    }

    html[dir='rtl'] .container.jsx-785064993 .swiper-control-next:before {
        content: '\e904';
        padding-right: 6px;
        padding-left: 0;
    }

    .bannerWrapper.jsx-785064993 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-bottom: -15px;
    }

    .timerWrapper.jsx-785064993 {
        position: absolute;
        pointer-events: none;
    }

    .timerWrapper-bottom-left.jsx-785064993 {
        position: absolute;
        top: 95%;
        left: 5%;
        -webkit-transform: translate(-5%, -95%);
        -ms-transform: translate(-5%, -95%);
        transform: translate(-5%, -95%);
    }

    .timerWrapper-bottom-right.jsx-785064993 {
        position: absolute;
        top: 95%;
        left: 95%;
        -webkit-transform: translate(-95%, -95%);
        -ms-transform: translate(-95%, -95%);
        transform: translate(-95%, -95%);
    }

    .timerWrapper-top-left.jsx-785064993 {
        position: absolute;
        top: 5%;
        left: 5%;
        -webkit-transform: translate(-5%, -5%);
        -ms-transform: translate(-5%, -5%);
        transform: translate(-5%, -5%);
    }

    .timerWrapper-top-right.jsx-785064993 {
        position: absolute;
        top: 5%;
        left: 95%;
        -webkit-transform: translate(-95%, -5%);
        -ms-transform: translate(-95%, -5%);
        transform: translate(-95%, -5%);
    }

    .timerWrapper-center.jsx-785064993 {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .timerWrapper-middle-right.jsx-785064993 {
        position: absolute;
        top: 50%;
        left: 95%;
        -webkit-transform: translate(-95%, -50%);
        -ms-transform: translate(-95%, -50%);
        transform: translate(-95%, -50%);
    }

    .timerWrapper-middle-left.jsx-785064993 {
        position: absolute;
        top: 50%;
        left: 5%;
        -webkit-transform: translate(-5%, -50%);
        -ms-transform: translate(-5%, -50%);
        transform: translate(-5%, -50%);
    }

    .timerWrapper-top-center.jsx-785064993 {
        position: absolute;
        top: 5%;
        left: 50%;
        -webkit-transform: translate(-50%, -5%);
        -ms-transform: translate(-50%, -5%);
        transform: translate(-50%, -5%);
    }

    .timerWrapper-bottom-center.jsx-785064993 {
        position: absolute;
        top: 95%;
        left: 50%;
        -webkit-transform: translate(-50%, -95%);
        -ms-transform: translate(-50%, -95%);
        transform: translate(-50%, -95%);
    }

    .timerContainer.jsx-785064993 {
        background-size: cover;
        color: #fff;
        font-weight: bold;
        padding: 5px;
    }</style>
    <style id="__jsx-2714670158">.container.jsx-2714670158 {
        position: relative;
        background-color: #f7f7fa;
        -webkit-transition: background-color ease 0.25s;
        transition: background-color ease 0.25s;
    }

    .container.loaded.jsx-2714670158 {
        background-color: transparent;
    }

    .container.jsx-2714670158:before {
        font-family: icomoon;
        content: '\e92d';
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -12px;
        margin-top: -12px;
        font-size: 24px;
        color: #b2bbd2;
        opacity: 1;
        -webkit-transition: opacity ease 0.25s;
        transition: opacity ease 0.25s;
    }

    .container.loaded.jsx-2714670158:before {
        opacity: 0;
    }

    .mediaContainer.jsx-2714670158 {
        position: absolute;
        top: 0;
        left: 0;
    }

    div.jsx-2714670158 img:not(.zoomImage), div.jsx-2714670158 video {
        display: block;
        max-width: 100%;
    }

    .iframe.jsx-2714670158 .mediaContainer.jsx-2714670158 {
        height: 100%;
        width: 100%;
    }

    div.jsx-2714670158 iframe {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }</style>
    <style id="__jsx-4021287695">.swiper-container {
        margin-left: auto;
        margin-right: auto;
        position: relative;
        overflow: hidden;
        z-index: 1;
    }

    .swiper-container-no-flexbox .swiper-slide {
        float: left;
    }

    .swiper-container-vertical > .swiper-wrapper {
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    .swiper-wrapper {
        position: relative;
        width: 100%;
        height: 100%;
        z-index: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: -webkit-transform;
        transition-property: -webkit-transform;
        -o-transition-property: transform;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: transform;
        transition-property: transform;
        -webkit-transition-property: -webkit-transform, -webkit-transform;
        -webkit-transition-property: transform, -webkit-transform;
        transition-property: transform, -webkit-transform;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
    }

    .swiper-container-android .swiper-slide, .swiper-wrapper {
        -webkit-transform: translate3d(0px, 0, 0);
        -webkit-transform: translate3d(0px, 0, 0);
        -ms-transform: translate3d(0px, 0, 0);
        transform: translate3d(0px, 0, 0);
    }

    .swiper-container-multirow > .swiper-wrapper {
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }

    .swiper-container-free-mode > .swiper-wrapper {
        -webkit-transition-timing-function: ease-out;
        -o-transition-timing-function: ease-out;
        -webkit-transition-timing-function: ease-out;
        transition-timing-function: ease-out;
        margin: 0 auto;
    }

    .swiper-slide {
        -webkit-flex-shrink: 0;
        -ms-flex-negative: 0;
        -webkit-flex-shrink: 0;
        -ms-flex-negative: 0;
        flex-shrink: 0;
        width: 100%;
        height: 100%;
        position: relative;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: -webkit-transform;
        transition-property: -webkit-transform;
        -o-transition-property: transform;
        -webkit-transition-property: -webkit-transform;
        -webkit-transition-property: transform;
        transition-property: transform;
        -webkit-transition-property: -webkit-transform, -webkit-transform;
        -webkit-transition-property: transform, -webkit-transform;
        transition-property: transform, -webkit-transform;
    }

    .swiper-invisible-blank-slide {
        visibility: hidden;
    }

    .swiper-container-autoheight, .swiper-container-autoheight .swiper-slide {
        height: auto;
    }

    .swiper-container-autoheight .swiper-wrapper {
        -webkit-box-align: start;
        -webkit-align-items: flex-start;
        -ms-flex-align: start;
        -webkit-align-items: flex-start;
        -webkit-box-align: flex-start;
        -ms-flex-align: flex-start;
        align-items: flex-start;
        -webkit-transition-property: height, -webkit-transform;
        -webkit-transition-property: height, -webkit-transform;
        -webkit-transition-property: height, -webkit-transform;
        transition-property: height, -webkit-transform;
        -o-transition-property: transform, height;
        -webkit-transition-property: -webkit-transform, height;
        -webkit-transition-property: transform, height;
        transition-property: transform, height;
        -webkit-transition-property: -webkit-transform, height, -webkit-transform;
        -webkit-transition-property: transform, height, -webkit-transform;
        transition-property: transform, height, -webkit-transform;
    }

    .swiper-container-3d {
        -webkit-perspective: 1200px;
        -webkit-perspective: 1200px;
        -moz-perspective: 1200px;
        -ms-perspective: 1200px;
        perspective: 1200px;
    }

    .swiper-container-3d .swiper-wrapper, .swiper-container-3d .swiper-slide, .swiper-container-3d .swiper-slide-shadow-left, .swiper-container-3d .swiper-slide-shadow-right, .swiper-container-3d .swiper-slide-shadow-top, .swiper-container-3d .swiper-slide-shadow-bottom, .swiper-container-3d .swiper-cube-shadow {
        -webkit-transform-style: preserve-3d;
        -webkit-transform-style: preserve-3d;
        -ms-transform-style: preserve-3d;
        transform-style: preserve-3d;
    }

    .swiper-container-3d .swiper-slide-shadow-left, .swiper-container-3d .swiper-slide-shadow-right, .swiper-container-3d .swiper-slide-shadow-top, .swiper-container-3d .swiper-slide-shadow-bottom {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        pointer-events: none;
        z-index: 10;
    }

    .swiper-container-3d .swiper-slide-shadow-left {
        background-image: -webkit-gradient(linear, right top, left top, from(rgba(0, 0, 0, 0.5)), to(rgba(0, 0, 0, 0)));
        background-image: -webkit-linear-gradient(right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: -o-linear-gradient(right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: linear-gradient(to left, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
    }

    .swiper-container-3d .swiper-slide-shadow-right {
        background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, 0.5)), to(rgba(0, 0, 0, 0)));
        background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: -o-linear-gradient(left, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
    }

    .swiper-container-3d .swiper-slide-shadow-top {
        background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(0, 0, 0, 0.5)), to(rgba(0, 0, 0, 0)));
        background-image: -webkit-linear-gradient(bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: -o-linear-gradient(bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: linear-gradient(to top, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
    }

    .swiper-container-3d .swiper-slide-shadow-bottom {
        background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0.5)), to(rgba(0, 0, 0, 0)));
        background-image: -webkit-linear-gradient(top, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: -o-linear-gradient(top, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
        background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0));
    }

    .swiper-container-wp8-horizontal, .swiper-container-wp8-horizontal > .swiper-wrapper {
        -ms-touch-action: pan-y;
        touch-action: pan-y;
    }

    .swiper-container-wp8-vertical, .swiper-container-wp8-vertical > .swiper-wrapper {
        -ms-touch-action: pan-x;
        touch-action: pan-x;
    }

    .swiper-button-prev, .swiper-button-next {
        position: absolute;
        top: 50%;
        width: 27px;
        height: 44px;
        margin-top: -22px;
        z-index: 10;
        cursor: pointer;
        background-size: 27px 44px;
        background-position: center;
        background-repeat: no-repeat;
    }

    .swiper-button-prev.swiper-button-disabled, .swiper-button-next.swiper-button-disabled {
        opacity: 0.35;
        cursor: auto;
        pointer-events: none;
    }

    .swiper-button-prev {
        left: 0px;
        right: unset;
    }

    .swiper-container-rtl .swiper-button-next {
        left: 28px;
        right: unset;
    }

    .swiper-button-next {
        right: 28px;
        left: unset;
    }

    .swiper-container-rtl .swiper-button-prev {
        right: 0px;
        left: unset;
    }

    .swiper-button-next:before, .swiper-container-rtl .swiper-button-prev:before {
        color: #7e859b;
        font-family: icomoon;
        content: '\e905';
        font-size: 28px;
        background-color: #fff;
        padding: 15px 20px;
        opacity: 0.5;
        -webkit-transition: 0.1s all ease-in;
        transition: 0.1s all ease-in;
    }

    .swiper-button-next:hover::before, .swiper-container-rtl .swiper-button-prev:hover::before {
        opacity: 1;
        color: #ffffff;
    }

    .swiper-button-prev:before, .swiper-container-rtl .swiper-button-next:before {
        color: #7e859b;
        font-family: icomoon;
        content: '\e904';
        font-size: 28px;
        background-color: #fff;
        padding: 15px 20px;
        opacity: 0.5;
        -webkit-transition: 0.1s all ease-in;
        transition: 0.1s all ease-in;
    }

    .swiper-button-prev:hover::before, .swiper-container-rtl .swiper-button-next:hover::before {
        opacity: 1;
        color: #ffffff;
    }

    .swiper-button-prev.swiper-button-white, .swiper-container-rtl .swiper-button-next.swiper-button-white {
        background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E");
    }

    .swiper-button-next.swiper-button-white, .swiper-container-rtl .swiper-button-prev.swiper-button-white {
        background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E");
    }

    .swiper-button-prev.swiper-button-black, .swiper-container-rtl .swiper-button-next.swiper-button-black {
        background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E");
    }

    .swiper-button-next.swiper-button-black, .swiper-container-rtl .swiper-button-prev.swiper-button-black {
        background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E");
    }

    .swiper-pagination {
        position: static;
        text-align: center;
        -webkit-transition: 300ms opacity;
        -o-transition: 300ms opacity;
        -webkit-transition: 300ms opacity;
        transition: 300ms opacity;
        -webkit-transform: translate3d(0, 0, 0);
        -webkit-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
        z-index: 10;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        padding: 10px 0;
    }

    .swiper-pagination.swiper-pagination-hidden {
        opacity: 0;
    }

    .swiper-pagination-fraction, .swiper-pagination-custom, .swiper-container-horizontal > .swiper-pagination-bullets {
        bottom: 10px;
        left: 0;
        width: 100%;
    }

    .swiper-pagination-bullets-dynamic {
        overflow: hidden;
        font-size: 0;
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        transform: scale(0.33);
        position: relative;
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active {
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev {
        -webkit-transform: scale(0.66);
        -ms-transform: scale(0.66);
        -webkit-transform: scale(0.66);
        -ms-transform: scale(0.66);
        transform: scale(0.66);
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev {
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        transform: scale(0.33);
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next {
        -webkit-transform: scale(0.66);
        -ms-transform: scale(0.66);
        -webkit-transform: scale(0.66);
        -ms-transform: scale(0.66);
        transform: scale(0.66);
    }

    .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next {
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        -webkit-transform: scale(0.33);
        -ms-transform: scale(0.33);
        transform: scale(0.33);
    }

    .swiper-pagination-bullet {
        width: 22px;
        height: 3px;
        display: inline-block;
        border-radius: 0;
        background: #e2e5f1;
        opacity: 1;
    }

    button.swiper-pagination-bullet {
        border: none;
        margin: 0;
        padding: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    .swiper-pagination-clickable .swiper-pagination-bullet {
        cursor: pointer;
    }

    .swiper-pagination-bullet-active {
        opacity: 1;
        background: #feee00;
    }

    .swiper-container-vertical > .swiper-pagination-bullets {
        right: 10px;
        top: 50%;
        -webkit-transform: translate3d(0px, -50%, 0);
        -webkit-transform: translate3d(0px, -50%, 0);
        -ms-transform: translate3d(0px, -50%, 0);
        transform: translate3d(0px, -50%, 0);
    }

    .swiper-container-vertical > .swiper-pagination-bullets .swiper-pagination-bullet {
        margin: 6px 0;
        display: block;
    }

    .swiper-container-vertical > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic {
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        width: 8px;
    }

    .swiper-container-vertical > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
        display: inline-block;
        -webkit-transition: 200ms top, 200ms -webkit-transform;
        -webkit-transition: 200ms top, 200ms -webkit-transform;
        -webkit-transition: 200ms top, 200ms -webkit-transform;
        transition: 200ms top, 200ms -webkit-transform;
        -o-transition: 200ms transform, 200ms top;
        -webkit-transition: 200ms -webkit-transform, 200ms top;
        -webkit-transition: 200ms transform, 200ms top;
        transition: 200ms transform, 200ms top;
        -webkit-transition: 200ms -webkit-transform, 200ms top, 200ms -webkit-transform;
        -webkit-transition: 200ms transform, 200ms top, 200ms -webkit-transform;
        transition: 200ms transform, 200ms top, 200ms -webkit-transform;
    }

    .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
        margin: 0 4px;
    }

    .swiper-container-horizontal > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic {
        left: 50%;
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%);
        white-space: nowrap;
    }

    .swiper-container-horizontal > .swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet {
        -webkit-transition: 200ms left, 200ms -webkit-transform;
        -webkit-transition: 200ms left, 200ms -webkit-transform;
        -webkit-transition: 200ms left, 200ms -webkit-transform;
        transition: 200ms left, 200ms -webkit-transform;
        -o-transition: 200ms transform, 200ms left;
        -webkit-transition: 200ms -webkit-transform, 200ms left;
        -webkit-transition: 200ms transform, 200ms left;
        transition: 200ms transform, 200ms left;
        -webkit-transition: 200ms -webkit-transform, 200ms left, 200ms -webkit-transform;
        -webkit-transition: 200ms transform, 200ms left, 200ms -webkit-transform;
        transition: 200ms transform, 200ms left, 200ms -webkit-transform;
    }

    .swiper-pagination-progressbar {
        background: rgba(0, 0, 0, 0.25);
        position: absolute;
    }

    .swiper-pagination-progressbar .swiper-pagination-progressbar-fill {
        background: #007aff;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        -webkit-transform: scale(0);
        -ms-transform: scale(0);
        -webkit-transform: scale(0);
        -ms-transform: scale(0);
        transform: scale(0);
        -webkit-transform-origin: left top;
        -ms-transform-origin: left top;
        -webkit-transform-origin: left top;
        -ms-transform-origin: left top;
        transform-origin: left top;
    }

    .swiper-container-rtl .swiper-pagination-progressbar .swiper-pagination-progressbar-fill {
        -webkit-transform-origin: right top;
        -ms-transform-origin: right top;
        -webkit-transform-origin: right top;
        -ms-transform-origin: right top;
        transform-origin: right top;
    }

    .swiper-container-horizontal > .swiper-pagination-progressbar {
        width: 100%;
        height: 4px;
        left: 0;
        top: 0;
    }

    .swiper-container-vertical > .swiper-pagination-progressbar {
        width: 4px;
        height: 100%;
        left: 0;
        top: 0;
    }

    .swiper-pagination-white .swiper-pagination-bullet-active {
        background: #ffffff;
    }

    .swiper-pagination-progressbar.swiper-pagination-white {
        background: rgba(255, 255, 255, 0.25);
    }

    .swiper-pagination-progressbar.swiper-pagination-white .swiper-pagination-progressbar-fill {
        background: #ffffff;
    }

    .swiper-pagination-black .swiper-pagination-bullet-active {
        background: #000000;
    }

    .swiper-pagination-progressbar.swiper-pagination-black {
        background: rgba(0, 0, 0, 0.25);
    }

    .swiper-pagination-progressbar.swiper-pagination-black .swiper-pagination-progressbar-fill {
        background: #000000;
    }

    .swiper-scrollbar {
        border-radius: 10px;
        position: relative;
        -ms-touch-action: none;
        background: rgba(0, 0, 0, 0.1);
    }

    .swiper-container-horizontal > .swiper-scrollbar {
        position: absolute;
        left: 1%;
        bottom: 3px;
        z-index: 50;
        height: 5px;
        width: 98%;
    }

    .swiper-container-vertical > .swiper-scrollbar {
        position: absolute;
        right: 3px;
        top: 1%;
        z-index: 50;
        width: 5px;
        height: 98%;
    }

    .swiper-scrollbar-drag {
        height: 100%;
        width: 100%;
        position: relative;
        background: rgba(0, 0, 0, 0.5);
        border-radius: 10px;
        left: 0;
        top: 0;
    }

    .swiper-scrollbar-cursor-drag {
        cursor: move;
    }

    .swiper-zoom-container {
        width: 100%;
        height: 100%;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: center;
    }

    .swiper-zoom-container > img, .swiper-zoom-container > svg, .swiper-zoom-container > canvas {
        max-width: 100%;
        max-height: 100%;
        -o-object-fit: contain;
        object-fit: contain;
    }

    .swiper-slide-zoomed {
        cursor: move;
    }

    .swiper-lazy-preloader {
        width: 42px;
        height: 42px;
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -21px;
        margin-top: -21px;
        z-index: 10;
        -webkit-transform-origin: 50%;
        -ms-transform-origin: 50%;
        -webkit-transform-origin: 50%;
        -ms-transform-origin: 50%;
        transform-origin: 50%;
        -webkit-animation: swiper-preloader-spin 1s steps(12, end) infinite;
        -webkit-animation: swiper-preloader-spin 1s steps(12, end) infinite;
        animation: swiper-preloader-spin 1s steps(12, end) infinite;
    }

    .swiper-lazy-preloader:after {
        display: block;
        content: '';
        width: 100%;
        height: 100%;
        background-position: 50%;
        background-size: 100%;
        background-repeat: no-repeat;
    }

    @-webkit-keyframes swiper-preloader-spin {
        100% {
            -webkit-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes swiper-preloader-spin {
        100% {
            -webkit-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes swiper-preloader-spin {
        100% {
            -webkit-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    .swiper-container .swiper-notification {
        position: absolute;
        left: 0;
        top: 0;
        pointer-events: none;
        opacity: 0;
        z-index: -1000;
    }

    .swiper-container-fade.swiper-container-free-mode .swiper-slide {
        -webkit-transition-timing-function: ease-out;
        -o-transition-timing-function: ease-out;
        -webkit-transition-timing-function: ease-out;
        transition-timing-function: ease-out;
    }

    .swiper-container-fade .swiper-slide {
        pointer-events: none;
        -webkit-transition-property: opacity;
        -o-transition-property: opacity;
        -webkit-transition-property: opacity;
        transition-property: opacity;
    }

    .swiper-container-fade .swiper-slide .swiper-slide {
        pointer-events: none;
    }

    .swiper-container-fade .swiper-slide-active, .swiper-container-fade .swiper-slide-active .swiper-slide-active {
        pointer-events: auto;
    }

    .swiper-container-cube {
        overflow: visible;
    }

    .swiper-container-cube .swiper-slide {
        pointer-events: none;
        -webkit-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        z-index: 1;
        visibility: hidden;
        -webkit-transform-origin: 0 0;
        -ms-transform-origin: 0 0;
        -webkit-transform-origin: 0 0;
        -ms-transform-origin: 0 0;
        transform-origin: 0 0;
        width: 100%;
        height: 100%;
    }

    .swiper-container-cube .swiper-slide .swiper-slide {
        pointer-events: none;
    }

    .swiper-container-cube.swiper-container-rtl .swiper-slide {
        -webkit-transform-origin: 100% 0;
        -ms-transform-origin: 100% 0;
        -webkit-transform-origin: 100% 0;
        -ms-transform-origin: 100% 0;
        transform-origin: 100% 0;
    }

    .swiper-container-cube .swiper-slide-active, .swiper-container-cube .swiper-slide-active .swiper-slide-active {
        pointer-events: auto;
    }

    .swiper-container-cube .swiper-slide-active, .swiper-container-cube .swiper-slide-next, .swiper-container-cube .swiper-slide-prev, .swiper-container-cube .swiper-slide-next + .swiper-slide {
        pointer-events: auto;
        visibility: visible;
    }

    .swiper-container-cube .swiper-slide-shadow-top, .swiper-container-cube .swiper-slide-shadow-bottom, .swiper-container-cube .swiper-slide-shadow-left, .swiper-container-cube .swiper-slide-shadow-right {
        z-index: 0;
        -webkit-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
    }

    .swiper-container-cube .swiper-cube-shadow {
        position: absolute;
        left: 0;
        bottom: 0px;
        width: 100%;
        height: 100%;
        background: #000;
        opacity: 0.6;
        -webkit-filter: blur(50px);
        -webkit-filter: blur(50px);
        filter: blur(50px);
        z-index: 0;
    }

    .swiper-container-flip {
        overflow: visible;
    }

    .swiper-container-flip .swiper-slide {
        pointer-events: none;
        -webkit-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        z-index: 1;
    }

    .swiper-container-flip .swiper-slide .swiper-slide {
        pointer-events: none;
    }

    .swiper-container-flip .swiper-slide-active, .swiper-container-flip .swiper-slide-active .swiper-slide-active {
        pointer-events: auto;
    }

    .swiper-container-flip .swiper-slide-shadow-top, .swiper-container-flip .swiper-slide-shadow-bottom, .swiper-container-flip .swiper-slide-shadow-left, .swiper-container-flip .swiper-slide-shadow-right {
        z-index: 0;
        -webkit-backface-visibility: hidden;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
    }

    .swiper-container-coverflow .swiper-wrapper {
        -ms-perspective: 1200px;
    }</style>
    <style id="__jsx-1481647590">footer.siteFooter {
        margin-top: 0 !important;
    }

    .staticNavContainer.jsx-1481647590 {
        padding: 32px 40px;
        border-top: 1px solid #e2e5f1;
    }

    @media only screen and (min-width: 992px) {
        .container.jsx-1481647590 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row-reverse;
            -ms-flex-direction: row-reverse;
            flex-direction: row-reverse;
            max-width: 1440px;
            margin: 0 auto;
        }

        .pageComponents.jsx-1481647590 {
            -webkit-flex: 1;
            -ms-flex: 1;
            flex: 1;
            min-width: 0;
        }

        .pageComponents.jsx-1481647590 .siteWidthContainer.jsx-1481647590 {
            min-width: 0;
        }

        .staticNavContainer.jsx-1481647590 {
            border-top: 0;
            width: 200px;
            padding: 0;
            background-color: #fff;
            border-right: 1px solid #e2e5f1;
        }

        html[dir='rtl'] .staticNavContainer.jsx-1481647590 {
            border-left: 1px solid #e2e5f1;
            border-right: 0;
        }
    }

    .componentArea.jsx-1481647590 {
        overflow: hidden;
        background-position: top center;
        background-size: cover;
        position: relative;
    }

    .componentAreaBgImage.jsx-1481647590 {
        position: absolute;
        background-position: top center;
        background-repeat: no-repeat;
        background-size: cover;
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }

    .componentArea.jsx-1481647590:nth-child(odd) {
        background-color: #fff;
    }

    .componentRow.jsx-1481647590 {
        padding: 40px 0 45px;
        position: relative;
    }

    .noPaddingComponentRow.jsx-1481647590, .noPaddingComponentRow.jsx-1481647590 .siteWidthContainer {
        padding: 0;
    }

    .recommendedProducts.jsx-1481647590 .siteWidthContainer, .productCarousel.jsx-1481647590 .siteWidthContainer {
        padding: 0;
    }

    .multiComponentArea.jsx-1481647590 .componentRow.jsx-1481647590 + .componentRow.jsx-1481647590 {
        padding-top: 15px;
        margin-top: -45px;
        position: relative;
    }

    .multiComponentArea.jsx-1481647590 .componentRow.textBannerVariation.jsx-1481647590 + .componentRow.jsx-1481647590 {
        margin-top: -34px;
    }

    .multiComponentArea.jsx-1481647590 .componentRow.textLinks.jsx-1481647590 + .componentRow.jsx-1481647590 {
        margin-top: -31px;
    }

    .multiComponentArea.jsx-1481647590 .componentRow.bannerModuleStripTimer.jsx-1481647590 + .componentRow.jsx-1481647590, .multiComponentArea.jsx-1481647590 .componentRow.bannerModuleStrip.jsx-1481647590 + .componentRow.jsx-1481647590 {
        margin-top: 15px;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab-list {
        padding: 19px 0;
        margin-bottom: -15px;
        white-space: nowrap;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        position: relative;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab-list:after {
        content: '';
        background-color: #e2e5f1;
        position: absolute;
        left: 0;
        bottom: 0;
        right: 0;
        height: 1px;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab {
        padding: 6px 0 20px;
        font-size: 1.083333rem;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        font-weight: bold;
        display: inline-block;
        color: #7e859b;
        cursor: pointer;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab + .react-tabs__tab {
        margin-left: 30px;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab--selected {
        color: #ffffff;
        position: relative;
    }

    .componentArea.jsx-1481647590 .react-tabs__tab--selected:after {
        content: '';
        background-color: #ffffff;
        position: absolute;
        left: 0;
        bottom: 0;
        right: 0;
        height: 3px;
        z-index: 10;
    }

    code.jsx-1481647590 {
        background-color: #404553;
        border-radius: 4px;
        display: block;
        color: #fff;
        padding: 15px 15px 20px;
        overflow-y: auto;
    }</style>
    <style id="__jsx-1280509355">a.banner.jsx-1280509355 {
        position: relative;
        display: block;
    }

    .textContainer.jsx-1280509355 {
        margin: 8px 0 0;
    }

    p.title.jsx-1280509355 {
        font-weight: 600;
        font-size: 1.25rem;
    }

    p.summary.jsx-1280509355 {
        margin-top: 2px;
    }

    .cta.jsx-1280509355 {
        font-weight: bold;
        font-size: 0.916666rem;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        padding: 6px 12px;
        border: 1px solid;
        background-color: #fff;
        margin-top: 10px;
        display: inline-block;
    }

    .featureBanner.jsx-1280509355 p.title.jsx-1280509355 {
        font-weight: bold;
        font-size: 2em;
        margin-top: -1px;
    }

    .featureBanner.jsx-1280509355 p.summary.jsx-1280509355 {
        font-size: 1.083333rem;
    }

    .bannerModuleOverlay.jsx-1280509355 .textContainer.jsx-1280509355, .bgImageBanner.jsx-1280509355 .textContainer.jsx-1280509355 {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin: 0;
        color: #fff;
    }

    .bgImageBanner.jsx-1280509355 {
        height: 100%;
        background-size: cover;
        background-position: center center;
    }

    ul.supportLinks.jsx-1280509355 {
        margin-top: 8px;
        font-size: 1.083333rem;
        color: #7e859b;
    }

    ul.supportLinks.jsx-1280509355 li.emphasis.jsx-1280509355 a.jsx-1280509355 {
        color: #ffffff;
        font-weight: bold;
        text-transform: uppercase;
        font-size: 1rem;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        padding-top: 14px;
    }

    ul.supportLinks.jsx-1280509355 li.jsx-1280509355 a.jsx-1280509355 {
        padding: 10px 0;
        display: block;
    }

    ul.supportLinks.jsx-1280509355 li.jsx-1280509355:last-child a.jsx-1280509355 {
        padding-bottom: 0;
    }</style>
    <style id="__jsx-16778177">.bannerWrapper.jsx-16778177 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-left: -15px;
        margin-bottom: -15px;
    }

    .bannerContainer.jsx-16778177 {
        padding-left: 15px;
        padding-bottom: 15px;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        box-sizing: border-box;
    }

    @media only screen and (max-width: 1199px) {
        .splitBannerRow.jsx-16778177 {
            -webkit-flex: 0 0 20% !important;
            -ms-flex: 0 0 20% !important;
            flex: 0 0 20% !important;
        }
    }

    .textBannerVariation .bannerWrapper.jsx-16778177 {
        margin-bottom: -21px;
    }

    .textBannerVariation .bannerContainer.jsx-16778177 {
        padding-bottom: 21px;
    }

    .bannerCarousel.jsx-16778177 {
        margin-left: -25px;
        margin-right: -25px;
        padding: 0 25px;
        white-space: nowrap;
        overflow-x: auto;
        overflow-y: hidden;
        -webkit-overflow-scrolling: touch;
    }

    .bannerSlide.jsx-16778177 {
        width: 18%;
        display: inline-block;
        vertical-align: top;
        white-space: initial;
    }

    .bannerSlide.jsx-16778177 + .bannerSlide.jsx-16778177 {
        margin-left: 15px;
    }

    html[dir='rtl'] .bannerSlide.jsx-16778177 + .bannerSlide.jsx-16778177 {
        margin-right: 15px;
        margin-left: 0;
    }

    @media only screen and (min-width: 992px) {
        .bannerCarousel.jsx-16778177 {
            white-space: normal;
            overflow: visible;
            margin: 0 0 -15px -15px;
            padding: 0;
        }

        .bannerSlide.jsx-16778177 {
            width: 20%;
            padding-left: 15px;
            padding-bottom: 15px;
            box-sizing: border-box;
        }

        .bannerSlide.jsx-16778177 + .bannerSlide.jsx-16778177 {
            margin-left: 0;
        }
    }

    @media only screen and (min-width: 1200px) {
        .bannerSlide.jsx-16778177 {
            width: 12.5%;
        }
    }

    .bannerModuleStripTimer.jsx-16778177 {
        position: relative;
    }

    .bannerModuleStripTimer.jsx-16778177 .timerWrapper.jsx-16778177 {
        position: absolute;
        pointer-events: none;
        direction: ltr;
    }

    .timerWrapper-bottom-left.jsx-16778177 {
        position: absolute;
        top: 80%;
        left: 3%;
        -webkit-transform: translate(-3%, -80%);
        -ms-transform: translate(-3%, -80%);
        transform: translate(-3%, -80%);
    }

    .timerWrapper-bottom-right.jsx-16778177 {
        position: absolute;
        top: 80%;
        left: 95%;
        -webkit-transform: translate(-95%, -80%);
        -ms-transform: translate(-95%, -80%);
        transform: translate(-95%, -80%);
    }

    .timerWrapper-top-left.jsx-16778177 {
        position: absolute;
        top: 5%;
        left: 3%;
        -webkit-transform: translate(-3%, -5%);
        -ms-transform: translate(-3%, -5%);
        transform: translate(-3%, -5%);
    }

    .timerWrapper-top-right.jsx-16778177 {
        position: absolute;
        top: 5%;
        left: 95%;
        -webkit-transform: translate(-95%, -5%);
        -ms-transform: translate(-95%, -5%);
        transform: translate(-95%, -5%);
    }

    .timerWrapper-center.jsx-16778177 {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .timerWrapper-middle-right.jsx-16778177 {
        position: absolute;
        top: 50%;
        left: 95%;
        -webkit-transform: translate(-95%, -50%);
        -ms-transform: translate(-95%, -50%);
        transform: translate(-95%, -50%);
    }

    .timerWrapper-middle-left.jsx-16778177 {
        position: absolute;
        top: 50%;
        left: 5%;
        -webkit-transform: translate(-5%, -50%);
        -ms-transform: translate(-5%, -50%);
        transform: translate(-5%, -50%);
    }

    .timerWrapper-top-center.jsx-16778177 {
        position: absolute;
        top: 5%;
        left: 50%;
        -webkit-transform: translate(-50%, -5%);
        -ms-transform: translate(-50%, -5%);
        transform: translate(-50%, -5%);
    }

    .timerWrapper-bottom-center.jsx-16778177 {
        position: absolute;
        top: 80%;
        left: 50%;
        -webkit-transform: translate(-50%, -80%);
        -ms-transform: translate(-50%, -80%);
        transform: translate(-50%, -80%);
    }

    .bannerModuleStripTimer.jsx-16778177 .timerContainer.jsx-16778177 {
        background-size: cover;
        color: #fff;
        font-weight: bold;
        padding: 5px;
        font-size: 16px;
    }

    .bannerGroup.jsx-16778177 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        margin-left: -15px;
    }

    .bannerGroup.jsx-16778177 .bannerWrapper.jsx-16778177 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        padding-left: 15px;
    }</style>
    <style id="__jsx-3400163498">header.jsx-3400163498 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: flex-start;
        -webkit-box-align: flex-start;
        -ms-flex-align: flex-start;
        align-items: flex-start;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-bottom: 14px;
        margin-top: -4px;
    }

    h3.jsx-3400163498 {
        font-weight: bold;
        font-size: 1.5rem;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    header.jsx-3400163498 a.jsx-3400163498 {
        font-weight: bold;
        color: #ffffff;
        font-size: 0.916666rem;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        padding: 6px 12px;
        margin-left: 10px;
        background-color: #3866df;
    }

    html[dir='rtl'] header.jsx-3400163498 a.jsx-3400163498 {
        margin-right: 10px;
        margin-left: 0;
    }

    .imageContainer.jsx-3400163498 {
        max-width: 190px;
    }

    header.jsx-3400163498 img.jsx-3400163498 {
        display: block;
        max-width: 100%;
    }</style>
    <style id="__jsx-564649128">.wrapper.jsx-564649128 {
        position: relative;
        background-color: #fff;
        color: #404553;
        text-align: initial;
        height: 100%;
        padding: 8px 8px 12px;
        box-sizing: border-box;
    }

    .product.jsx-564649128 {
        height: 100%;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    .navigatingLoader.jsx-564649128 {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 9;
        background: #ffffff8a;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    .imageContainer.jsx-564649128 {
        margin: -8px -8px 6px;
        position: relative;
        overflow: hidden;
    }

    .imageContainer.jsx-564649128 .limitedDealContainer.jsx-564649128 {
        position: absolute;
        bottom: 8px;
        left: 0;
        right: 0;
        text-align: center;
    }

    .detailsContainer.jsx-564649128 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    .name.jsx-564649128 {
        margin-bottom: 4px;
        font-size: 1rem;
    }

    div.name.jsx-564649128 {
        min-height: 32px;
    }

    div.name > div {
        overflow: inherit !important;
    }

    .discountFulfilmentRow.jsx-564649128 .discountFulfilmentContent.jsx-564649128 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 4px;
    }

    .discountFulfilmentRow.jsx-564649128 .fulfilmentContainer.jsx-564649128 {
        margin-right: 6px;
    }

    html[dir='rtl'] .discountFulfilmentRow.jsx-564649128 .fulfilmentContainer.jsx-564649128 {
        margin-left: 6px;
        margin-right: 0;
    }

    .wishlistCta.jsx-564649128 {
        position: absolute;
        top: 5px;
        right: 5px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        z-index: 1;
        -webkit-transition: color ease-in 0.2s;
        transition: color ease-in 0.2s;
        color: #7e859b;
        font-size: 13px;
        background-color: rgba(247, 247, 250, 0.9);
        border-radius: 100%;
        width: 28px;
        height: 28px;
        box-sizing: border-box;
        padding-top: 1px;
    }

    .wishlistCta[disabled].jsx-564649128 {
        pointer-events: none;
    }

    .wishlistCta.jsx-564649128:hover {
        opacity: 0.7;
    }

    .wishlistCta.jsx-564649128 .addedWishListCtr.jsx-564649128 {
        color: #ce0f0f;
    }

    html[dir='rtl'] .wishlistCta.jsx-564649128 {
        left: 5px;
        right: auto;
    }

    .gridView.jsx-564649128 .productTagContainer.jsx-564649128 {
        position: absolute;
        top: 10px;
        left: 0;
        right: 36px;
        font-size: 0;
    }

    .gridView.jsx-564649128 .discountFulfilmentRow.jsx-564649128 {
        margin-top: auto;
    }

    html[dir='rtl'] .gridView.jsx-564649128 .productTagContainer.jsx-564649128 {
        right: 0;
        left: 36px;
    }

    .wrapper.listView.jsx-564649128 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    html[dir='ltr'] .wrapper.listView.jsx-564649128 {
        padding: 12px 16px 12px 0;
    }

    html[dir='rtl'] .wrapper.listView.jsx-564649128 {
        padding: 12px 0 12px 16px;
    }

    .wrapper.listView.jsx-564649128 .product.jsx-564649128 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .wrapper.listView.jsx-564649128 .imageContainer.jsx-564649128 {
        -webkit-flex: 0 0 30%;
        -ms-flex: 0 0 30%;
        flex: 0 0 30%;
        max-width: 130px;
        margin: -12px 0;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 {
        margin-left: 15px;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .plpSpecifications.jsx-564649128 {
        margin-bottom: 5px;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .plpSpecifications.jsx-564649128 ul.jsx-564649128 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        list-style-type: none;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .plpSpecifications.jsx-564649128 ul.jsx-564649128::before {
        content: '';
        width: 0.5em;
        height: 0.5em;
        background-color: #c4c4c4;
        float: left;
        margin: 0.3em 0.8em 0.3em 0em;
        border-radius: 50%;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .plpSpecifications.jsx-564649128 ul.jsx-564649128 li.jsx-564649128 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .specKey.jsx-564649128 {
        font-size: 1.08333rem;
        color: #7e859b;
    }

    .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 .specValue.jsx-564649128 {
        font-size: 1.08333rem;
        color: #404553;
        margin-left: 5px;
    }

    .wrapper.listView.jsx-564649128 .productTagContainer.jsx-564649128 {
        margin-bottom: 6px;
        font-size: 0;
    }

    .wrapper.listView.jsx-564649128 .brand.jsx-564649128 {
        margin-bottom: 1px;
        color: #7e859b;
    }

    .wrapper.listView.jsx-564649128 .name.jsx-564649128 {
        color: #404553;
        font-weight: 600;
        margin-bottom: 8px;
        font-size: 1.125rem;
    }

    .wrapper.listView.jsx-564649128 .limitedDealContainer.jsx-564649128 {
        margin-top: 12px;
    }

    html[dir='rtl'] .wrapper.listView.jsx-564649128 .detailsContainer.jsx-564649128 {
        margin-right: 15px;
        margin-left: 0;
    }

    html[dir='rtl'] .plpSpecifications.jsx-564649128 {
        margin-right: -10px;
        margin-left: 0px;
    }

    html[dir='rtl'] .plpSpecifications.jsx-564649128 .specKey.jsx-564649128 {
        margin-right: 5px;
        margin-left: 0px;
    }

    html[dir='rtl'] .plpSpecifications.jsx-564649128 .specValue.jsx-564649128 {
        margin-right: 5px;
        margin-left: 0px;
    }

    .wrapper.lockedProduct.jsx-564649128 .productTagContainer.jsx-564649128, .wrapper.lockedProduct.jsx-564649128 .priceRow.jsx-564649128, .wrapper.lockedProduct.jsx-564649128 .discountFulfilmentRow.jsx-564649128 {
        z-index: 3;
    }

    .wrapper.lockedProduct.jsx-564649128 .priceRow.jsx-564649128 {
        position: relative;
        color: #fff;
    }

    .wrapper.lockedProduct.jsx-564649128 .priceRow.jsx-564649128 .sellingPrice .currency, .wrapper.lockedProduct.jsx-564649128 .priceRow.jsx-564649128 .preReductionPrice {
        color: #fff;
    }

    .wrapper.lockedProduct.jsx-564649128 .discountTag .bg {
        background-color: #d8efd2;
        opacity: 1;
    }

    .wrapper.lockedProduct.jsx-564649128 .discountFulfilmentRow.jsx-564649128 {
        position: relative;
    }

    .wrapper.listView.lockedProduct.jsx-564649128 .ctaContainer.jsx-564649128 {
        visibility: hidden;
    }

    .ctaContainer.jsx-564649128 {
        border-left: 1px solid #e2e5f1;
        padding-left: 16px;
        margin-left: 16px;
        width: 132px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    .ctaContainer.jsx-564649128 button.jsx-564649128 {
        padding: 0 12px;
        font-weight: bold;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.02rem;
        -moz-letter-spacing: 0.02rem;
        -ms-letter-spacing: 0.02rem;
        letter-spacing: 0.02rem;
        font-size: 0.833333333333333rem;
        height: 32px;
    }

    .ctaContainer.jsx-564649128 button.jsx-564649128 + button.jsx-564649128 {
        margin-top: 10px;
    }

    .ctaContainer.jsx-564649128 button.jsx-564649128:hover {
        opacity: 0.65;
    }

    .ctaContainer.jsx-564649128 button[disabled].jsx-564649128 {
        pointer-events: none;
    }

    .ctaContainer.jsx-564649128 .btnATC.jsx-564649128 {
        background-color: #ffffff;
        color: #fff;
        margin-top: 5px;
    }

    .ctaContainer.jsx-564649128 .btnATC.added.jsx-564649128 {
        background-color: #f7f7fa;
        color: #b2bbd2;
    }

    .ctaContainer.jsx-564649128 .btnWishlist.jsx-564649128 {
        color: #ffffff;
        border: 1px solid;
    }

    .ctaContainer.jsx-564649128 .btnWishlist.added.jsx-564649128 {
        color: #b2bbd2;
    }

    html[dir='rtl'] .ctaContainer.jsx-564649128 {
        border-left: none;
        border-right: 1px solid #e2e5f1;
        padding-right: 16px;
        padding-left: 0;
        margin-right: 16px;
        margin-left: 0;
    }</style>
    <style id="__jsx-194994594">.placeholderContainer.jsx-194994594 .brand.jsx-194994594 {
        max-width: 100px;
        padding-bottom: 0;
        margin-bottom: 6px;
    }

    .placeholderContainer.jsx-194994594 .name.jsx-194994594 {
        max-width: 200px;
        margin-bottom: 10px;
    }

    .placeholderContainer.jsx-194994594 .preReductionPrice.jsx-194994594 {
        display: block;
        max-width: 80px;
        margin-bottom: 4px;
    }

    .placeholderContainer.jsx-194994594 .sellingPrice.jsx-194994594 {
        display: block;
        max-width: 120px;
    }

    .placeholderContainer.jsx-194994594 .placeholder.jsx-194994594 {
        background-color: #f7f7fa;
        background-image: linear-gradient(90deg, #f7f7fa, #e2e5f1, #f7f7fa);
        background-size: 200px 100%;
        background-repeat: no-repeat;
        -webkit-animation: loadingAnimation-jsx-194994594 1.5s infinite;
        animation: loadingAnimation-jsx-194994594 1.5s infinite;
    }

    @-webkit-keyframes loadingAnimation-jsx-194994594 {
        0% {
            background-position: -200px 0;
        }
        100% {
            background-position: calc(200px + 100%) 0;
        }
    }

    @keyframes loadingAnimation-jsx-194994594 {
        0% {
            background-position: -200px 0;
        }
        100% {
            background-position: calc(200px + 100%) 0;
        }
    }</style>
    <style id="__jsx-4285006856">.container.jsx-4285006856 {
        max-width: 1440px;
        margin: auto;
    }

    .headerContainer.jsx-4285006856 {
        padding: 0 25px;
    }

    .productContainer.jsx-4285006856 {
        height: auto;
        width: 166px;
        padding: 2px;
        box-sizing: border-box;
    }

    .productContainer.jsx-4285006856:first-child {
        margin-left: 25px;
    }

    .productContainer.jsx-4285006856:last-child {
        margin-right: 25px;
    }

    .swiper-button-prev, .swiper-button-next {
        display: none;
    }

    @media only screen and (min-width: 992px) {
        .productContainer.jsx-4285006856:first-child {
            margin-left: 0;
        }

        .productContainer.jsx-4285006856:last-child {
            margin-right: 0;
        }

        .carouselWrapper.jsx-4285006856 {
            padding: 0 25px;
        }

        .swiper-button-prev, .swiper-button-next {
            display: block;
            z-index: 999999;
        }

        .swiper-button-prev.swiper-button-disabled, .swiper-button-next.swiper-button-disabled {
            display: none;
        }
    }

    @media only screen and (min-width: 1200px) {
        .headerContainer.jsx-4285006856, .carouselWrapper.jsx-4285006856 {
            padding: 0 45px;
        }
    }</style>
    <style id="__jsx-1442578048">.contactLinksModule.jsx-1442578048 {
        background-color: #F7F9FE;
        border-bottom: 1px solid #e2e5f1;
        padding: 20px 0 25px;
    }

    .contactLinksModule.jsx-1442578048 p.title.jsx-1442578048 {
        font-size: 1.5rem;
        font-weight: 700;
    }

    .contactLinksModule.jsx-1442578048 p.summary.jsx-1442578048 {
        font-size: 1rem;
        color: #7e859b;
        display: block;
        margin: 2px 0 20px;
    }

    .contactLinksModule.jsx-1442578048 .topText.jsx-1442578048 {
        -webkit-flex: 0 0 32%;
        -ms-flex: 0 0 32%;
        flex: 0 0 32%;
    }

    .contactLinksModule.jsx-1442578048 ul.jsx-1442578048 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }

    .contactLinksModule.jsx-1442578048 li.jsx-1442578048 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .contactLinksModule.jsx-1442578048 a.jsx-1442578048 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        text-align: initial;
        -webkit-transition: opacity 0.2s ease-in-out;
        transition: opacity 0.2s ease-in-out;
    }

    .contactLinksModule.jsx-1442578048 a.jsx-1442578048:hover {
        opacity: 0.6;
    }

    .contactLinksModule.jsx-1442578048 a.jsx-1442578048 p.jsx-1442578048 {
        margin-left: 10px;
        font-weight: 600;
        font-size: 1.416666rem;
        color: #404553;
    }

    .contactLinksModule.jsx-1442578048 a.jsx-1442578048 p.whatsapp.jsx-1442578048 {
        line-height: 36px;
    }

    html[dir='rtl'] .contactLinksModule.jsx-1442578048 a.jsx-1442578048 p.jsx-1442578048 {
        margin-left: unset;
        margin-right: 10px;
    }

    html[dir='rtl'] .contactLinksModule.jsx-1442578048 a.jsx-1442578048 p.whatsapp.jsx-1442578048 {
        direction: ltr !important;
    }

    .contactLinksModule.jsx-1442578048 .heading.jsx-1442578048 {
        display: block;
        font-size: 0.83333rem;
        font-weight: 600;
        text-transform: uppercase;
        color: #7e859b;
    }

    .contactLinksModule.jsx-1442578048 i {
        color: white;
        background: #005E00 !important;
        width: 36px;
        height: 36px;
        border: 1px solid #e2e5f1;
        background-color: #fff;
        box-sizing: border-box;
        border-radius: 100%;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        font-size: 15px;
    }

    @media only screen and (min-width: 992px) {
        .contactLinksModule.jsx-1442578048 .siteWidthContainer.jsx-1442578048 {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: space-around;
            -webkit-justify-content: space-around;
            -ms-flex-pack: space-around;
            justify-content: space-around;
            -webkit-align-item: center;
            -webkit-box-align-item: center;
            -ms-flex-align-item: center;
            align-item: center;
        }

        .contactLinksModule.jsx-1442578048 ul.jsx-1442578048 {
            -webkit-flex: 1 1 0%;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            margin-left: 80px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .contactLinksModule.jsx-1442578048 p.summary.jsx-1442578048 {
            margin-bottom: unset;
        }

        .contactLinksModule.jsx-1442578048 .topText.jsx-1442578048 {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .contactLinksModule.jsx-1442578048 p.summary.jsx-1442578048 {
            margin-bottom: unset;
        }
    }</style>
    <style id="__jsx-3729605677">.appLinksModule.jsx-3729605677 {
        margin-bottom: 35px;
    }

    .appLinksModule.jsx-3729605677 p.title.jsx-3729605677 {
        font-weight: 700;
        margin-bottom: 10px;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        text-transform: uppercase;
    }

    .badgesContainer.jsx-3729605677 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    .appLinksModule.jsx-3729605677 a.jsx-3729605677 {
        display: block;
        width: 135px;
        height: 40px;
        background: url(assets/images/sprite.png) no-repeat -2000px 0 / 187px auto;;
        text-indent: 100%;
        white-space: nowrap;
        overflow: hidden;
        -webkit-transition: opacity 0.2s ease-in-out;
        transition: opacity 0.2s ease-in-out;
    }

    .appLinksModule.jsx-3729605677 a.android.jsx-3729605677 {
        background-size: 184px auto;
    }

    .appLinksModule.jsx-3729605677 a.jsx-3729605677:hover {
        opacity: 0.6;
    }

    .appLinksModule.jsx-3729605677 a.jsx-3729605677 + a.jsx-3729605677 {
        margin-left: 10px;
    }

    html[dir='rtl'] a.jsx-3729605677 + a.jsx-3729605677 {
        margin-right: 10px;
        margin-left: 0;
    }

    .appLinksModule.jsx-3729605677 a.ios.jsx-3729605677 {
        background-position: 0 0;
    }

    .appLinksModule.jsx-3729605677 a.android.jsx-3729605677 {
        background-position: 0 -84px;
    }

    html[dir='rtl'] a.android.jsx-3729605677 {
        background-position: 0 -124px !important;
    }

    html[dir='rtl'] a.ios.jsx-3729605677 {
        background-position: 0 -45px !important;
    }</style>
    <style id="__jsx-1844971943">.socialLinksModule.jsx-1844971943 {
        margin-bottom: 35px;
    }

    .socialLinksModule.jsx-1844971943 p.title.jsx-1844971943 {
        font-weight: 700;
        margin-bottom: 10px;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        text-transform: uppercase;
    }

    .socialLinksModule.jsx-1844971943 li.jsx-1844971943 {
        display: inline-block;
        margin: 0 10px;
    }

    .socialLinksModule.jsx-1844971943 a.jsx-1844971943 {
        width: 45px;
        height: 45px;
        font-size: 22px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        background-color: #005E00;
        border-radius: 100%;
        -webkit-transition: opacity 0.2s ease-in-out;
        transition: opacity 0.2s ease-in-out;
        color: white;
    }

    .socialLinksModule.jsx-1844971943 a.jsx-1844971943:hover {
        opacity: 0.6;
    }</style>
    <style id="__jsx-722193732">ul.jsx-722193732 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }

    li.jsx-722193732 {
        margin: 0 0.2rem;
        border: 1px solid #e2e5f1;
        border-radius: 2px;
        width: 50px;
        height: 30px;
        background: url(assets/images/sprite.png) no-repeat -2000px 0 / 187px auto;;
        background-color: #fff;
        text-indent: 100%;
        white-space: nowrap;
        overflow: hidden;
    }

    li.jsx-722193732 + li.jsx-722193732 {
        margin-left: 15px;
    }

    html[dir='rtl'] li.jsx-722193732 + li.jsx-722193732 {
        margin-right: 15px;
        margin-left: 0;
    }

    li.amex.jsx-722193732 {
        background-position: -137px 0;
    }

    li.mastercard.jsx-722193732 {
        background-position: -137px -32px;
    }

    li.visa.jsx-722193732 {
        background-position: -137px -64px;
    }

    li.cod.jsx-722193732 {
        background-position: -137px -96px;
    }

    li.mada.jsx-722193732 {
        background-position: -68px -220px;
    }

    html[dir='rtl'] li.cod.jsx-722193732 {
        background-position: -137px -128px;
    }</style>
    <style id="__jsx-4225977845">.siteFooter.jsx-4225977845 {
        text-align: center;
        border-top: 1px solid #e2e5f1;
        background-color: #fff;
    }

    .primaryRow.jsx-4225977845 {
        padding: 40px 0;
    }

    .moduleRow.jsx-4225977845 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        margin: 50px 0 -20px 0;
    }

    .moduleRow.jsx-4225977845 > div {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }

    .supportRow.jsx-4225977845 {
        background-color: #f7f7fa;
        border-top: 1px solid #e2e5f1;
        padding: 20px 0;
    }

    .copyright.jsx-4225977845 {
        color: #b2bbd2;
    }

    .categoryLinksModule.jsx-4225977845 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        text-align: initial;
        margin-left: -20px;
    }

    .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        margin-left: 20px;
    }

    .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845 .uspContainer.jsx-4225977845 .link.jsx-4225977845 {
        font-size: 1rem;
        font-weight: normal;
        text-transform: capitalize;
    }

    .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845 .uspContainer.jsx-4225977845 .link.jsx-4225977845:hover {
        -webkit-text-decoration: underline;
        text-decoration: underline;
    }

    .categoryLinksModule.jsx-4225977845 p.jsx-4225977845 {
        font-weight: bold;
        text-transform: uppercase;
        -webkit-letter-spacing: 0.2px;
        -moz-letter-spacing: 0.2px;
        -ms-letter-spacing: 0.2px;
        letter-spacing: 0.2px;
        font-size: 1.1rem;
    }

    .categoryLinksModule.jsx-4225977845 ul.jsx-4225977845 {
        margin-top: -4px;
    }

    .categoryLinksModule.jsx-4225977845 li.jsx-4225977845 {
        margin-top: 16px;
    }

    .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845:nth-child(4), .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845:nth-child(5) {
        display: none;
    }

    @media only screen and (min-width: 992px) {
        .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845:nth-child(4), .categoryLinksModule.jsx-4225977845 .category.jsx-4225977845:nth-child(5) {
            display: block;
        }
    }

    .supportLinks.jsx-4225977845 {
        margin: 10px 0 18px;
        color: #7e859b;
    }

    .supportLinks.jsx-4225977845 li.jsx-4225977845 {
        display: inline-block;
    }

    .supportLinks.jsx-4225977845 li.jsx-4225977845:hover {
        -webkit-text-decoration: underline;
        text-decoration: underline;
    }

    .supportLinks.jsx-4225977845 a {
        display: block;
        padding: 7px 10px 0;
    }

    .supportInfoWrapper.jsx-4225977845 {
        background-color: #f7f7fa;
        border-top: 1px solid #e2e5f1;
    }

    .supportInfoContainer.jsx-4225977845 {
        padding: 20px 15px 18px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        max-width: 1440px;
        margin: 0 auto;
    }

    .copyright.jsx-4225977845 {
        font-size: 1rem;
    }

    .topCopyright.jsx-4225977845 {
        display: none;
    }

    .customClass {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        box-shadow: rgba(0, 0, 0, 0.45) 0px 0px 12px !important;
        opacity: 1 !important;
        z-index: 99999 !important;
        border: 1px solid #e2e5f1;
        max-width: 180px;
    }

    @media only screen and (min-width: 992px) {
        .supportInfoContainer.jsx-4225977845 {
            padding: 0 45px;
        }
    }

    @media only screen and (min-width: 1200px) {
        .bottomCopyright.jsx-4225977845 {
            display: none;
        }

        .topCopyright.jsx-4225977845 {
            display: block;
            margin-right: 20px;
        }

        html[dir='rtl'] .topCopyright.jsx-4225977845 {
            margin-right: unset;
            margin-left: 20px;
        }

        .supportInfoContainer.jsx-4225977845 {
            padding: 12px 45px;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }

        .supportLinks.jsx-4225977845 {
            margin-left: auto;
            margin-right: 115px;
        }
    }</style>
    <style id="__jsx-1282087270">#wrapper.jsx-1282087270 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        min-height: 100vh;
        background: #f7f7fa;
    }

    #content.jsx-1282087270 {
        -webkit-flex: 1;
        -ms-flex: 1;
        flex: 1;
        width: 100%;
    }</style>
    <style id="__jsx-78397880">
    @font-face {
        font-family: 'icomoon';
        src: url("assets/fonts/icomoon.eot?qoxotp");
        src: url("assets/fonts/icomoon.eot?qoxotp#iefix") format("embedded-opentype"), url("assets/fonts/icomoon.ttf?qoxotp") format("truetype"), url("assets/fonts/icomoon.woff?qoxotp") format("woff");
        font-weight: normal;
        font-style: normal;
    }

    html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }

    article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
        display: block;
    }

    body {
        line-height: 1;
        font-family: 'Proxima Nova', sans-serif;
    }

    ol, ul {
        list-style: none;
    }

    blockquote, q {
        quotes: none;
    }

    blockquote:before, blockquote:after, q:before, q:after {
        content: '';
        content: none;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    html, body {
        font-family: Proxima Nova, Helvetica Neue, Arial, sans-serif;
        line-height: 1.3;
        color: #404553;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    html {
        font-size: 12px;
    }

    html[dir=rtl] body {
        font-family: Cairo, sans-serif;
    }

    html[dir=rtl] * {
        -webkit-letter-spacing: 0 !important;
        -moz-letter-spacing: 0 !important;
        -ms-letter-spacing: 0 !important;
        letter-spacing: 0 !important;
    }

    body {
        min-height: 100vh;
        background-color: #fff;
    }

    .siteWidthContainer {
        padding: 0 25px;
        margin: 0 auto;
        min-width: 768px;
        max-width: 1440px;
        box-sizing: border-box;
    }

    @media only screen and (min-width: 1200px) {
        .siteWidthContainer {
            padding: 0 45px;
        }
    }

    strong, b {
        font-weight: bold;
    }

    a {
        -webkit-text-decoration: none;
        text-decoration: none;
        cursor: pointer;
        color: inherit;
    }

    a:focus, button:focus, input:focus {
        outline: 0;
    }

    img {
        max-width: 100%;
        display: block;
    }

    input, button, textarea, select {
        font: inherit;
        color: inherit;
        padding: 0;
        margin: 0;
        border: 0;
        box-sizing: border-box;
        border-radius: 0;
    }

    input, textarea, select {
        border: 1px solid #e2e5f1;
        font-size: 16px;
    }

    input::-ms-clear {
        display: none;
    }

    button {
        background-color: transparent;
        cursor: pointer;
    }

    button[disabled] {
        cursor: default;
    }

    body .Select-control, body .Select.is-open > .Select-control, body .Select-menu-outer {
        border-color: #e2e5f1;
        border-radius: 0;
    }

    #launcher {
        left: auto !important;
        right: 0 !important;
    }

    html[dir=rtl] #launcher {
        left: 0px !important;
        right: auto !important;
    }

    .zopim {
        right: 15px !important;
        left: auto !important;
    }

    html[dir=rtl] .zopim {
        left: 15px !important;
        right: unset !important;
    }

    [class^="icon-"], [class*=" icon-"] {
        font-family: 'icomoon' !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .icon-timer-clock:before {
        content: "\e953";
    }

    .icon-flame:before {
        content: "\e954";
    }

    .icon-non-returnable:before {
        content: "\e951";
    }

    .icon-pencil:before {
        content: "\e947";
    }

    .icon-aeroplane-o:before {
        content: "\e900";
    }

    .icon-aeroplane:before {
        content: "\e901";
    }

    .icon-angle-down:before {
        content: "\e902";
    }

    .icon-angle-up:before {
        content: "\e903";
    }

    .icon-angle-left:before {
        content: "\e904";
    }

    .icon-angle-right:before {
        content: "\e905";
    }

    .icon-arrow-up:before {
        content: "\e906";
    }

    .icon-arrow-left:before {
        content: "\e907";
    }

    .icon-arrow-right:before {
        content: "\e908";
    }

    .icon-arrow-down:before {
        content: "\e909";
    }

    .icon-bell:before {
        content: "\e90a";
    }

    .icon-suitcase:before {
        content: "\e90b";
    }

    .icon-calendar:before {
        content: "\e90c";
    }

    .icon-caret-down:before {
        content: "\e90d";
    }

    .icon-caret-up:before {
        content: "\e90e";
    }

    .icon-cart-o:before {
        content: "\e90f";
    }

    .icon-cart:before {
        content: "\e910";
    }

    .icon-checkbox-checked:before {
        content: "\e911";
    }

    .icon-checkbox-unchecked:before {
        content: "\e912";
    }

    .icon-cross:before {
        content: "\e913";
    }

    .icon-tick:before {
        content: "\e914";
    }

    .icon-coins:before {
        content: "\e915";
    }

    .icon-credit-card-o:before {
        content: "\e916";
    }

    .icon-credit-card:before {
        content: "\e917";
    }

    .icon-envelope-o:before {
        content: "\e918";
    }

    .icon-privacy-off:before {
        content: "\e919";
    }

    .icon-privacy-on:before {
        content: "\e91a";
    }

    .icon-sort:before {
        content: "\e91b";
    }

    .icon-flag:before {
        content: "\e91c";
    }

    .icon-globe:before {
        content: "\e91d";
    }

    .icon-grid-o:before {
        content: "\e91e";
    }

    .icon-grid:before {
        content: "\e91f";
    }

    .icon-headphones:before {
        content: "\e920";
    }

    .icon-heart-o:before {
        content: "\e921";
    }

    .icon-heart:before {
        content: "\e922";
    }

    .icon-home-o:before {
        content: "\e923";
    }

    .icon-home:before {
        content: "\e924";
    }

    .icon-info-o:before {
        content: "\e925";
    }

    .icon-info:before {
        content: "\e926";
    }

    .icon-lines:before {
        content: "\e927";
    }

    .icon-lock-o:before {
        content: "\e928";
    }

    .icon-lock:before {
        content: "\e929";
    }

    .icon-map-marker-o:before {
        content: "\e92a";
    }

    .icon-map-marker:before {
        content: "\e92b";
    }

    .icon-minus:before {
        content: "\e92c";
    }

    .icon-noon:before {
        content: "\e92d";
    }

    .icon-notepad:before {
        content: "\e92e";
    }

    .icon-phone-o:before {
        content: "\e92f";
    }

    .icon-phone:before {
        content: "\e930";
    }

    .icon-plus:before {
        content: "\e931";
    }

    .icon-power:before {
        content: "\e932";
    }

    .icon-radio-checked:before {
        content: "\e933";
    }

    .icon-radio-unchecked:before {
        content: "\e934";
    }

    .icon-dots:before {
        content: "\e935";
    }

    .icon-search:before {
        content: "\e936";
    }

    .icon-share:before {
        content: "\e937";
    }

    .icon-filter:before {
        content: "\e938";
    }

    .icon-star-o:before {
        content: "\e939";
    }

    .icon-star:before {
        content: "\e93a";
    }

    .icon-timer:before {
        content: "\e93b";
    }

    .icon-book:before {
        content: "\e93c";
    }

    .icon-broadcast:before {
        content: "\e93d";
    }

    .icon-help:before {
        content: "\e93e";
    }

    .icon-help-o:before {
        content: "\e93f";
    }

    .icon-user-sign-in:before {
        content: "\e940";
    }

    .icon-user:before {
        content: "\e941";
    }

    .icon-users:before {
        content: "\e942";
    }

    .icon-warning-o:before {
        content: "\e943";
    }

    .icon-warning:before {
        content: "\e944";
    }

    .icon-medal-o:before {
        content: "\e945";
    }

    .icon-trash:before {
        content: "\e946";
    }

    .icon-truck:before {
        content: "\e948";
    }

    .icon-returns:before {
        content: "\e949";
    }

    .icon-shield:before {
        content: "\e94a";
    }

    .icon-wallet:before {
        content: "\e94b";
    }

    .icon-offer-tag:before {
        content: "\e94c";
    }

    .icon-return-arrow:before {
        content: "\e94d";
    }

    .icon-medal:before {
        content: "\e94e";
    }

    .icon-list:before {
        content: "\e94f";
    }

    .icon-envelope:before {
        content: "\e950";
    }

    .icon-twitter:before {
        content: "\f099";
    }

    .icon-facebook:before {
        content: "\f09a";
    }

    .icon-google-plus:before {
        content: "\f0d5";
    }

    .icon-instagram:before {
        content: "\f16d";
    }

    .icon-linkedin:before {
        content: "\eaca";
    }

    .icon-whatsapp:before {
        content: "\ea93";
    }

    .icon-gift:before {
        content: "\e952";
    }

    .icon-claims:before {
        content: "\e955";
    }

    .price.jsx-4251264678 .sellingPrice.jsx-4251264678 {
        margin-right: 4px;}
    .sellingPrice.jsx-4251264678 {
        font-weight: bold;
        font-size: 1.33333rem;
    }

    .price.jsx-4251264678 .sellingPrice.jsx-4251264678 .currency.medium {
        font-size: 80%;
    }

    .price.jsx-4251264678 .sellingPrice.jsx-4251264678 .currency {
        font-size: 60%;
        font-weight: normal;
        color: rgb(126, 133, 155);
    }

    .price.jsx-4251264678 .sellingPrice.jsx-4251264678 .value {
        margin-left: -1px;
    }

    .price.jsx-4251264678 .preReductionPrice.jsx-4251264678 {
        color: rgb(178, 187, 210);
    }

    .preReductionPrice.jsx-4251264678 {
        color: rgb(126, 133, 155);
        text-decoration: line-through;
        font-size: 0.916666rem;
    }

    

    </style>

</head>
<body>
<div id="__next">
    <div id="wrapper" class="jsx-1282087270">
        <div class="headroom-wrapper">
            <div class="headroom headroom--unfixed">
                <header class="jsx-127861163 siteHeader">
                    <div class="jsx-127861163 supportBar">
                        <div class="jsx-127861163 siteWidthContainer">
                            <div class="jsx-127861163 localeSettings">
                                <div class="jsx-127861163 languageSelectLink"><a class="jsx-127861163 languageButton">العربية</a>
                                </div>
                                <div class="jsx-127861163 countryWrapper">
                                    <div class="jsx-522397250 container"><a class="jsx-522397250 countryWrapper"><i
                                            class="jsx-522397250 countryFlag ae"></i>Ship to<!-- -->  <span
                                            class="jsx-522397250 shippingCountry">UAE</span><i
                                            class="icon icon-caret-down " style="color:;font-size:10"></i></a></div>
                                </div>
                            </div>
                            <div class="jsx-127861163"></div>
                            <ul class="jsx-127861163 usps">
                                <li class="jsx-127861163"><span class="jsx-127861163 uspIcon returns"></span>Free &amp;
                                    Easy Returns
                                </li>
                                <li class="jsx-127861163"><span class="jsx-127861163 uspIcon secure"></span>Best Deals
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="jsx-127861163 siteWidthContainer container ">
                        <div class="jsx-1079652234">
                            <button aria-label="Menu" class="jsx-1079652234 menuTrigger"><i class="icon icon-lines "
                                                                                            style="color:"></i></button>
                            <div aria-hidden="true" class="jsx-1079652234 menuPanelContainer ">
                                <div class="jsx-1079652234 scrim"></div>
                                <div class="jsx-1079652234 menuPanel">
                                    <header class="jsx-1079652234">
                                        <div class="jsx-1079652234 topCtr">
                                            <div class="jsx-1079652234 leftCtr">
                                                <button aria-label="Close" class="jsx-1079652234 panelClose"><i
                                                        class="icon icon-cross " style="color:"></i></button>
                                                <div class="jsx-1079652234 logoContainer"><img
                                                        src=""
                                                        alt="noon"
                                                        style="display:block;max-width:100%;max-height:100%;width:auto"/>
                                                </div>
                                            </div>
                                            <div class="jsx-1079652234 rgtCtr"><p class="jsx-1079652234 greeting">Hi, We
                                                Are noon</p></div>
                                        </div>
                                    </header>
                                    <div class="jsx-696087597 iconMenuContainer">
                                        <ul class="jsx-696087597">
                                            <li class="jsx-696087597">
                                                <button class="jsx-696087597"><i class="icon icon-home-o "
                                                                                 style="color:"></i><span
                                                        class="jsx-696087597 menuTitle">Home</span></button>
                                            </li>
                                            <li class="jsx-696087597">
                                                <button aria-label="Sign In" class="jsx-696087597"><i
                                                        class="icon icon-user-sign-in " style="color:"></i><span
                                                        class="jsx-696087597 menuTitle">Sign In</span></button>
                                            </li>
                                            <li class="jsx-696087597">
                                                <button aria-label="Sign Up" class="jsx-696087597"><i
                                                        class="icon icon-user " style="color:"></i><span
                                                        class="jsx-696087597 menuTitle">Sign Up</span></button>
                                            </li>
                                            <li class="jsx-696087597">
                                                <button class="jsx-696087597">
                                                    <div class="jsx-696087597 counterIcon"><i class="icon icon-cart-o "
                                                                                              style="color:"></i></div>
                                                    <span class="jsx-696087597 menuTitle">Cart</span></button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="jsx-409027264 container">
                                        <div class="jsx-409027264"><p class="jsx-409027264 title">All Categories</p>
                                            <ul class="jsx-409027264">
                                                <li class="jsx-409027264">
                                                    <button aria-label="Electronics &amp; Mobiles"
                                                            class="jsx-409027264">Electronics &amp; Mobiles<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Beauty &amp; Health" class="jsx-409027264">
                                                        Beauty &amp; Health<i class="icon icon-angle-right "
                                                                              style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Fashion" class="jsx-409027264">Fashion<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Home &amp; Kitchen" class="jsx-409027264">Home
                                                        &amp; Kitchen<i class="icon icon-angle-right "
                                                                        style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Sports &amp; Fitness" class="jsx-409027264">
                                                        Sports &amp; Fitness<i class="icon icon-angle-right "
                                                                               style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Toys &amp; Games" class="jsx-409027264">Toys
                                                        &amp; Games<i class="icon icon-angle-right " style="color:"></i>
                                                    </button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Baby Products" class="jsx-409027264">Baby
                                                        Products<i class="icon icon-angle-right " style="color:"></i>
                                                    </button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Grocery" class="jsx-409027264">Grocery<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Automotive" class="jsx-409027264">Automotive<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Tools &amp; Home Improvement"
                                                            class="jsx-409027264">Tools &amp; Home Improvement<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Books" class="jsx-409027264">Books<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Pet Supplies" class="jsx-409027264">Pet Supplies<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Office Supplies" class="jsx-409027264">Office
                                                        Supplies<i class="icon icon-angle-right " style="color:"></i>
                                                    </button>
                                                </li>
                                                <li class="jsx-409027264">
                                                    <button aria-label="Music, Movies &amp; TV Shows"
                                                            class="jsx-409027264">Music, Movies &amp; TV Shows<i
                                                            class="icon icon-angle-right " style="color:"></i></button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="jsx-1773613676 supportMenuContainer">
                                        <ul class="jsx-1773613676">
                                            <li class="jsx-1773613676">
                                                <div class="jsx-2715901464">
                                                    <div class="jsx-2715901464 panelTrigger">
                                                        <button aria-label="Country" class="jsx-1773613676"><i
                                                                class="icon icon-globe " style="color:"></i><span
                                                                class="jsx-1773613676 menuTitle">Country</span><span
                                                                class="jsx-1773613676 currentOption">UAE<i
                                                                class="jsx-1773613676 countryFlag uae"></i></span>
                                                        </button>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         class="jsx-2715901464 optionsPanelContainer ">
                                                        <div class="jsx-2715901464 scrim"></div>
                                                        <div class="jsx-2715901464 optionsPanel ">
                                                            <header class="jsx-2715901464"><h3 class="jsx-2715901464">
                                                                Country</h3>
                                                                <button aria-label="Close" class="jsx-2715901464"><i
                                                                        class="icon icon-cross " style="color:"></i>
                                                                </button>
                                                            </header>
                                                            <ul class="jsx-2715901464">
                                                                <li class="jsx-2715901464">
                                                                    <button aria-label="UAE"
                                                                            class="jsx-2715901464 selected"><span
                                                                            class="jsx-2715901464 optionWrapper"><span
                                                                            class="jsx-2715901464">UAE</span></span><i
                                                                            class="icon icon-radio-checked "
                                                                            style="color:"></i></button>
                                                                </li>
                                                                <li class="jsx-2715901464">
                                                                    <button aria-label="KSA" class="jsx-2715901464 ">
                                                                        <span class="jsx-2715901464 optionWrapper"><span
                                                                                class="jsx-2715901464">KSA</span></span><i
                                                                            class="icon icon-radio-unchecked "
                                                                            style="color:"></i></button>
                                                                </li>
                                                                <li class="jsx-2715901464">
                                                                    <button aria-label="Egypt" class="jsx-2715901464 ">
                                                                        <span class="jsx-2715901464 optionWrapper"><span
                                                                                class="jsx-2715901464">Egypt</span></span><i
                                                                            class="icon icon-radio-unchecked "
                                                                            style="color:"></i></button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="jsx-1773613676">
                                                <button aria-label="Language" class="jsx-1773613676"><i
                                                        class="icon icon-flag " style="color:"></i><span
                                                        class="jsx-1773613676 menuTitle">Language</span><span
                                                        class="jsx-1773613676 currentOption language">العربية</span>
                                                </button>
                                            </li>
                                            <li class="jsx-1773613676"><a href="https://wa.me/97180038888?text=Hello"
                                                                          target="_blank" rel="noopener noferrer"
                                                                          class="jsx-1773613676"><i
                                                    class="icon icon-whatsapp " style="color:"></i><span
                                                    class="jsx-1773613676 menuTitle">WhatsApp Us</span></a></li>
                                            <li class="jsx-1773613676 half"><a href="https://help.abiyat.com/hc/en-us"
                                                                               class="jsx-1773613676"><i
                                                    class="icon icon-info-o " style="color:"></i><span
                                                    class="jsx-1773613676 menuTitle">Help</span></a></li>
                                            <li class="jsx-1773613676 half"><a href="tel:800-38888"
                                                                               class="jsx-1773613676"><i
                                                    class="icon icon-phone-o " style="color:"></i><span
                                                    class="jsx-1773613676 menuTitle">Contact</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="jsx-127861163 logoContainer" style="display:block;max-width:100%;max-height:100%;width:auto;font-size: 25px;
    font-weight: 900;"><a href="index.html" id="title_home"><!--<img
                                src=""
                                alt="noon" style="display:block;max-width:100%;max-height:100%;width:auto"/>-->Abiyat</a></span>
                        <div class="jsx-127861163 searchWrapper">
                            <div class="jsx-3997850843 container">
                                <div class="jsx-3997850843 inputContainer"><input type="text" id="searchBar"
                                                                                  autoComplete="off"
                                                                                  placeholder="What are you looking for?"
                                                                                  value=""
                                                                                  aria-label="What are you looking for?"
                                                                                  class="jsx-3997850843"/>
                                    <div class="jsx-3997850843 searchTrigger"><i class="icon icon-search "
                                                                                 style="color:"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="jsx-127861163 userWrapper">
                            <div class="jsx-127861163 userLoading"><i class="icon icon-user " style="color:"></i>
                                <div style="transition:opacity 300ms ease-in-out;opacity:0">
                                    <div class="jsx-127861163 counterLoader secondary">
                                        <div style="position:absolute;top:-6px;right:-7px;min-height:false"
                                             class="jsx-3230834092 loaderWrapper">
                                            <div style="width:31px;height:31px" class="jsx-3230834092 loader"><img
                                                    src="assets/images/loader-white.gif"
                                                    alt="loader" style="height:31px"
                                                    class="jsx-3230834092 loaderImage"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a title="Cart" class="cartLink"><span class="jsx-127861163 cartTitle">Cart</span>
                            <div class="jsx-127861163 cartIconContainer"><i class="icon icon-cart-o cartIcon"
                                                                            style="color:"></i>
                                <div style="transition:opacity 300ms ease-in-out;opacity:0">
                                    <div class="jsx-127861163 counterLoader">
                                        <div style="position:absolute;top:-6px;right:-7px;min-height:false"
                                             class="jsx-3230834092 loaderWrapper">
                                            <div style="width:31px;height:31px" class="jsx-3230834092 loader"><img
                                                    src="assets/images/loader-white.gif"
                                                    alt="loader" style="height:31px"
                                                    class="jsx-3230834092 loaderImage"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a></div>
                    <div class="jsx-127861163 desktopNavContainer">
                        <div class="jsx-127861163 siteWidthContainer">
                            <div class="jsx-127861163 desktopNav">
                                <div class="jsx-150789449 container">
                                    <button aria-label="All Categories" class="jsx-150789449 menuTrigger">All Categories<i
                                            class="icon icon-caret-down " style="color:"></i></button>
                                </div>
                                <div class="jsx-1187918855 mainCategories">
                                    <ul class="jsx-1187918855 nav"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </div>
        <div id="content" class="jsx-1282087270">
            <div class="jsx-1282087270 pageWrapper">
                <div class="jsx-1481647590 container">
                    <div class="jsx-1481647590 pageComponents">

                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow bannerSlider noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-785064993 container">
                                        <div class="jsx-785064993 slide"><a href="">
                                            <div class="jsx-2714670158">
                                                <div class="jsx-785064993"><img
                                                        src="assets/cms/pages/20200927/a2358cc424a65780198d14fe9bebbb03/en_slider-01.png"
                                                        alt="" class="jsx-785064993"/></div>
                                            </div>
                                        </a></div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:25px;padding-bottom:10px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355"><a href="bds-fash-sep.html">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background:#ffffff" class="jsx-1481647590 componentArea ">
                            <div style="padding-top:40px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/fashion">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/fashion" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/home-appliances">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/home-appliances" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/home-kitchen">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/home-kitchen" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/baby-sept-sale">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/baby-sept-sale" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/beauty">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/beauty" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/sports-outdoors">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/sports-outdoors" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/fragrance-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/fragrance-store" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/beauty-boutique">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/beauty-boutique" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/toys">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/toys" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/electronics">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/electronics" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/mobiles">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/mobiles" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="/uae-en/grocery">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 128%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/categories/en_cat-module-10.png"
                                                                    alt="/grocery" class="jsx-1280509355"></div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:15px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="fashion.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="baby.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="home-kitchen.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="home-appliances.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="electronics.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="mobiles.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="beauty.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="sports-outdoors.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="fragrance-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="beauty-boutique">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="toys">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 8.333333333333332%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="grocery">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow recommendedProducts">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Recommended For
                                                You</h3></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
<!--                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a-->
<!--                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">-->
<!--                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">-->
<!--                                                            <div style="padding-bottom:136.375%"-->
<!--                                                                 class="jsx-2714670158 container undefined">-->
<!--                                                                <div class="jsx-2714670158 mediaContainer"><img-->
<!--                                                                        src="assets/nr/misc/product-image-default.jpg"-->
<!--                                                                        alt="productImageDefault"-->
<!--                                                                        class="jsx-564649128 jsx-194994594"/></div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p-->
<!--                                                                class="jsx-564649128 jsx-194994594 brand placeholder">-->
<!--                                                             </p>-->
<!--                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">-->
<!--                                                                 -->
<!--                                                            </div>-->
<!--                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span-->
<!--                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span-->
<!--                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                    </a></div>-->
<!--                                                   -->
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer" style="width: 173px; margin-right: 8px;"><div class="jsx-564649128 wrapper gridView "><a href="/uae-en/refurbished-iphone-7-with-facetime-black-32gb-4g-lte/N19857683A/p?o=b05863677b1225ae" class="jsx-564649128 product"><div class="jsx-564649128 imageContainer"><div class="jsx-2714670158 container undefined loaded" style="padding-bottom: 136.375%;"><div class="jsx-2714670158 mediaContainer"><img src="https://k.nooncdn.com/t_desktop-thumbnail-v2/v1584209321/N19857683A_1.jpg" alt="productBoxImg_v1584209321/N19857683A_1"></div></div></div><div class="jsx-564649128 detailsContainer"><div class="jsx-564649128 productTagContainer"><p class="jsx-2027916868 productTag" style="background-color: rgb(56, 102, 223);">10% off max 75 AED! Use Code: NOON10</p></div><div class="jsx-564649128 name"><div style="overflow: hidden;"><div><span>Apple Refurbished - iPhone 7 With FaceTime Black… </span></div></div></div><div class="jsx-564649128 priceRow"><p class="jsx-4251264678 price"><span class="jsx-4251264678 sellingPrice"><span><span class="currency medium">AED</span> <span class="value">649.00</span></span></span><span class="jsx-4251264678 preReductionPrice"><span><span class="currency null">AED</span> <span class="value">1109</span></span></span></p></div><div class="jsx-564649128 discountFulfilmentRow"><div class="jsx-564649128 discountFulfilmentContent"><div class="jsx-564649128 fulfilmentContainer"><div><div class="jsx-193278340 container"><img src="https://k.nooncdn.com/s/app/2019/noon-bigalog/9c7149677942537511bcfbcb76797c615187d21a/static/images/noon-express-en.png" alt="noon-express" class="jsx-193278340 fbn"></div></div></div><span class="jsx-2467480612 discountTag"><span class="jsx-2467480612 bg"></span><span class="jsx-2467480612 value">41% Off</span></span></div></div></div></a></div></div>

                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="garnier.html">
                                                    <div class="jsx-1280509355 banner bannerModule featureBanner">
                                                        <div class="jsx-2714670158 container undefined loaded"
                                                             style="padding-bottom: 12.5%;">
                                                            <div class="jsx-2714670158 mediaContainer"><img
                                                                    src="assets/images/media_containers/mc-01.gif"
                                                                    alt="/the-dubai-mall-store" class="jsx-1280509355">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed ;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355"><a href="loyalty-learn-more">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>



                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bogo-sept-FA_03.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="home-sale-sept-15-ae">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="appliances-15-ae-sfc">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="oakley-bds-WE_04.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-frag-10-sep-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Huge Savings On
                                                Best Sellers</h3><a href="sept-dc-ae22d0?f[is_fbn]=1"
                                                                    class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="sportswear-all">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-tops-megadeal-FA_03">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="megadeal-sfc-FA_03">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="eyewear-watches-md-bds-sep-ae-EW_04">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerSlider noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-785064993 container">
                                        <div class="jsx-785064993 slide"><a href="ipl-2020">
                                            <div class="jsx-2714670158">
                                                <div class="jsx-785064993"><img
                                                        src="assets/cms/pages/20200917/e8fcaf95ebe91518791a2e92427625f5/en_banner-01.png"
                                                        alt="/ipl-2020" class="jsx-785064993"/></div>
                                            </div>
                                        </a></div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Health Care
                                                Essentials</h3><a href="health-measures-ae" class="jsx-3400163498">VIEW
                                                ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="casio/watches-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="ray_ban/eyewear-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="tommy_hilfiger/watches-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="guess/eyewear-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="munchkin/baby-products">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="fashion-Nike-40-70-off">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="reebok-sale-dec">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="fashion-adidas-40-70-off">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bfs-mango-40-70.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="tools-and-home-improvement/black_decker/p-1558788ea?limit=150">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="p-11302">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="p-12776">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="p-14158">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 14.285714285714285%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="toys-and-games/intex">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:20px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="beauty-and-health/dyson81f5?sort[by]=new_arrivals&amp;sort[dir]=desc">
                                                    <div class="jsx-1280509355 banner bannerModule featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:10px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-fash-tops-FA_03">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-fash-bottoms-FA_03">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="fashion/women-31229/clothing-16021/dresses-176129432?f[price][min]=1&amp;f[price][max]=59">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="fashion/women-31229/clothing-16021/arabic-clothing-31230c8a0?f[price][min]=1&amp;f[price][max]=129">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="fashion-sandals-ramadan161b?f[price][min]=1&amp;f[price][max]=99">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="sneakers-40-80-off.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="watches-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="eyewear-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="fashion/luggage-and-bags/handbags-34070065a?f[price][min]=1&amp;f[price][max]=79">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:5px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">40%-80% Off
                                                Activewear</h3><a href="sportswear-all"
                                                                  class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background:#005e00" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/small-appliances/kettles/noonfav">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="pl-beddingcombo">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/kitchen-and-dining/cookware/noon_east5806?f[partner]=p_1&amp;f[price][min]=39&amp;f[price][max]=39">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="pl-towels-b1g1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498" style="color: white;">Bestsellers In
                                                noon Brands</h3><a href="noonfav"
                                                                   class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="mobiles.html">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="tablets">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="gaminghub">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="electronics-and-mobiles/television-and-video/televisions">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 20%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="wearables-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="laptops">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="electronics-and-mobiles/camera-and-photo-16165">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="acc-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 25%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="electronics-and-mobiles/portable-audio-and-video/headphones-2405622d0?f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Bestselling
                                                Electronics</h3><a href="top_ea_ae"
                                                                   class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:15px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Bestselling
                                                Mobiles</h3><a
                                                    href="electronics-and-mobiles/mobiles-and-accessories/mobiles-2090522d0?f[is_fbn]=1"
                                                     class="jsx-3400163498">VIEW ALL</a>
                                            </header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/small-appliances/irons-and-steamers6e52?f[price][min]=9&amp;f[price][max]=299&amp;f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/small-appliances/coffee-makers/appliances-deals22d0?f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/small-appliances/fryers/air-fryers/appliances-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/large-appliances/vacuums-and-floor-care/appliances-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/small-appliances/blenders/appliances-dealsf1c1?f[price][min]=18&amp;f[price][max]=199">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="home-and-kitchen/home-appliances-31235/large-appliances/heating-cooling-and-air-quality/household-fans/appliances-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Bestselling
                                                Appliances</h3><a href="automated-top-picks-appliances-ae"
                                                                  style="background-color:#d3f5ec"
                                                                  class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 50%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-06-menfrag-FR_08">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 50%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-06-womfrag-FR_08">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Signature scents
                                                at 50% off or more</h3><a href="fragrance-flat-50-off22d0?f[is_fbn]=1"
                                                                          class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bts-makeup-all-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-06-lips-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bds-06-eyes-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bts-skincare-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="bts-haircare-BE_07">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="grooming-styling-tools">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="baby-transport">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="baby-products/bathing-and-skin-care">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="feeding-nursing">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="toys-and-games/sports-and-outdoor-play/pools-and-water-fun">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="toys-and-games/games-18311/toys-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="toys-and-games/tricycles-scooters-and-wagonsb687?f[price][min]=4&amp;f[price][max]=199&amp;f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:25px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Bestselling Toys
                                                &amp; Games</h3><a href="automated-top-picks-toys-ae"
                                                                   class="jsx-3400163498">VIEW ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #edf4ed;opacity: 100%;" class="jsx-1481647590 componentArea multiComponentArea">
                            <div style="padding-top:0px;padding-bottom:0px"
                                 class="jsx-1481647590 componentRow bannerModuleStrip noPaddingComponentRow">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 100%"
                                                 class="jsx-16778177 bannerContainer bannerModuleStrip ">
                                                <div class="jsx-1280509355">
                                                    <div class="jsx-1280509355 banner bannerModuleStrip featureBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:0px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sports-and-outdoors/exercise-and-fitness/yoga-16328/sports-deals85b8?f[is_fbn]=1&amp;f[price][min]=10&amp;f[price][max]=99">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sports-and-outdoors/exercise-and-fitness/strength-training-equipment/sports-deals22d0.html?f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sportswear-all161b?f[price][min]=1&amp;f[price][max]=99">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sports-nutrition-deals22d0?f[is_fbn]=1">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sports-and-outdoors/exercise-and-fitness/cardio-training/sports-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 16.666666666666664%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a
                                                        href="sports-and-outdoors/cycling-16009/bikes/sports-deals">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 50%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="refurbished-sale">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 50%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="_renewed">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow productCarousel">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-4285006856 container">
                                        <div class="jsx-4285006856 headerContainer">
                                            <header class="jsx-3400163498"><h3 class="jsx-3400163498">Renewed By noon -
                                                Like New, For Less</h3><a href="renewed-sep" class="jsx-3400163498">VIEW
                                                ALL</a></header>
                                        </div>
                                        <div class="jsx-4285006856 carouselWrapper undefined">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                    <div class="swiper-slide jsx-4285006856 productContainer"><a
                                                            class="jsx-564649128 jsx-194994594 product gridView placeholderContainer">
                                                        <div class="jsx-564649128 jsx-194994594 imageContainer">
                                                            <div style="padding-bottom:136.375%"
                                                                 class="jsx-2714670158 container undefined">
                                                                <div class="jsx-2714670158 mediaContainer"><img
                                                                        src="assets/nr/misc/product-image-default.jpg"
                                                                        alt="productImageDefault"
                                                                        class="jsx-564649128 jsx-194994594"/></div>
                                                            </div>
                                                        </div>
                                                        <div class="jsx-564649128 jsx-194994594 detailsContainer"><p
                                                                class="jsx-564649128 jsx-194994594 brand placeholder">
                                                             </p>
                                                            <div class="jsx-564649128 jsx-194994594 name placeholder">
                                                                 
                                                            </div>
                                                            <div class="jsx-564649128 jsx-194994594 productPrice"><span
                                                                    class="jsx-564649128 jsx-194994594 preReductionPrice placeholder"> </span><span
                                                                    class="jsx-564649128 jsx-194994594 sellingPrice placeholder"> </span>
                                                            </div>
                                                        </div>
                                                    </a></div>
                                                </div>
                                                <div class="swiper-button-next"></div>
                                                <div class="swiper-button-prev"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top:35px;padding-bottom:35px"
                                 class="jsx-1481647590 componentRow bannerModule">
                                <div class="jsx-1481647590 siteWidthContainer">
                                    <div class="jsx-16778177 container">
                                        <div class="jsx-16778177 bannerWrapper">
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="book-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="stationery">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                            <div style="flex:0 0 33.33333333333333%"
                                                 class="jsx-16778177 bannerContainer bannerModule ">
                                                <div class="jsx-1280509355"><a href="automotive-store">
                                                    <div class="jsx-1280509355 banner bannerModule standardBanner">
                                                        <div style="padding-bottom:0%"></div>
                                                    </div>
                                                </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="jsx-4225977845 siteFooter">
            <div class="jsx-1442578048 contactLinksModule">
                <div class="jsx-1442578048 siteWidthContainer">
                    <div class="jsx-1442578048 topText"><p class="jsx-1442578048 title">We’re Always Here To Help</p>
                        <p class="jsx-1442578048 summary">Reach out to us through any of these support channels</p>
                    </div>
                    <ul class="jsx-1442578048">
                        <li class="jsx-1442578048"><a href="https://help.abiyat.com/hc/en-us" target="_blank"
                                                      rel="noopener noreferrer" class="jsx-1442578048"><i
                                class="icon icon-info-o " style="color:"></i>
                            <p class="jsx-1442578048"><span class="jsx-1442578048 heading">Help Center</span>help.abiyat.com
                            </p></a></li>
                        <li class="jsx-1442578048"><a href="mailto:care@abiyat.com" class="jsx-1442578048"><i
                                class="icon icon-envelope-o " style="color:"></i>
                            <p class="jsx-1442578048"><span class="jsx-1442578048 heading">Email Support</span>care@abiyat.com
                            </p></a></li>
                        <li class="jsx-1442578048"><a href="tel:800-38888" class="jsx-1442578048"><i
                                class="icon icon-phone-o " style="color:"></i>
                            <p class="jsx-1442578048"><span class="jsx-1442578048 heading">Phone Support</span>800-38888
                            </p></a></li>
                        <li class="jsx-1442578048"><a href="https://wa.me/97180038888?text=Hello" target="_blank"
                                                      rel="noopener noferrer" class="jsx-1442578048"><i
                                class="icon icon-whatsapp " style="color:"></i>
                            <p class="jsx-1442578048 whatsapp"><span class="jsx-1442578048 heading"></span>WhatsApp Us
                            </p></a></li>
                    </ul>
                </div>
            </div>
            <div class="jsx-4225977845 primaryRow">
                <div class="jsx-4225977845 siteWidthContainer">
                    <div class="jsx-4225977845 categoryLinksModule">
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">electronics</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="mobiles.html">mobiles</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="tablets">tablets</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="laptops">laptops</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="home-appliances.html">home appliances</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="cameras">camera, photo &amp; video</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="audio-video">televisions</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="headphones-24056">headphones</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="gaminghub">video games</a></span></li>
                            </ul>
                        </div>
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">fashion</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="fashion-women">women&#x27;s fashion</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="fashion-men">men&#x27;s fashion</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="girls-31223">girls&#x27; fashion</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="boys-31221">boys&#x27; fashion</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="watches-store">watches</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="jewellery-store">jewellery</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="handbags-16699">women&#x27;s handbags</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="eyewear-and-eyewear-accessories-19605">men&#x27;s eyewear</a></span></li>
                            </ul>
                        </div>
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">home and kitchen</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="bath-16182">bath</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="home-decor">home decor</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="kitchen-dining">kitchen &amp; dining</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="tools-and-home-improvement">Tools &amp; Home Improvement</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="audio-video">audio &amp; video</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="furniture-10180">furniture</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="home-and-kitchen/patio-lawn-and-garden">Patio, Lawn &amp; Garden</a></span>
                                </li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="pet-supplies">pet supplies</a></span></li>
                            </ul>
                        </div>
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">beauty</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="fragrance">fragrance</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="makeup-16142">make-up</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="hair-care">haircare</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="skin-care-16813">skincare</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="personal-care-16343">personal care </a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="tools-and-accessories">tools &amp; accessories</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="mens-grooming">men&#x27;s grooming</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="vitamins-and-dietary-supplements">vitamins</a></span></li>
                            </ul>
                        </div>
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">baby</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="strollers-18691">Strollers &amp; Prams</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="car-seats">car seats</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="feeding-16153">feeding</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="baby-products/bathing-and-skin-care">bathing &amp; skincare</a></span>
                                </li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="diapering">diapering</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="baby-products/clothing-shoes-and-accessories">baby clothing &amp; shoes</a></span>
                                </li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="toys-and-games/baby-and-toddler-toys">baby &amp; toddler toys</a></span>
                                </li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="baby-foods">baby foods</a></span></li>
                            </ul>
                        </div>
                        <div class="jsx-4225977845 category"><p class="jsx-4225977845">top brands</p>
                            <ul class="jsx-4225977845">
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="mothercare">mothercare</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="apple">apple</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="nike">nike</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="samsung">samsung</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="tefal">tefal</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="loreal">l&#x27;Oreal paris</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="skechers">skechers</a></span></li>
                                <li class="jsx-4225977845 uspContainer"><span class="jsx-4225977845 link"><a
                                        href="silsal">silsal</a></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="jsx-4225977845 moduleRow">
                        <div class="jsx-3729605677 appLinksModule"><p class="jsx-3729605677 title">Shop On The Go</p>
                            <div class="jsx-3729605677 badgesContainer"><a target="_blank"
                                                                           href=""
                                                                           rel="noopener noreferrer"
                                                                           aria-label="Visit the AppStore (opens in a new window)"
                                                                           class="jsx-3729605677 ios">ios App</a><a
                                    target="_blank"
                                    href=""
                                    rel="noopener noreferrer" aria-label="Visit the PlayStore (opens in a new window)"
                                    class="jsx-3729605677 android">Android App</a></div>
                        </div>
                        <div class="jsx-1844971943 socialLinksModule"><p class="jsx-1844971943 title">Connect With
                            Us</p>
                            <ul class="jsx-1844971943">
                                <li class="jsx-1844971943"><a href="" target="_blank"
                                                              rel="noopener noreferrer" title="Facebook"
                                                              class="jsx-1844971943"><i class="icon icon-facebook "
                                                                                        style="color:"></i></a></li>
                                <li class="jsx-1844971943"><a href="" target="_blank"
                                                              rel="noopener noreferrer" title="Twitter"
                                                              class="jsx-1844971943"><i class="icon icon-twitter "
                                                                                        style="color:"></i></a></li>
                                <li class="jsx-1844971943"><a href="" target="_blank"
                                                              rel="noopener noreferrer" title="Instagram"
                                                              class="jsx-1844971943"><i class="icon icon-instagram "
                                                                                        style="color:"></i></a></li>
                                <li class="jsx-1844971943"><a href=""
                                                              target="_blank" rel="noopener noreferrer"
                                                              title="Linked In" class="jsx-1844971943"><i
                                        class="icon icon-linkedin " style="color:"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="jsx-4225977845 supportInfoWrapper">
                <div class="jsx-4225977845 supportInfoContainer">
                    <div class="jsx-4225977845 copyright topCopyright"><a data-tip="top-copyright"
                                                                          data-for="top-copyright-tooltip"
                                                                          class="jsx-4225977845">© 2020 abiyat. All Rights
                        Reserved.</a>
                        <div class="__react_component_tooltip place-top type-dark " id="top-copyright-tooltip"
                             data-id="tooltip">&#x27;abiyat&#x27;, the &#x27;abiyat device&#x27;, &#x27;abiyat east&#x27;,
                            &#x27;east&#x27; and the &#x27;east device&#x27; are trade marks or registered trade marks
                            of abiyat AD Holdings LTD. in the UAE and other countries
                        </div>
                    </div>
                    <div class="jsx-722193732 container">
                        <ul class="jsx-722193732">
                            <li class="jsx-722193732 mastercard">mastercard</li>
                            <li class="jsx-722193732 visa">visa</li>
                            <li class="jsx-722193732 cod">cod</li>
                            <li class="jsx-722193732 amex">amex</li>
                        </ul>
                    </div>
                    <ul class="jsx-4225977845 supportLinks">
                        <li class="jsx-4225977845"><a target="_blank" rel="noopener noreferrer"
                                                      href="https://www.jobsatabiyat.com/"
                                                      class="jsx-4225977845">Careers</a></li>
                        <li class="jsx-4225977845"><a href="warranty-policy" class="jsx-4225977845">Warranty Policy</a>
                        </li>
                        <li class="jsx-4225977845"><a target="_blank" rel="noopener noreferrer"
                                                      href=""
                                                      class="jsx-4225977845">Sell with us</a></li>
                        <li class="jsx-4225977845"><a href="terms-of-use" class="jsx-4225977845">Terms of Use</a></li>
                        <li class="jsx-4225977845"><a href="terms-of-sale" class="jsx-4225977845">Terms of Sale</a></li>
                        <li class="jsx-4225977845"><a href="privacy-policy" class="jsx-4225977845">Privacy Policy</a>
                        </li>
                    </ul>
                    <div class="jsx-4225977845 copyright bottomCopyright"><a data-tip="bottom-copyright"
                                                                             data-for="bottom-copyright-tooltip"
                                                                             class="jsx-4225977845">© 2020 abiyat. All
                        Rights Reserved.</a>
                        <div class="__react_component_tooltip place-top type-dark " id="bottom-copyright-tooltip"
                             data-id="tooltip">&#x27;abiyat&#x27;, the &#x27;abiyat device&#x27;, &#x27;abiyat east&#x27;,
                            &#x27;east&#x27; and the &#x27;east device&#x27; are trade marks or registered trade marks
                            of abiyat AD Holdings LTD. in the UAE and other countries
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
</body>
</html>