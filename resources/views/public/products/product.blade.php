@extends('layouts.public.app')
@section('content')
<style>
    body{
        font-family: 'Montserrat', sans-serif;
        color: #222222
    }
      
      .cart-button{
        background: #005e00;
        color:#fff
      }
      .review-button{
        background: #005e00;
        color:#fff
      }
      /* description tabs */
      .cool-link {
    display: inline-block;
    color: #000;
    
    text-decoration: none;
    }

    .cool-link::after {
        content: '';
        display: block;
        width: 0;
        height: 4px;
        color: #005e00;
        background: #005e00;
        transition: width .3s;
    }

    .cool-link:hover::after {
        width: 100%;
        
    }
    .cool-link:hover{
        color: #005e00;
        font-weight: 900;
    }
    .cool-link.active{
        color: #005e00;
        font-weight: 900;
    }
    .text-dark.active .cool-link{
        color: #005e00;
        font-weight: 600;
    }
    .text-dark.active .cool-link:after {
        width: 100%;
        
    }

/* closw description tabs */
    .product-price{
        padding-bottom: 0%;
        margin-bottom: 0%;
        font-size: 24px;
        font-weight: 700;
        color: #000;
        font-family: 'Montserrat', sans-serif;
    }
    .price-time{
        font-size: 15px;
        padding-bottom: 0%;
        margin-bottom: 0%;
        font-family: 'Montserrat', sans-serif;
    }
    .price-time-befor{
        font-size: 12px;
        padding-bottom: 0%;
        margin-bottom: 0%;
        font-family: 'Montserrat', sans-serif;
    }
    
    .product-price-saving{
        padding-bottom: 0%;
        margin-bottom: 0%;
        font-size: 13px;
        font-family: 'Montserrat', sans-serif;
    }


    .floatLeft {
        display: inline-block;
    }
    
    .shadow-sm-category{
        /* border: 1px solid #000000; */
        padding: 5%;
    }
    
    .categories-section::-webkit-scrollbar {
    width: 0px;
    background: transparent; /*/ make scrollbar transparent / */
    }

    .thumbnail:hover {
    position:relative;
    top:-25px;
    left:-35px;
    width:500px;
    height:auto;
    display:block;
    z-index:999;
}
/* ///////////////// */


    .breadcrumb-item+.breadcrumb-item::before {
        content: ">"
    }

    .breadcrumb {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        padding: .1rem 0rem !important;
        margin-bottom: 0rem;
        list-style: none;
        background-color: #ffffff;
        border-radius: .25rem
    }

    .single_product {
        padding-top: 66px;
        padding-bottom: 140px;
        background-color: #e5e5e5;
        margin-top: 0px;
        padding: 17px
    }

    .product_name {
        font-size: 20px;
        font-weight: 400;
        margin-top: 0px;
        font-family: 'Montserrat', sans-serif;
    }

    .product_price {
        display: inline-block;
        font-size: 30px;
        font-weight: 500;
        margin-top: 9px;
        clear: left
    }

    .product_discount {
        display: inline-block;
        font-size: 17px;
        font-weight: 300;
        margin-top: 9px;
        clear: left;
        margin-left: 10px;
        color: red
    }

    .product_saved {
        display: inline-block;
        font-size: 15px;
        font-weight: 200;
        color: #999999;
        clear: left
    }

    .singleline {
        margin-top: 1rem;
        margin-bottom: .40rem;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, .1)
    }

    .product_info {
        color: #4d4d4d;
        display: inline-block
    }

    .product_options {
        margin-bottom: 10px
    }

    .product_description {
        padding-left: 0px
    }

    .product_quantity {
        min-width: 80px;
        height: 47px;
        border: solid 1px #e5e5e5;
        border-radius: 3px;
        overflow: hidden;
        padding-left: 0;
        padding-top: -4px;
        padding-bottom: 44px;
        float: left;
        margin-right: 0;
        /* margin-bottom: 11px */
    }

    .order_info {
        margin-top: 18px
    }

    .shop-button {
        height: 47px
    }

    .product_fav i {
        line-height: 44px;
        color: #cccccc
    }

    .product_fav {
        display: inline-block;
        width: 52px;
        height: 46px;
        background: #FFFFFF;
        box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
        border-radius: 11%;
        text-align: center;
        cursor: pointer;
        margin-left: 3px;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease
    }

    .br-dashed {
        border-radius: 5px;
        border: 1px dashed #dddddd;
        margin-top: 6px
    }

    .pr-info {
        margin-top: 2px;
        padding-left: 2px;
        margin-left: 2px;
        padding-left: 0px
    }

    .break-all {
        color: #5e5e5e
    }

    .image_selected {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: calc(100% + 15px);
        height: 350px;
        -webkit-transform: translateX(-15px);
        -moz-transform: translateX(-15px);
        -ms-transform: translateX(-15px);
        -o-transform: translateX(-15px);
        transform: translateX(-15px);
        /* border: solid 1px #e8e8e8; */
        /* box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.1); */
        overflow: hidden;
        padding: 5px
    }

    .image_list li {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 165px;
        /* border: solid 1px #e8e8e8; */
        /* box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.1) !important; */
        margin-bottom: 10px;
        cursor: pointer;
        padding: 15px;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease;
        overflow: hidden
    }

    @media (max-width: 390px) {
        .product_fav {
            display: none
        }
    }

    .bbb_combo {
        width: 100%;
        margin-right: 7%;
        padding-top: 21px;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 24px;
        border-radius: 5px;
        margin-top: 0px;
        text-align: -webkit-center
    }

    .bbb_combo_image {
        width: 170px;
        height: 170px;
        margin-bottom: 15px
    }

    .fs-10 {
        font-size: 10px
    }

    .step {
        background: #167af6;
        border-radius: 0.8em;
        -moz-border-radius: 0.8em;
        -webkit-border-radius: 6.8em;
        color: #ffffff;
        display: inline-block;
        font-weight: bold;
        line-height: 3.6em;
        margin-right: 5px;
        text-align: center;
        width: 3.6em;
        margin-top: 116px
    }

    .row-underline {
        content: "";
        display: block;
        border-bottom: 2px solid #3798db;
        margin: 0px 0px;
        margin-bottom: 20px;
        margin-top: 15px
    }

    .deal-text {
        margin-left: -10px;
        font-size: 25px;
        margin-bottom: 10px;
        color: #000;
        font-weight: 700
    }

    .padding-0 {
        padding-left: 0;
        padding-right: 0
    }

    .padding-2 {
        margin-right: 2px;
        margin-left: 2px
    }

    .vertical-line {
        display: inline-block;
        border-left: 3px solid #167af6;
        margin: 0 10px;
        height: 364px;
        margin-top: 4px
    }

    .p-rating {
        color: green
    }

    .combo-pricing-item {
        display: flex;
        flex-direction: column
    }

    .boxo-pricing-items {
        display: inline-flex
    }

    .combo-plus {
        margin-left: 10px;
        margin-right: 18px;
        margin-top: 10px
    }

    .add-both-cart-button {
        margin-left: 36px
    }

    .items_text {
        color: #b0b0b0
    }

    .combo_item_price {
        font-size: 18px
    }

    .p_specification {
        font-weight: 500;
        margin-left: 22px
    }

    .mt-10 {
        margin-top: 10px
    }

    @charset "utf-8";
    @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Rubik:300,400,500,700,900');

    * {
        margin: 0;
        padding: 0;
        -webkit-font-smoothing: antialiased;
        -webkit-text-shadow: rgba(0, 0, 0, .01) 0 0 1px;
        text-shadow: rgba(0, 0, 0, .01) 0 0 1px
    }

    body {
        font-family: 'Montserrat', sans-serif;
        font-size: 14px;
        font-weight: 400;
        background: #FFFFFF;
        color: #000000
    }

    div {
        display: block;
        position: relative;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box
    }

    ul {
        list-style: none;
        margin-bottom: 0px
    }

    .single_product {
        padding-top: 16px;
        padding-bottom: 140px
    }

    .image_list li {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 165px;
        border: solid 1px #e8e8e8;
        box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
        margin-bottom: 15px;
        cursor: pointer;
        padding: 15px;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease;
        overflow: hidden
    }

    .image_list li:last-child {
        margin-bottom: 0
    }

    .image_list li:hover {
        box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3)
    }

    .image_list li img {
        max-width: 100%
    }

    .image_selected {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: calc(100% + 15px);
        height: 350px;
        -webkit-transform: translateX(-15px);
        -moz-transform: translateX(-15px);
        -ms-transform: translateX(-15px);
        -o-transform: translateX(-15px);
        transform: translateX(-15px);
        /* border: solid 1px #e8e8e8; */
        /* box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1); */
        overflow: hidden;
        padding: 5px
    }

    .image_selected img {
        max-width: 100%
    }

    .product_category {
        font-size: 12px;
        color: rgba(0, 0, 0, 0.5)
    }

    .product_rating {
        margin-top: 7px
    }

    .product_rating i {
        margin-right: 4px
    }

    .product_rating i::before {
        font-size: 13px
    }

    .product_text {
        margin-top: 27px
    }

    .product_text p:last-child {
        margin-bottom: 0px
    }

    .order_info {
        margin-top: 16px
    }

    .product_quantity {
        min-width: 80px;
        height: 46px;
        border: solid 1px #e5e5e5;
        border-radius: 5px;
        overflow: hidden;
        padding-left: 0;
        float: left;
        margin-right: 0
    }

    .product_quantity span {
        display: block;
        height: 50px;
        font-size: 16px;
        font-weight: 300;
        color: rgba(0, 0, 0, 0.5);
        line-height: 50px;
        float: left
    }

    .product_quantity input {
        display: block;
        width: 100%;
        height: 50px;
        border: none;
        outline: none;
        font-size: 16px;
        font-weight: 300;
        color: rgba(0, 0, 0, 0.5);
        text-align: left;
        padding-left: 12px;
        line-height: 50px;
        float: left
    }

    .quantity_buttons {
        position: absolute;
        top: 0;
        right: 0;
        height: 100%;
        width: 29px;
        border-left: solid 1px #e5e5e5
    }

    .quantity_inc,
    .quantity_dec {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 50%;
        cursor: pointer
    }

    .quantity_control i {
        font-size: 11px;
        color: rgba(0, 0, 0, 0.3);
        pointer-events: none
    }

    .quantity_control:active {
        border: solid 1px rgba(14, 140, 228, 0.2)
    }

    .quantity_inc {
        padding-bottom: 2px;
        justify-content: flex-end;
        border-top-right-radius: 5px
    }

    .quantity_dec {
        padding-top: 2px;
        justify-content: flex-start;
        border-bottom-right-radius: 5px
    }

 



#thumbnails {
    float: left;
}

#thumbnails li {
    margin-bottom: 10px;
}

#thumbnails li img {
    width: 100px;
}

#thumbnails li a:hover img {
    border: 1px solid #005e00;
}


.img-thumbnail {
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    display: inline-block;
    max-width: 100%;
    height: auto;
}

.img-responsive, .thumbnail > img, .thumbnail a > img, .carousel-inner > .item > img, .carousel-inner > .item > a > img{
    display: block;
    max-width: 100%;
    height: auto;
}

.product-cover-wrap {
    border: 1px solid #eee;
    float: left;
}
.product-detail-icon{
    margin-right: 6px;
}
.product-detail-icon.icn {
    border-radius: 50px;
    margin-right: 6px;
    padding: 3px;
}
.product-detail-icon i {
    font-size: 15px;
    padding: 3px;
}
.product-detail-icon img {
    width: 30px;
}

.product-detail-title div, .detail-text-1{
    font-size: 13px;
    font-weight: 500;
}
.detail-text-1 a {
    display: inline;
    position: relative;
    color: inherit;
    border-bottom: solid 1px #ffa07f;
    -webkit-transition: all 200ms ease;
    -moz-transition: all 200ms ease;
    -ms-transition: all 200ms ease;
    -o-transition: all 200ms ease;
    transition: all 200ms ease
}

.detail-text-1 a:active {
    position: relative;
    color: #FF6347
}

.detail-text-1 a:hover {
    color: #FFFFFF;
    background: #ffa07f
}

.detail-text-1 a:hover::after {
    opacity: 0.2
}
</style>
<div class="super_container">
    <div class="container-fluid m-0 pl-5 pr-5 ">
        <div class="row">
            <div class="col-12">
                <!-- Breadcrumbs open -->
                <nav aria-label="breadcrumb" class="  d-none d-md-block mt-3">
                    <ol class="breadcrumb breadcrumb-arrow p-0">
                        <li class="breadcrumb-item"><a href="#" class="text-uppercase pl-3">Home</a></li>
                        <li class="breadcrumb-item pl-0"><a href="#" class="text-uppercase">{{$category->name}}</a></li>
                        <li aria-current="page" class="breadcrumb-item pl-0 active text-uppercase pl-4">{{ $product->name }}</li>
                    </ol>
                </nav>
                <!-- Breadcrumbs close -->
            </div>
        </div>
    </div>
    <!-- Product Detail open -->
    <div class="single_product p-0">
        <div class="container-fluid  " style=" background-color: #fff; padding: 11px;">
            <div class="row">
                <div class="col-lg-5 order-1 pl-2" id="web-cursour"> 
                    <ul id="thumbnails" class="col-md-4 list-unstyled">
                        <li>
                            <a href="javascript: void(0)">
                                @if(isset($product->cover))
                                <img class="img-responsive img-thumbnail"
                                    src="{{ asset("storage/$product->cover") }}"
                                    alt="{{ $product->name }}" />
                                @else
                                <img class="img-responsive img-thumbnail"
                                    src="{{ asset("https://placehold.it/180x180") }}"
                                    alt="{{ $product->name }}" />
                                @endif
                            </a>
                        </li>
                        @if(isset($images) && !$images->isEmpty())
                            @foreach($images as $image)
                            <li>
                                <a href="javascript: void(0)">
                                <img class="img-responsive img-thumbnail"
                                    src="{{ asset("storage/$image->src") }}"
                                    alt="{{ $product->name }}" />
                                </a>
                            </li>
                            @endforeach
                        @endif
                    </ul>
                    <figure class="text-center product-cover-wrap col-md-8">
                        @if(isset($product->cover))
                            <img id="main-image" class="product-cover img-responsive"
                                src="{{ asset("storage/$product->cover") }}?w=400"
                                data-zoom="{{ asset("storage/$product->cover") }}?w=1200">
                        @else
                            <img id="main-image" class="product-cover" src="https://placehold.it/300x300"
                                data-zoom="{{ asset("storage/$product->cover") }}?w=1200" alt="{{ $product->name }}">
                        @endif
                    </figure>
                    <!-- Images closed -->
                </div>

                
                <div id="mobile-cursour" >
                    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            @php
                                $counter = 0;
                            @endphp
                            @foreach($images as $key => $img)
                            @if ($counter == 0)
                            <li data-target="#demo" data-slide-to="{{$counter}}" class="active"></li>
                            @endif
                            
                            <li data-target="#demo" data-slide-to="{{$counter}}"></li>
                            @php $counter++; @endphp
                            @endforeach
                        </ul>

                        <!-- The slideshow -->
                        <div class="product carousel-inner">
                            <div class="carousel-item active">
                                <img src='{{ asset("storage/$product->cover") }}' style="width:100%;" alt="">
                            </div>
                            @foreach($images as $key => $img)
                            <div class="carousel-item ">
                                <img src='{{ asset("storage/$img->src") }}' style="width:100%;" alt="">
                            </div>
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 order-2  ">
                    <div class="product_description">
                        <div class="company_name mt-5 mt-lg-0">{{ isset($product->brand->name) ? $product->brand->name : '' }}</div>
                        <div class="product_name">{{ $product->name }}</div>
                        <!--<div class="product-rating"><span class="badge badge-color"><i class="las la-star"></i> 4.5</span> <span class="rating-review">1 Rating</span></div>-->
                        @if($product->discount)
                        <div class="form-inline mt-3">
                            <p class="price-time mr-3">WAS:</p>
                            <p class="price-time-befor"><strike>{{ config('cart.currency') }} {{ $product->price }}</strike></p>
                        </div>
                        @endif
                        <div class="form-inline m-0">
                            <p class="price-time mr-3">NOW:</p>
                            <p class="product-price"> {{ config('cart.currency_symbol') }}  @if($product->sale_price) {{ $product->sale_price }} @else {{ $product->price }}@endif</p>
                            <p class="price-time ml-2">(inclusive of VAT)</p>
                        </div>
                        @if($product->discount)
                        <div class="form-inline m-0">
                            <div class="form-inline">
                                <p class="price-time mr-3">SAVING:</p>
                                <p class="product-price-saving"> {{ config('cart.currency') }} {{ $product->price - $product->sale_price}}</p>
                            </div>
                            <div>
                                <p class="text-pertage"><span>{{ $product->discount }}%</span>&nbsp; Off</p>
                            </div>
                        </div>
                        @endif
                        <!-- delivery to section start-->
                        <div class="product-deliver-card hide">
                            <div class="card mt-3 mb-3 ">    
                                <a href="#" class="card-header orders-card-header p-1">
                                    <p class="m-0">Deliver to <strong> Riyadh</strong></p>
                                    <i class="fas fa-chevron-right p-1"></i>
                                </a>
                                <div class="card-body card-header p-1 pt-2">
                                    <p class="m-0">Order in <strong>4 </strong> hrs <strong>20</strong> mins</p>
                                    <div class="form-inline">
                                    <p class="m-0"><strong>free delivery </strong>by <span style="color:#005e00;"> &nbsp;Tomorrow, Apr 26</span></p>
                                    <img src="" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- delivery to section end -->
                        <div class="mt-2 mb-2"><a href="#"><img src="{{asset('public_assets/images/Banner 1.png')}}" class="img-fluid" alt="image"></a></div>
                        <!-- <div class="br-dashed">
                            <div class="pr-info"> <span class="break-all">Get 5% instant discount + 10X rewards @ RENTOPC</span> </div>
                        </div>      -->
                        </div>                        
                        <form action="{{ route('cart.store') }}" class="mt-2 mb-2" method="post">
                            {{ csrf_field() }}

                            @if(isset($productAttributes) && !$productAttributes->isEmpty())
                            <div class="form-group">
                                <label for="productAttribute">Choose Combination</label> <br />
                                <select name="productAttribute" id="productAttribute" class="form-control select2">
                                    @foreach($productAttributes as $productAttribute)
                                        <option value="{{ $productAttribute->id }}">
                                            @foreach($productAttribute->attributesValues as $value)
                                                {{ $value->attribute->name }} : {{ ucwords($value->value) }}
                                            @endforeach
                                            @if(!is_null($productAttribute->price))
                                                ( {{ config('cart.currency_symbol') }} {{ $productAttribute->price }})
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="row mt-2 mobile-cart-btn">
                                <div class="col-4 pr-0" >
                                    <p class="price-time"><b>QTY</b></p>
                                    <div class="product_quantity"> 
                                    <input id="quantity" name="quantity" type="number" value="1" min="1" autocomplete="off" pattern="[0-9]*" value="{{ old('quantity') }}">
                                    <input type="hidden" name="product" value="{{ $product->id }}" />
                                        
                                    </div>
                                    
                                </div>
                                <div class="col-6"> 
                                    <button type="submit" class="btn cart-button shop-button mt-4 w-100" >Add to Cart</button> 
                                </div>
                                <div class="col-2 pl-0 text-center"> 
                                    <a role="button" class="product-wishicon"><i class="lab la-gratipay mt-3"></i></a>
                                </div>
                            </div>
                            <div class="mt-2 hide">
                                <p class="mt-0">Color Name: <strong>Blue</strong></p>
                            </div>
                        </form>
                </div>    
                <div class="col-lg-3 order-3 pr-2 border-left">  
                    <p class="mt-5 mt-lg-0"><b>Offer Details</b></p>
                    <hr>
                    @if (isset($product->warranty) && $product->warranty > 0)
                    <div class="border-bottom pt-2 pb-2 m-0 d-flex align-items-center"><span class="product-detail-icon icn bg-light mt-1">
                            <i class="far fa-calendar-alt text-success"></i>
                        </span> <span class="detail-text-1">{{$product->warranty}} @if (isset($product->warranty) && $product->warranty > 1) months @else month @endif warranty</span></div>  
                    @endif
                   
                    <div class="border-bottom pt-2 pb-2 m-0 d-flex align-items-center "><span class="product-detail-icon icn bg-light mt-1">
                            <i class="fas fa-undo-alt text-success"></i>
                        </span> <span class="detail-text-1">Enjoy Hassle free returns with this offer</span></div>  
                    <div class="border-bottom pt-2 pb-2 m-0 d-flex align-items-center"><span class="product-detail-icon icn bg-light mt-1">
                            <i class="fas fa-store-alt text-success"></i>
                        </span> <span class="detail-text-1">Sold by <a href="#">abiyat</a></span></div> 
                    <div class="mt-5">
                        <div class="form-inline d-flex align-items-start mb-2 flex-nowrap">
                            <div class="product-detail-icon mt-1">
                                <img src="{{asset('public_assets/images/secure-shield-f.png')}}" class="" alt="Truck">
                                <!-- <i class="fas fa-user-lock text-success"></i> -->
                            </div>
                            <div class="product-detail-title">
                                <div class="m-0">SECURE SHIPPING</div>
                                <p class="m-0">Your data is always protected</p>
                            </div>
                        </div>
                        <div class="form-inline d-flex align-items-start mb-2 flex-nowrap">
                            <div class="product-detail-icon mt-1">
                                <img src="{{asset('public_assets/images/return-box-f.png')}}" class="" alt="Truck">
                                <!-- <i class="fas fa-undo text-success"></i> -->
                            </div>
                            <div class="product-detail-title">
                                <div class="m-0">FREE RETURNS</div>
                                <p class="m-0">Get free returns on eligible items</p>
                            </div>
                        </div>
                        <div class="form-inline d-flex align-items-start mb-2 flex-nowrap">
                            <div class="product-detail-icon mt-1">
                                <img src="{{asset('public_assets/images/delivery-truck-f.png')}}" class=" " alt="Truck">
                                <!-- <i class="fas fa-shipping-fast text-success"></i> -->
                            </div>
                            <div class="product-detail-title">
                                <div class=" m-0">TRUSTED SHIPPING</div>
                                <p class="m-0">Free shippinng when spend 2000 taka and above on express items</p>
                            </div>
                        </div>
                    </div>
                    <!-- <p><i class="fas fa-tag text-success" ></i> Other 12 offers form <b> {{ config('cart.currency') }} 999.00</b></p>    -->
                </div>
            </div> 
        </div>
    </div>
    <!-- Product Detail close -->
    <div class="row bg-light" style="height:30px">
    </div>

    <!-- Decsription detail and Review tabs open-->
    <div id="tabs-xSEcht0rZ5" class="col-lg-12 mt-5">
        <!-- Tabs -->
        <ul class="nav list-unstyled border-bottom" role="tablist">
            <li role="presentation" class="list-inline-item  mr-3">
                <a href="#tab-1" role="tab" data-toggle="tab" class="text-dark active" aria-selected="true">
                <p class="description-tabs cool-link"> OVERVIEW </p>
                </a>
            </li>
            <li role="presentation" class="list-inline-item  mr-3">
                <a href="#tab-2" role="tab" data-toggle="tab" class="text-dark" aria-selected="false">
                <p class="description-tabs cool-link"> SPECIFICATIONS </p>
                </a>
            </li>
            <li role="presentation" class="list-inline-item mr-3">
                <a href="#tab-3" role="tab" data-toggle="tab" class="text-dark" aria-selected="false">
                <p class="description-tabs cool-link"> RATING & REVIEWS </p>
                </a>
            </li>
        </ul>
    
        <!-- Content -->
        <div class="tab-content mt-3">
            <div role="tabpanel" class="tab-pane active show" id="tab-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                        {!! $product->description !!}
                        </div>
                        <!-- <div class="col-lg-4">

                        </div> -->
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane " id="tab-2">
            <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                        {!! $product->specificaiton !!}
                        </div>
                        <!-- <div class="col-lg-4">

                        </div> -->
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab-3">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-4 bg-success">
                            
                        </div>
                        <div class="col-lg-8">
                            <form method="post" class="well padding-bottom-10 mb-3" onsubmit="return false;">
                                <textarea rows="2" class="form-control" placeholder="Write a review"></textarea>
                                <div class="margin-top-10"> 
                                    <button type="submit" class="btn btn-sm review-button m-1"> Submit Review </button>    
                                </div>
                            </form>
                            <div class="chat-body no-padding profile-message">
                                <ul>
                                    <li class="message mt-3 pb-3 mb-3 border-bottom">
                                        <span class=""> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x text-muted"></i> </span> <br>
                                        <span class="message-text"> <a href="#" class="username"> Alisha Molly   </a> <br>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni numquam cum sit consequuntur itaque veniam neque magnam, eveniet odit quod doloribus nemo officiis quae enim libero cupiditate praesentium possimus beatae!</span>
                                    </li>
                                    <li class="message"> 
                                        <li class="message mt-3 pb-3 mb-3 border-bottom">
                                            <span class=""> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x"></i> <i class="fa fa-star fa-2x "></i> <i class="fa fa-star fa-2x text-muted"></i> </span> <br>
                                            <span class="message-text"> <a href="#" class="username"> Alisha Molly   </a> <br>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni numquam cum sit consequuntur itaque veniam neque magnam, eveniet odit quod doloribus nemo officiis quae enim libero cupiditate praesentium possimus beatae!</span>
                                        </li>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- Decsription detail and Review tabs close-->
</div>
<script>
    // Wishlist icon selected toggle
$(document).ready(function(){
        $(".product-wishicon").click(function(){
        $(this).toggleClass("active");
    });
});
</script>
@endsection
