@extends('layouts.public.app')
@section('content')

<div class="container-fluid p-0 bg-light pb-5">
    <div class="main-empty-cart">
        <div class="empty-img">
            <img src="{{ asset('public_assets/images/empty-state-cart.svg')}}" alt="">
        </div>
        <div class="empty-text">
            <h3>Your shopping cart looks empty</h3>
            <p>What are you waiting for?</p>
        </div>
        <button type="button" class="btn btn-primary btn-lg">Start Shopping</button>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img src="http://127.0.0.1:8000/public_assets/images/Bannernew.png"
                    class="cart-banner img-fluid d-none d-md-block" alt="">
                <div class="d-flex justify-content-between">
                    <div class="form-inline m-2">
                        <p class="cart-heading mr-2"><b>Wishlist</b></p>
                        <p class="cart-heading-items">(<span>1</span>items)</p>
                    </div>
                </div>
                <!-- cart item open -->
                <div class="container bg-white">
                    <div class="row border-bottom pb-3">
                        <div class=" col-4 col-md-3 ">
                            <img src="http://127.0.0.1:8000/storage/products/GchRrHhBhPTOBkAsO3wYYfe0YsJFqWHdRtaXR7p3.jpeg"
                                class="img-fluid" alt="Quasi nobis sunt et odit sint nihil quae laudantium.">
                        </div>
                        <div class="col-6 col-md-6 ">
                            <div class="product_description">
                                <div class="company_name mt-3">ACER</div>
                                <div class="product_name">Quasi nobis sunt et odit sint nihil quae laudantium.</div>
                            </div>
                            <div class="deliver-description">
                                <p class="cart-deliver-message">Delivered by
                                    <strong class="cart-deliver-date">Mon, Nov 16</strong><br>
                                    <span>when you order in 10 hrs 37 mins</span>
                                </p>
                            </div>
                            <div class="deliver-description">
                                <form action="http://127.0.0.1:8000/cart/eef12573176125ce53e333e13d747a17"
                                    method="post">
                                    <input type="hidden" name="_token" value="u1BqiDjMErJTkuBpnLUIpeILpeaVTiPwOyA54ckG">
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="form-inline">
                                    <button class="btn btn-success btn-sm mr-3"><i class="fas fa-cart-arrow-down mr-2"></i>Move to Cart?</button>
                                    <button onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i
                                            class="far fa-trash-alt mr-2" aria-hidden="true"></i>Delete</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- cart item close -->
            </div>
        </div>
    </div>
</div>
@endsection