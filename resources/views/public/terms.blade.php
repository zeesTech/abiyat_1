@extends('layouts.public.app')
@section('content')
<div class="privacy-modify">
    <p>Terms & Conditions</p>
    <span>Last modified: March 03, 2021</span>
</div>
<div class="privacy-wrap">

    <p>Abiyat.com reserves the right to change, modify, add, or remove portions of these Terms and Conditions at any
        time without any prior notification. Changes will be effective when posted on the Site with no other notice
        provided. Please check these Terms and Conditions regularly for updates. Your continued use of the Site
        following the posting of changes to Terms and Conditions of use constitutes your acceptance of those changes.
    </p>

    <h4>THIRD PARTY BUSINESSES</h4>
    <p>Parties other than Abiyat and its affiliates may operate stores, provide services, or sell product lines on the
        Site. For example,
        businesses and individuals offer products via Marketplace. In addition, we provide links to the websites of
        affiliated companies and
        certain other businesses. We are not responsible for examining or evaluating, and we do not warrant or endorse
        the offerings of any of
        these businesses or individuals, or the content of their websites. We do not assume any responsibility or
        liability for the actions, products,
        and content of any of these and any other third-parties. You can tell when a third-party is involved in your
        transactions by reviewing your
        transaction carefully, and we may share customer information related to those transactions with that
        third-party. You should carefully review their
        privacy statements and related terms and conditions.</p>

    <h4>ORDER ACCEPTANCE</h4>
    <p>Each product in your order is sold either by us or by the local or international seller that is specified on the
        Site.Our acceptance of your order
        will take place when we notify you of our acceptance in writing (e.g. by email or mobile messaging). If we are
        unable to accept your order, we will
        inform you of this in writing or through a call and will not charge you for the product. We accept payment
        bycredit/debit card; via your mobile banking;
        or cash on delivery (an amount not exceeding BDT 5,000). We may remove or add cards or other payment methods
        that we accept at any time without prior notice to you.</p>
    <p><b>Cancelling Order:</b> You may cancel your order immediately prior to shipping for any reason.</p>
    <p><b>Our Cancellation:</b> We may cancel your order(s) if:</p>

    <ul>
        <li>• you do not make any payment to us when it is due;</li>
        <li>• you do not, within a reasonable time of us asking for it, provide us with information that is necessary
            for us to provide the products; or</li>
        <li>• you do not, within a reasonable time, allow us to deliver the products to you or collect them from us; or
        </li>
        <li>• We reserve the right to reject any orders, at our sole discretion, where we detect bulk purchasing or
            multiple units of similar products being purchased.</li>
    </ul>

    <h4>DELIVERY OF YOUR ORDER</h4>
    <p>The costs of delivery will be as displayed to you on our Site. Delivery Date will be displayed to you on our
        Site. We will issue an electronic invoice for your purchase
        and send such invoice to the email address you provided to us.</p>
    <p>If our supply of the product is delayed by an event outside of our control, then we will contact you as soon as
        possible to let you know and we will take steps to minimise the effect of the delay. If no one is available at
        your address to take delivery and the products cannot be posted through your letterbox, we will notify you of
        how to rearrange delivery or collect the product. if you do not collect the product from us as arranged or if,
        after a failed delivery to you, you do not re-arrange delivery or collect them from a delivery depot, we will
        contact you for further instructions. If, despite our reasonable efforts, we are unable to contact you or
        re-arrange delivery or collection, we will cancel your order.</p>
    <p>ID Requirement Upon Delivery. We may, at our sole discretion, make any inquiry we deem necessary to verify your
        identity and/or ownership of your financial instruments by requesting that you provide us with further
        information or documentation, including, but not limited to requesting a form of identification and/or credit
        card. If we are unable to verify or authenticate any information you provide, we have the right to refuse
        delivery and cancel the order.</p>
    <p>Delivery from Overseas. Note that you might be considered the importer of record in the event that your order
        requires delivery from overseas. In such instance, you are required to ensure that your ordered products can be
        lawfully imported, comply with all laws and regulations and to pay all fees and customs duties relevant to your
        purchase.</p>
    <p>Title to Products. A product will be considered owned by you and your responsibility from the time we deliver the
        product to the delivery address and you have paid for the product.</p>

    <h4>COMMUNICATING WITH US</h4>
    <P>When you visit the Site, or send e-mails to us, you are communicating with us electronically. You will be
        required to provide a valid phone number while placing an order with us. We may communicate with you by e-mail,
        SMS, phone call or by posting notices on the Site or by any other mode of communication we choose to employ. For
        contractual purposes, you consent to receive communications (including transactional, promotional and/or
        commercial messages), from us with respect to your use of the website (and/or placement of your order) and agree
        to treat all modes of communication with the same importance.</P>

    <h4>LOSSES</h4>
    <p>We will not be responsible for any business or personal losses (including but not limited to loss of profits,
        revenue, contracts, anticipated savings, data, goodwill, or wasted expenditure) or any other indirect or
        consequential loss that is not reasonably foreseeable to both you and us when you commenced using the Site.</p>

    <h4>TERMINATION</h4>
    <p>In addition to any other legal or equitable remedies, we may, without prior notice to you, immediately terminate
        the Terms and Conditions or revoke any or all of your rights granted under the Terms and Conditions. Upon any
        termination of this Agreement, you shall immediately cease all access to and use of the Site and we shall, in
        addition to any other legal or equitable remedies, immediately revoke all password(s) and account identification
        issued to you and deny your access to and use of this Site in whole or in part. Any termination of this
        agreement shall not affect the respective rights and obligations (including without limitation, payment
        obligations) of the parties arising before the date of termination. You furthermore agree that the Site shall
        not be liable to you or to any other person as a result of any such suspension or termination. If you are
        dissatisfied with the Site or with any terms, conditions, rules, policies, guidelines, or practices in operating
        the Site, your sole and exclusive remedy is to discontinue using the Site.</p>

    <h4>GOVERNING LAW AND JURISDICTION</h4>
    <p>These terms and conditions are governed by and construed in accordance with the laws of The People's Republic of
        Bangladesh. You agree that the courts, tribunals and/or quasi-judicial bodies located in Dhaka, Bangladesh shall
        have the exclusive jurisdiction on any dispute arising inside Bangladesh under this Agreement.</p>

</div>
@endsection