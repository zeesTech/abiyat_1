@extends('layouts.public.app')
@section('content')
    <style>
        .floatLeft {
        display: inline-block;
        }
        
        .shadow-sm-category{
        border: 1px solid #00A71C;
        border-radius: 50%;
        padding: 0%;
        background-color: #ffffff;
        }
        
        .categories-section::-webkit-scrollbar {
        width: 0px;
        background: transparent; /*/ make scrollbar transparent / */
        }
        .banner-website{
          display:block;          
        }
        .banner-mob{
          display:none;
        }
        @media (max-width:767px){
          .banner-website{
          display:none;          
        }
        .banner-mob{
          display:block;
        } 
        }
        .shaded-sm{
            padding:6px;
        }
        </style>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>       
<div class="container-fluid p-0 bg-light">
        <!--Carousel Open  -->
        <div class="container-fluid p-0 bg-light">
    
            <div id="carouselExampleIndicators" class="carousel slide bg-light " data-ride="carousel">
                <!-- <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class=" indicator-color active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="indicator-color"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="indicator-color"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3" class="indicator-color"></li>
                </ol> -->
                <div class="carousel-inner home">
                <div class="carousel-item active ">
                    <a href="#" class="banner-website"><img src="public_assets/images/website_banners/Free-shiping-banner-pc.png" class="d-block w-100 " style="min-height: 170px;" alt="..."></a>
                    <a href="#" class="banner-mob"><img src="public_assets/images/mobile_banners/Free-shiping-banner-phone.png" class="d-block w-100 " alt="..."></a>
                </div>
                <!-- <div class="carousel-item">
                    <a href="#">
                        <img src="public_assets/images/Bannernew.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                    </a>
                </div> -->
                </div>
                <!-- <a class="carousel-control-prev " href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon " aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a> -->
            </div>
        
        </div>
        <!--Carousel Close -->
        <!-- Category Images open-->
        
        <div class="container-fluid categories-section" style="overflow-x: scroll;overflow-y: hidden;">
            <div class="pr-4" style="white-space: nowrap;">
                @foreach ($cat_list as $category)
                @if($category->status != 0) 
                <div class="col-4 col-sm-2 col-md-2 col-lg-1 p-1 text-center floatLeft">
                    <div class="m-1 shaded-sm">
                        <a href="{{ route('front.category.slug', str_slug($category->slug)) }}" class="text-decoration-non text-dark ">
                            <img src="{{ asset("storage/$category->cover") }}" class="img-fluid shadow-sm-category" alt="">
                            <br>
                            <span class="circle-image-caption "><b>{{$category->name}}</b></span>
                        </a>
                    </div>
                </div>
                @endif
                @endforeach
                
            </div>
        </div>
        <!-- Category Images closed -->

        <!-- Product Carousel open-->
        <!--Carousel Open  -->
        @if (count($products) > 0)
        <div class="container-fluid  bg-light design-arrow">
            <div class="mt-0 pt-2 pl-4">
                <h5><b>Recommended For You</b></h5>
            </div>

            <div id="recommended-for-you" class="owl-carousel ">
            @foreach ($products as $value) 
                <div class="product-style" >
                    <a role="button" class="product-wishicon productwish"><i class="lab la-gratipay"></i></a>
                    <a href="{{ route('front.get.product', str_slug($value->slug)) }}" class="text-decoration-non text-dark m-2">
                        <!-- <div class=" rounded-lg ml-1 mr-1 top-discount-bar-color text-truncate">
                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                        </div>     -->
                        <div class="p-1">
                            <img src="{{ asset("storage/$value->cover") }}" class="img-fluid product_image" alt="">
                        </div >
                        <div class="ml-2 mr-2 mt-2  mb-10">
                            <span class="product-detail product-description" >{{$value->name}}</span>
                        </div>
                        <div class="from-inline pl-2 pr-2 mb-10">
                            <span class="text-muted currency-name" ><b>{{ config('cart.currency_symbol') }}</b></span>
                            <span class="currecny-value"><b>@if ($value->sale_price) {{$value->sale_price}} @else {{$value->price}} @endif</b></span>
                            <!-- <span class="currency-line">BDT</span>
                            <span class="value-line">57</span> -->
                            <div>
                            @if ($value->discount)
                            <p class=" pl-2 pr-2 mb-0"><strike><span>{{ config('cart.currency_symbol') }}</span> {{$value->price}}</strike> <span class="text-pertage"> {{ $value->discount}}%</span></p>
                            @else
                            &nbsp;
                            @endif
                        </div>
                        </div>
                        
                        <div class="form-inline pl-2 p-0 mb-10">
                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="Express">
                            <!-- <span class="bg-light pl-2 off-value" > 27% OFF</span> -->
                            <!--<div class="product-rating m-0 ml-2"><span class="badge badge-color"><i class="las la-star"></i> 5.0</span> <span class="rating-review product">(1)</span></div>-->
                        </div>
                    </a>
                </div>
            @endforeach    
                <!-- <div style="width:auto; height:fit-content; max-width: 155px;">
                    <a href="#" class="text-decoration-non text-dark m-2">
                        <div class=" rounded-lg ml-1 mr-1 top-discount-bar-color text-truncate">
                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                        </div>    
                        <div class="p-1">
                            <img src="public_assets/images/product1.png" class="img-fluid" alt="">
                        </div >
                        <div class="ml-2 mr-2">
                            <span class="product-detail product-description" >Product 1</span>
                        </div>
                        <div class="from-inline pl-2 pr-2">
                            <span class="text-muted currency-name" >BDT</span>
                            <span class="currecny-value"><b>19.50</b></span>
                            <span class="currency-line">BDT</span>
                            <span class="value-line">57</span>
                        </div>
                        <div class="form-inline pl-2 p-0">
                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="">
                            <span class="bg-light pl-2 off-value" > 27% OFF</span>
                        </div>
                    </a>
                </div> -->
                
                
            </div>
        </div>
        @endif
        <!-- Product Carousel close-->         
        
        <!-- Add with Code Open-->
        <!-- open-->
        <!-- <div class="container-fluid bg-light ">
            <div class="row bg-transparent">
                <div class="col p-0 ">
                    <a href="#" class="dropdown-item p-0"><img src="public_assets/images/add rectangle.gif"class=" img-fluid" alt=""></a>
                </div>   
            </div>
            <div class="row bg-transparent">
                <div class="col p-0 ">
                    <a href="#" class="dropdown-item p-0"><img src="public_assets/images/add rectangle small.png"class=" img-fluid" alt=""></a>
                </div>   
            </div>
            <div class="row bg-transparent hide">
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 mb-3 "><img src="public_assets/images/circle/MOBILES.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                        </a>
                        <h6><b>Mobile & Accessories<br/>Get 10% off</b></h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/SPORTS & OUTDOOR.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                        </a>
                        <h6><b>Sports Products <br/>Get 10% off</b></h6> 
                    </div>      
                </div>
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/HOME & KITCHEN.png"class="rounded mb-3 shadow-sm img-fluid" style="background-color: #9ec1a0;" alt="">
                        </a>
                        <h6><b>Kitchen Electronics<br/>Get 10% off</b></h6>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/BEAUTY BOUTIQUE.png"class="rounded mb-3 img-fluid" style="background-color: #9ec1a0;" alt="">
                        </a>
                        <h6><b>Fashion & Cosmetics<br/>Get 10% off</b></h6>
                    </div>
                </div>   
            </div>
        </div> -->
        <!-- opne-->
        <!-- Add with Code Close-->


        
        <!-- Add Mega Deal Open -->
        <div class="container-fluid p-3 hide">
            <div class="row ">
                <div class="col-12">
                    <h5><b>MEGA DEALS</b></h5>
                </div>
            </div>
            <div class="row ">
                <div class="col-6 col-md-3 pb-2">
                    <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                </div>
                <div class="col-6 col-md-3 pb-2">
                    <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                </div>
                <div class="col-6 col-md-3 pb-2">
                    <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                </div>
                <div class="col-6 col-md-3 pb-2">
                    <img src="public_assets/images/add shop now.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
        <!-- Add Mega Deal Open -->
        <!--Carousel Open  -->
        @foreach ($cat_list1 as $cat)
        @if(count($cat->products) > 0)
        <div class="container-fluid  mt-3 my-3 bg-light design-arrow">
            <div class="pl-4">
                <!-- <h5><b>Health Care Essentials</b></h5> -->
                <h5><b>{{$cat->name }}</b></h5>
            </div>
            <div id="recommended-for-you-3" class="owl-carousel ">
                
            @foreach ($cat->products as $value) 
                <div class="product-style" >
                    <a role="button" class="product-wishicon productwish"><i class="lab la-gratipay"></i></a>
                    <a href="{{ route('front.get.product', str_slug($value->slug)) }}" class="text-decoration-non text-dark m-2">
                        <!-- <div class=" rounded-lg ml-1 mr-1 top-discount-bar-color text-truncate">
                            <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                        </div>     -->
                        <div class="p-1">
                            <img src="{{ asset("storage/$value->cover") }}" class="img-fluid product_image" alt="">
                        </div >
                        <div class="ml-2 mr-2 mt-2  mb-10">
                            <span class="product-detail product-description" >{{$value->name}}</span>
                        </div>
                        <div class="from-inline pl-2 pr-2 mb-10">
                            <span class="text-muted currency-name" ><b>{{ config('cart.currency_symbol') }}</b></span>
                            <span class="currecny-value"><b>@if ($value->sale_price) {{$value->sale_price}} @else {{$value->price}} @endif</b></span>
                            <!-- <span class="currency-line">BDT</span>
                            <span class="value-line">57</span> -->
                            <div>
                            @if ($value->discount)
                            <p class=" pl-2 pr-2 mb-0"><strike><span>{{ config('cart.currency_symbol') }}</span> {{$value->price}}</strike> <span class="text-pertage"> {{ $value->discount}}%</span></p>
                            @else
                            &nbsp;
                            @endif
                        </div>
                        </div>
                        
                        <div class="form-inline pl-2 p-0 mb-10">
                            <img src="public_assets/images/PRODUCT EX.png" class="image-tag"  alt="Express">
                            <!-- <span class="bg-light pl-2 off-value" > 27% OFF</span> -->
                            <!--<div class="product-rating m-0 ml-2"><span class="badge badge-color"><i class="las la-star"></i> 5.0</span> <span class="rating-review product">(1)</span></div>-->
                        </div>
                    </a>
                </div>


                @endforeach
            </div>
            
        </div>
        @endif
        @endforeach
        <!-- Product Carousel close-->


        

        <!--Carousel Open  -->
        <div class="container-fluid p-0 mt-3 bg-light">
    
            <div id="carouselExampleIndicators1" class="carousel slide bg-light " data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators1" data-slide-to="0" class=" indicator-color active"></li>
                    <li data-target="#carouselExampleIndicators1" data-slide-to="1" class="indicator-color"></li>
                    <li data-target="#carouselExampleIndicators1" data-slide-to="2" class="indicator-color"></li>
                    <li data-target="#carouselExampleIndicators1" data-slide-to="3" class="indicator-color"></li>
                </ol>
                <div class="carousel-inner ">
                    <div class="carousel-item active ">
                        <a href="#"><img src="public_assets/images/Banner 1.png" class="d-block w-100 " style="min-height: 170px;"  alt="..."></a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;" alt="...">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="#">
                            <img src="public_assets/images/Banner 1.png" class="d-block w-100" style="min-height: 170px;"alt="...">
                        </a>
                    </div>
                </div>
                <a class="carousel-control-prev " href="#carouselExampleIndicators1" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon " aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleIndicators1" role="button" data-slide="next">
                <span class="carousel-control-next-icon " aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!--Carousel Close -->

        
        
        
        <!-- Featured Brand open-->
        <div class="container-fluid bg-white pt-5 pl-4 pr-4 bg-light hide">
            <div class="d-flex justify-content-between">
                <div class="">
                    <h5><b>FEATURED BRAND</b></h5>
                </div>
                <div class="">
                    <button type="button" class="btn btn-light btn-sm border">View All</button>
                </div>
            </div>
            <div class="row bg-transparent" id="featured-brands">
                <div class="col-3 col-md-2 p-2 "  >
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;" ><img src="public_assets/images/FEATURED BRAND.png"class="img-fluid" alt=""></a>
                </div>
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>       
                </div>
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item"style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>  
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>  
                <div class="col-3 col-md-2 p-2">
                    <a href="#" class="dropdown-item" style="background-color: #f7f7f7;"><img src="public_assets/images/FEATURED BRAND.png"class=" img-fluid" alt=""></a>
                </div>  
            </div>
        </div>
        <!-- Featured Brand close-->
        
        <!-- open-->
        <div class="container-fluid bg-white p-5 bg-light hide">
            <div class="row bg-transparent">
                <div class="col p-2 ">
                    <a href="#" class="dropdown-item"><img src="public_assets/images/Banner 2.png"class="border shadow-sm img-fluid bg-green" alt=""></a>
                </div>   
            </div>
            <div class="row bg-transparent">
                <div class="col-sm-6 col-md-3 p-2 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/MOBILES.png"class="rounded shadow-sm img-fluid mb-3 bg-green" alt="">
                        </a><br>
                        <h5><b>MOBILES</b></h5>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 p-2 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/SPORTS & OUTDOOR.png"class="rounded shadow-sm img-fluid mb-3 bg-green" alt="">
                        </a><br>
                        <h5><b>SPORTS</b></h5> 
                    </div>      
                </div>
                <div class="col-sm-6 col-md-3 p-2 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/HOME & KITCHEN.png"class="rounded shadow-sm img-fluid mb-3 bg-green" alt="">
                        </a><br>
                        <h5><b>KITCHEN ELECTRONICS</b></h5>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 p-2 col-6">
                    <div class="text-center">
                        <a href="#" class="text-center mt-2 "><img src="public_assets/images/circle/GROCERIES.png"class="rounded shadow-sm img-fluid mb-3 bg-green" alt="">
                        </a><br>
                        <h5><b>GROSERIES</b></h5>
                    </div>
                </div>   
            </div>
        </div>
</div>
<script>
    // Wishlist icon selected toggle
$(document).ready(function(){
    $(".product-wishicon").click(function(){
        $(this).toggleClass("active");
    });
});
</script>
@endsection
