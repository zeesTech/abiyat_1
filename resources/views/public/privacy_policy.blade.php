@extends('layouts.public.app')
@section('content')

<div class="privacy-modify">
    <p>PRIVACY POLICY</p>
    <span>Last modified: March 03, 2021</span>
</div>
<div class="privacy-wrap">

    <p>Abiyat.com respects your privacy. Abiyat.com knows that you care how information about you is used and shared,
        and we appreciate your trust that we will do so carefully and sensibly.</p>

    <h4>We want you to </h4>
    <ul>
        <li>• Feel our website trustworthy.</li>
        <li>• Feel comfortable using our website.</li>
        <li>• Feel secure to submit your informations.</li>
        <li>• Contact us with your questions or concerns about privacy on this site.</li>
        <li>• Know that by using our sites you are consenting to the collection of certain data.</li>
    </ul>

    <h4>What information is, or may be, collected from you?</h4>
    <p>We will automatically receive and collect certain anonymous information in standard usage logs through our Web
        server, including computer-identification information obtained from "cookies," sent to your browser from a web
        server cookie stored on your hard drive an IP address, assigned to the computer which you use the domain server
        through which you access our service the type of computer you're using the type of web browser you're using.</p>

    <h4>We may collect the following personally identifiable information about you -</h4>
    <ul>
        <li>• Name including first and last name.</li>
        <li>• Alternate email address.</li>
        <li>• Mobile phone number and contact details.</li>
        <li>• ZIP/Postal code.</li>
        <li>• Financial information (like account or credit card numbers) - Opinions of features on our websites.</li>
        <li>• Other information as per our registration process.</li>
    </ul>

    <h4>We may also collect the following information -</h4>
    <ul>
        <li>• About the pages you visit/access.</li>
        <li>• The links you click on our site.</li>
        <li>• The number of times you access the page.</li>
        <li>• The number of times you have shopped on our web site.</li>
    </ul>
    <p>You can terminate your account at any time. However, your information may remain stored in archive on our servers
        even after the deletion or the termination of your account.</p>

    <h4>How is the information used</h4>
    <p>We use your personal information to:</p>
    <ul>
        <li>• Make our bond more stronger by knowing your interests and tailoring our site to that</li>
        <li>• To get in touch with you when necessary</li>
        <li>• To provide the services requested by you</li>
        <li>• To preserve social history as governed by existing law or policy</li>
    </ul>

    <h4>We use contact information internally to:</h4>
    <ul>
        <li>• direct our efforts for product improvement</li>
        <li>• contact you as a survey respondent</li>
        <li>• notify you if you win any contest; and</li>
        <li>• send you promotional materials from our contest sponsors or advertisers</li>
    </ul>

    <h4>Generally, we use anonymous traffic information to:</h4>
    <ul>
        <li>• remind us of who you are in order to deliver to you a better and more personalized service from both an
            advertising and an editorial perspective</li>
        <li>• recognize your access privileges to our Websites</li>
        <li>• track your entries in some of our promotions, sweepstakes and contests to indicate a player's progress
            through the promotion and to track entries, submissions, and status in prize drawings</li>
        <li>• make sure that you don't see the same ad repeatedly</li>
        <li>• help diagnose problems with our server</li>
        <li>• administer our websites, track your session so that we can understand better how people use our sites</li>
    </ul>

    <h4>With whom will your information be shared?</h4>
    <p>We will not use your financial information for any purpose other than to complete a transaction with you. We do
        not rent, sell or share your personal information and we will not disclose any of your personally identifiable
        information to third parties unless:</p>
    <ul>
        <li>• we have your permission</li>
        <li>• to provide products or services you've requested</li>
        <li>• to help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud,
            potential threat to the safety or security of any person, violations of Abiyat.com's terms of use or to
            defend against legal claims</li>
        <li>• Special circumstances such as compliance with subpoenas, court orders, requests/order, notices from legal
            authorities or law enforcement agencies requiring such disclosure.</li>
    </ul>
    <p>We share your information with advertisers on an aggregate basis only.</p>

    <h4>Your rights</h4>
    <p>If you are concerned about your data you have the right to request access to the personal data which we may hold
        or process about you. You have the right to require us to correct any inaccuracies in your data free of charge.
        At any stage you also have the right to ask us to stop using your personal data for direct marketing purposes.
    </p>

    <h4>Policy updates</h4>
    <p>We reserve the right to change or update this policy at any time by placing a prominent notice on our site. Such
        changes shall be effective immediately upon posting to this site.</p>

</div>

@endsection