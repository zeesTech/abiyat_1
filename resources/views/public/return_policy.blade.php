@extends('layouts.public.app')
@section('content')
<div class="privacy-modify">
    <p>Return Policy</p>
    <span>Last modified: March 03, 2021</span>
</div>
<div class="privacy-wrap">
    <h4>Items shipped from noon retail, including deals, can be returned within 3 days of receipt of the shipment. </h4>
    <p><b>If you have received a wrong product</b> (a product that is not as described on
        the Site; or a damaged product.) You can return if product is unused, in original unbroken
        packaging and includes all tags. For electronics, open products will be accepted if found defective,
        different from description or picture displayed. <b>Change of mind not applicable.</b> </p>

    <h4>You do not have a right to return, replace or exchange products in respect of: </h4>
    <ul>
        <li>• Products with tampered or missing serial numbers </li>
        <li>• Products that are classified as hazardous materials or use flammable liquids or gases </li>
        <li>• Products that have been used or damaged by you or are not in the same condition as you received them </li>
        <li>• Any consumable product which has been used or installed</li>
        <li>• Products that fall under specific categories, including food, beverages, household goods, digital books,
            swimwear, hosiery, underwear, socks, health, contact lenses, hygiene related and personal care products and
            certain baby products (e.g. tethers, diapers, hygiene tissues, feeding related products) music, video and
            video games </li>
    </ul>

    <h4>How to return: </h4>
    <p>Contact Abiyat Customer Care team by calling 01725844744 or by emailing care@abiyat.com within 03 days after
        receiving your order.

        There are two ways to return/replacement the product to us. In Dhaka City, we offer a free pick-up service for
        your return/replacement. Other than Dhaka City, you have to send the product on your own to our office address.

        Once we pick up or receive your return, we will do a quality check of the product at our end and if a return is
        invalid, we will replace the product with a new one or we will proceed with the refund. </p>

    <h4>You will receive a refund anytime between 7-10 working days. If you don’t receive refund within this time,
        please write to us at care@abiyat.com and we shall investigate. </h4>
</div>

@endsection