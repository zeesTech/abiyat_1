@extends('layouts.public.app')
@section('content')
<style>
.mob-botm-copyright{
    display: none !important;
}
</style>
<section>
    <div class="container-fluid mob-catg-main">
        <div class="custom-row">
            <div class="custom-col-1">
                <div class="mob-sidebar">
                    <div class="mob-sidebar-title-wrapper">
                        <a href="javascript:;" onclick="openCity('catg-tab-1')">
                            <h2 class="mob-sidebar-title">Just for you</h2>
                        </a>
                    </div>
                    <div class="mob-sidebar-menu">
                        <ul>
                        @foreach ($cat_list as $category)
                        @if($category->status != 0) 
                        
                            <li><button type="button" onclick="openCity('catg-tab-{{$category->id}}')" >{{$category->name}}</button></li>
                        @endif
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @foreach ($cat_list as $category)
                    @if($category->status != 0)
                    <div class="custom-col-2" id="catg-tab-{{$category->id}}">
                    <h2 class="main-catg-title">{{$category->name}}</h2>
                     <!-- banner -->
                    <div class="mob-category-banner">
                        <img src="public_assets/images/mobile-category/banner-mob-bang.png" alt="">
                    </div>
                        @if($category->children()->count() > 0)
                            @foreach($category->children as $sub)
                            <a href="{{ route('front.category.slug', str_slug($category->slug)) }}" class="catg-card">
                                <div class="catg-img">
                                    <img src="{{ asset("storage/$sub->cover") }}" alt="">
                                </div>
                                <div class="catg-text">
                                    <h3>{{ $sub->name }}</h3>
                                </div>
                            </a>
                            @endforeach
                        @endif
                        
                    </div> 
                    @endif
            @endforeach            
           
            <div class="custom-col-2" id="catg-tab-1">
                <!-- banner -->
                <div class="mob-category-banner">
                    <img src="public_assets/images/mobile-category/banner-mob-bang.png" alt="">
                </div>
                @foreach ($cat_list as $category)
                    @if($category->status != 0) 
                    <a href="{{ route('front.category.slug', str_slug($category->slug)) }}" class="catg-card">
                        <div class="catg-img">
                            <img src="{{ asset("storage/$category->cover") }}" alt="">
                        </div>
                        <div class="catg-text">
                            <h3>{{$category->name}}</h3>
                        </div>
                    </a>             
                    @endif
                @endforeach
               
                <!-- <div class="panel-group" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                 
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading11">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupOpen"
                                href="#collapseOpen11" aria-expanded="false" aria-controls="collapse11">
                                Watches, Bags & Accessories
                            </a>
                        </div>
                        <div id="collapseOpen11" class="panel-collapse collapse" role="tabpanel"
                            aria-labelledby="headingLeftOne">
                            <div class="panel-body">
                                <a href="#" class="catg-card">
                                    <div class="catg-img">
                                        <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                                    </div>
                                    <div class="catg-text">
                                        <h3>Footware & Fasion</h3>
                                    </div>
                                </a>
                                <a href="#" class="catg-card">
                                    <div class="catg-img">
                                        <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                                    </div>
                                    <div class="catg-text">
                                        <h3>Footware & Fasion</h3>
                                    </div>
                                </a>
                                <a href="#" class="catg-card">
                                    <div class="catg-img">
                                        <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                                    </div>
                                    <div class="catg-text">
                                        <h3>Footware & Fasion</h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                
                <!-- Top Brands -->
                <!-- <div class="catg-brand">
                    <div class="main-catg-title-section">
                        <h2 class="brand-title">Top Brands</h2>
                    </div>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                    <a href="#" class="brand-card">
                        <div class="catg-img">
                            <img src="public_assets/images/mobile-category/fashion-footwear.jpg" alt="">
                        </div>
                    </a>
                </div> -->
            </div>
        </div>
    </div>
</section>

@endsection
<script>
function openCity(tabid) {
  var i;
  var x = document.getElementsByClassName("custom-col-2");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(tabid).style.display = "flex";
}
</script>