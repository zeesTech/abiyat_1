@extends('layouts.public.app')
@section('content')
<style>
.custom-checkbox .custom-control-indicator .custom-control-input {
    background-color: #fff;
    border: 1px solid #2c2b2c;
    border-radius: 50%;
    height: 25px;
    width: 25px;
}

.custom-control-input:checked~.custom-control-label::before {
    color: #343a40;
    border-color: #005e00;
    background-color: #005e00;
}

.floatLeft {
    display: inline-block;
}

.shadow-sm-category {
    /* border: 1px solid #000000; */
    border-radius: 50%;
    padding: 0%;
    background-color: #e5efe5;
}

.categories-section::-webkit-scrollbar {
    width: 0px;
    background: transparent;
    /*/ make scrollbar transparent / */
}

.icon-control {
    margin-top: 5px;
    float: right;
    font-size: 80%;
}

.list-menu a {
    color: #343a40;
}

.list-menu a:hover {
    color: #005e00;
}

.custom-control-label {
    color: #000;
}
</style>
<section class="section-content">
    <div class="container-fluid p-3 bg-light">
        <div class="row">
            <div class="col-12">
                <!-- Breadcrumbs open -->
                <div class="container-fluid">
                    <div class="row">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-arrow p-0">
                                <li class="breadcrumb-item"><a href="#" class="text-uppercase pl-3">Home</a></li>
                                <!-- <li class="breadcrumb-item pl-0"><a href="#" class="text-uppercase">PAGE 1</a></li>
                                <li class="breadcrumb-item pl-0"><a href="#" class="text-uppercase">PAGE 2</a></li>
                                <li class="breadcrumb-item pl-0"><a href="#" class="text-uppercase">PAGE 3</a></li> -->
                                <li aria-current="page" class="breadcrumb-item pl-0 active text-uppercase pl-4">
                                    {{$category->name}}</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <!-- Breadcrumbs close -->
            </div>
        </div>
        <div class="row sidebar-mob">
            <!-- side bar category col open -->
            <aside class="col-md-3 hideshow-web" style="color:#005e00">
                <div class="card">
                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_1" aria-expanded="true" class=""
                                style="color:#005e00">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Category</h6>
                            </a>
                        </header>
                        <div class="filter-content collapse show" id="collapse_1">
                            <div class="card-body">
                                <!-- <form class="pb-3">
                                <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-light" type="button"><i class="fa fa-search"></i></button>
                                </div>
                                </div>
                                </form> -->

                                <?php $data=['categories' => $categories] ?>
                                @include('front.categories.sidebar-category',$data)

                            </div> <!-- card-body.// -->
                        </div>
                    </article> <!-- filter-group  .// -->
                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_2" aria-expanded="true" class=""
                                style="color:#005e00">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Brands </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse show" id="collapse_2">
                            <div class="card-body">

                                @foreach($brands as $brand)
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input">
                                    <div class="custom-control-label">{{$brand->name}}
                                        <b class="badge badge-pill badge-light float-right bg-light"></b>
                                    </div>
                                </label>
                                @endforeach

                            </div> <!-- card-body.// -->
                        </div>
                    </article> <!-- filter-group .// -->

                    <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_3" aria-expanded="true" class=""
                                style="color:#005e00">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Price range </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse show" id="collapse_3">
                            <div class="card-body">
                                <input type="range" class="custom-range" min="0" max="100" name=""
                                    style="color:#005e00">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Min</label>
                                        <input class="form-control" placeholder="$0" type="number">
                                    </div>
                                    <div class="form-group text-right col-md-6">
                                        <label>Max</label>
                                        <input class="form-control" placeholder="$1,0000" type="number">
                                    </div>
                                </div> <!-- form-row.// -->
                                <button class="btn btn-block" style="color:#fff;background-color:#005e00">Apply</button>
                            </div><!-- card-body.// -->
                        </div>
                    </article> <!-- filter-group .// -->

                    <!-- <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_4" aria-expanded="true" class="" style="color:#005e00">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">Sizes </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse show" id="collapse_4" >
                            <div class="card-body">
                            <label class="checkbox-btn">
                                <input type="checkbox">
                                <span class="btn btn-light"> XS </span>
                            </label>
                
                            <label class="checkbox-btn">
                                <input type="checkbox">
                                <span class="btn btn-light"> SM </span>
                            </label>
                
                            <label class="checkbox-btn">
                                <input type="checkbox">
                                <span class="btn btn-light"> LG </span>
                            </label>
                
                            <label class="checkbox-btn">
                                <input type="checkbox">
                                <span class="btn btn-light"> XXL </span>
                            </label>
                        </div>
                        </div>
                    </article>  -->

                    <!-- <article class="filter-group">
                        <header class="card-header">
                            <a href="#" data-toggle="collapse" data-target="#collapse_5" aria-expanded="false" class="" style="color:#005e00">
                                <i class="icon-control fa fa-chevron-down"></i>
                                <h6 class="title">More filter </h6>
                            </a>
                        </header>
                        <div class="filter-content collapse in" id="collapse_5">
                            <div class="card-body">
                                <label class="custom-control custom-radio">
                                <input type="radio" name="myfilter_radio" checked="" class="custom-control-input">
                                <div class="custom-control-label">Any condition</div>
                                </label>
                
                                <label class="custom-control custom-radio">
                                <input type="radio" name="myfilter_radio" class="custom-control-input">
                                <div class="custom-control-label">Brand new </div>
                                </label>
                
                                <label class="custom-control custom-radio">
                                <input type="radio" name="myfilter_radio" class="custom-control-input">
                                <div class="custom-control-label">Used items</div>
                                </label>
                
                                <label class="custom-control custom-radio">
                                <input type="radio" name="myfilter_radio" class="custom-control-input">
                                <div class="custom-control-label">Very old</div>
                                </label>
                            </div>
                        </div>
                    </article>  -->
                </div> <!-- card.// -->
            </aside>
            <!-- side bar category col open -->

            <main class="col-md-9 ">

                <header class="pt-2">
                    <div class="form-inline border-bottom pb-2">
                        <span class="mr-md-auto form-inline"><b class="pr-1">{{count($products)}}</b>Items found for <b
                                class="pl-1">{{ $category->name }}</b></span>
                        <!-- <div class="catg-sort">
                                <div class="sort-text">
                                    Sort By: 
                                </div>
                                <div class="sort-btn">
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle sort-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown button
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">RECOMENDED</a>
                                        <a class="dropdown-item" href="#">PRICE: HIGHT TO LOW</a>
                                        <a class="dropdown-item" href="#">PRICE: LOW TO HIGH</a>
                                        <a class="dropdown-item" href="#">NEW ARRIVAL</a>
                                    </div>
                                    </div>
                                </div>
                            </div> -->
                    </div>
                    <!-- <div class="form-inline border-bottom pt-2 pb-2">
                            <span class="mr-md-auto form-inline mr-2">Sort by:
                                <div>
                                    <button class="btn btn-default filter-button border ml-1" data-filter="photo">Mobiles</button>
                                    <button class="btn btn-default filter-button border ml-1" data-filter="graphic">Apple</button>
                                    <button class="btn btn-default filter-button border ml-1" data-filter="webdesign">Color</button>
                                    <button class="btn filter-button border ml-1" ><i class="fas fa-times"></i></button>
                                </div>
                            </span>
                        </div> -->
                </header>
                <!-- sect-heading -->

                <div class="row d-flex categories-wrapper" style="width: auto; height: auto;">
                    <!-- banner -->
                    <div class="catg-banner"> 
                        <img src="{{ asset("storage/$category->banner") }}" alt="">
                    </div>
                    <!-- Website Filters -->
                    <!-- <div class="catg-filters">
                        <div class="hideshow-web">
                            <span>Filterd By:</span>
                            <ul>
                                <li><button type="button"><span>Brand</span>: <b> <span> Apple</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Color</span>:<b><span> Black</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Seller</span>:<b><span> Abiyat</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Year</span>:<b><span> 2020</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Brand</span>:<b><span> Apple</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Color</span>:<b><span> Black</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Seller</span>:<b><span> Abiyat</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><span>Year</span>:<b><span> 2020</span></b><i
                                            class="las la-times"></i></button></li>
                                <li><button type="button"><i class="las la-times"></i></button></li>
                            </ul>
                        </div>


                         <div class="hideshow-mob">
                            <ul>
                                <li><a href=""><img src="{{ asset('public_assets/images/PRODUCT EX.png')}}" alt=""></a>
                                </li>
                                <li><button type="button" onclick=filterFunction()>Filter<i class="las la-filter"></i></button></li>
                                <div class="card filter-mob filter-hide" id="filterMOB" >
                                    <button type="button" onclick=filterCloseFunction()><i class="las la-times"></i></button>
                                    <article class="filter-group">
                                        <header class="card-header">
                                            <a href="#" data-toggle="collapse" data-target="#collapse_1"
                                                aria-expanded="true" class="" style="color:#005e00">
                                                <i class="icon-control fa fa-chevron-down"></i>
                                                <h6 class="title">Category</h6>
                                            </a>
                                        </header>
                                        <div class="filter-content collapse show" id="collapse_1">
                                            <div class="card-body catg-filters">
                                                <form class="pb-3">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="Search">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-light" type="button"><i
                                                                    class="fa fa-search"></i></button>
                                                        </div>
                                                    </div>
                                                </form>

                                                <?php $data=['categories' => $categories] ?>
                                                @include('front.categories.sidebar-category',$data)

                                            </div>
                                        </div>
                                    </article>
                                    <article class="filter-group">
                                        <header class="card-header">
                                            <a href="#" data-toggle="collapse" data-target="#collapse_2"
                                                aria-expanded="true" class="" style="color:#005e00">
                                                <i class="icon-control fa fa-chevron-down"></i>
                                                <h6 class="title">Brands </h6>
                                            </a>
                                        </header>
                                        <div class="filter-content collapse show" id="collapse_2">
                                            <div class="card-body catg-filters">

                                                @foreach($brands as $brand)
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input">
                                                    <div class="custom-control-label">{{$brand->name}}
                                                        <b
                                                            class="badge badge-pill badge-light float-right bg-light"></b>
                                                    </div>
                                                </label>
                                                @endforeach

                                            </div> 
                                        </div>
                                    </article>

                                    <article class="filter-group">
                                        <header class="card-header">
                                            <a href="#" data-toggle="collapse" data-target="#collapse_3"
                                                aria-expanded="true" class="" style="color:#005e00">
                                                <i class="icon-control fa fa-chevron-down"></i>
                                                <h6 class="title">Price range </h6>
                                            </a>
                                        </header>
                                        <div class="filter-content collapse show" id="collapse_3">
                                            <div class="card-body">
                                                <input type="range" class="custom-range" min="0" max="100" name=""
                                                    style="color:#005e00">
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Min</label>
                                                        <input class="form-control" placeholder="$0" type="number">
                                                    </div>
                                                    <div class="form-group text-right col-md-6">
                                                        <label>Max</label>
                                                        <input class="form-control" placeholder="$1,0000" type="number">
                                                    </div>
                                                </div> 
                                                <button class="btn btn-block"
                                                    style="color:#fff;background-color:#005e00">Apply</button>
                                            </div>
                                        </div>
                                    </article> 

                                    <article class="filter-group">
                                        <header class="card-header">
                                            <a href="#" data-toggle="collapse" data-target="#collapse_4"
                                                aria-expanded="true" class="" style="color:#005e00">
                                                <i class="icon-control fa fa-chevron-down"></i>
                                                <h6 class="title">Sizes </h6>
                                            </a>
                                        </header>
                                        <div class="filter-content collapse show" id="collapse_4">
                                            <div class="card-body ">
                                                <label class="checkbox-btn">
                                                    <input type="checkbox">
                                                    <span class="btn btn-light"> XS </span>
                                                </label>

                                                <label class="checkbox-btn">
                                                    <input type="checkbox">
                                                    <span class="btn btn-light"> SM </span>
                                                </label>

                                                <label class="checkbox-btn">
                                                    <input type="checkbox">
                                                    <span class="btn btn-light"> LG </span>
                                                </label>

                                                <label class="checkbox-btn">
                                                    <input type="checkbox">
                                                    <span class="btn btn-light"> XXL </span>
                                                </label>
                                            </div>
                                        </div>
                                    </article>

                                    <article class="filter-group">
                                        <header class="card-header">
                                            <a href="#" data-toggle="collapse" data-target="#collapse_5"
                                                aria-expanded="false" class="" style="color:#005e00">
                                                <i class="icon-control fa fa-chevron-down"></i>
                                                <h6 class="title">More filter </h6>
                                            </a>
                                        </header>
                                        <div class="filter-content collapse in" id="collapse_5">
                                            <div class="card-body">
                                                <label class="custom-control custom-radio">
                                                    <input type="radio" name="myfilter_radio" checked=""
                                                        class="custom-control-input">
                                                    <div class="custom-control-label">Any condition</div>
                                                </label>

                                                <label class="custom-control custom-radio">
                                                    <input type="radio" name="myfilter_radio"
                                                        class="custom-control-input">
                                                    <div class="custom-control-label">Brand new </div>
                                                </label>

                                                <label class="custom-control custom-radio">
                                                    <input type="radio" name="myfilter_radio"
                                                        class="custom-control-input">
                                                    <div class="custom-control-label">Used items</div>
                                                </label>

                                                <label class="custom-control custom-radio">
                                                    <input type="radio" name="myfilter_radio"
                                                        class="custom-control-input">
                                                    <div class="custom-control-label">Very old</div>
                                                </label>
                                            </div>
                                        </div>
                                    </article>
                                    <div class="btn-filter-wrap">
                                    <button type="button" class="btn btn-primary btn-sm">Reset</button>
                                    <button type="button" class="btn btn-primary btn-sm">Apply</button>
                                    </div>
                                </div>
                                <li><button type="button" onclick=sortFunction()>Sort By<i
                                            class="las la-align-center"></i></button></li>
                                <div id="sortSIDE" class="sort-wrapper sort-hide" onclick=sortCloseFunction()>
                                    <button type="button"><i class="las la-times"></i></button>
                                    <a href="#">Recomended</a>
                                    <a href="#">Low to High</a>
                                    <a href="#">High to Low</a>
                                    <a href="#">New Arrival</a>
                                </div>
                            </ul>
                        </div>
                    </div> -->

                    @foreach($products as $product)
                    <div class="product-style">
                        <a role="button" class="product-wishicon productwish"><i class="lab la-gratipay"></i></a>
                        <a href="{{ route('front.get.product', str_slug($product->slug)) }}"
                            class="text-decoration-non text-dark m-2">
                            <!-- <div class=" rounded-lg ml-1 mr-1 top-discount-bar-color text-truncate">
                                <span class="pl-2 pr-2 top-discount-bar" >get 10% discount</span>
                            </div> -->
                            <div class="p-1">
                                @if(isset($product->cover))
                                <img src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}"
                                    class="img-fluid product_image">
                                @else
                                <img src="https://placehold.it/263x330" alt="{{ $product->name }}"
                                    class="img-bordered img-responsive" />
                                @endif
                            </div>
                            <div class="ml-2 mr-2 mt-2  mb-10">
                                <span class="product-detail product-description" >product description product description product description product description product description product description</span>
                            </div>
                            
                            <div class="from-inline pl-2 pr-2 mb-10">
                                <span class="text-muted currency-name" ><b>{{ config('cart.currency_symbol') }}</b></span>
                                <span class="currecny-value"><b>@if ($product->sale_price) {{$product->sale_price}} @else {{$product->price}} @endif</b></span>
                                <!-- <span class="currency-line">BDT</span>
                                <span class="value-line">57</span> -->
                                <div>
                                @if ($product->discount)
                                    <p class=" pl-2 pr-2 mb-0"><strike><span>{{ config('cart.currency_symbol') }}</span> {{$product->price}}</strike> <span class="text-pertage"> {{ $product->discount}}%</span></p>
                                @else
                                &nbsp;
                                @endif
                                </div>
                            </div>
                            <div class="form-inline pl-2 p-0 mb-10">
                                <img src="{{asset('public_assets/images/PRODUCT EX.png')}}" class="image-tag"  alt="Express">
                                <!-- <span class="bg-light pl-2 off-value" > 27% OFF</span> -->
                                <!--<div class="product-rating m-0 ml-2"><span class="badge badge-color"><i class="las la-star"></i> 5.0</span> <span class="rating-review product">(1)</span></div>-->
                            </div>
                        </a>
                    </div>
                    @endforeach



                </div> <!-- row end.// -->


                <!-- <nav class="mt-4 d-flex justify-content-center" aria-label="Page navigation sample">
                <ul class="pagination">
                    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
                </nav> -->

            </main> <!-- col.// -->

        </div>

    </div> <!-- container .//  -->
</section>
<script>
function sortFunction() {
    var showsort = document.getElementById("sortSIDE");
    showsort.classList.remove("sort-hide");
    showsort.classList.add("sort-show");
}

function sortCloseFunction() {
    var hidesort = document.getElementById("sortSIDE");
    hidesort.classList.remove("sort-show");
    hidesort.classList.add("sort-hide");
}
function filterFunction(){
    var showfilter = document.getElementById("filterMOB");
    showfilter.classList.remove("filter-hide");
    showfilter.classList.add("filter-show");
} 
function filterCloseFunction(){
    var hidefilter = document.getElementById("filterMOB");
    hidefilter.classList.remove("filter-show");
    hidefilter.classList.add("filter-hide");
} 
</script>
<script>
    // Wishlist icon selected toggle
$(document).ready(function(){
    $(".product-wishicon").click(function(){
        $(this).toggleClass("active");
    });
});
</script>
@endsection