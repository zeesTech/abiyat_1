<a @if(request()->segment(2) == $category->slug) class="active" @endif href="{{ route('front.category.slug', $category->slug) }}">{{ $category->name }}</a>
<ul class="list-unstyled sidebar-category-sub">
@foreach($subs as $sub) 
        <li><a @if(request()->segment(2) == $sub->slug) class="active" @endif href="{{ route('front.category.slug', $sub->slug) }}">{{ $sub->name }}</a></li>
@endforeach
</ul>