<style>
	@media screen and (max-width: 600px) {
  footer {
    visibility: hidden;
    display: none;
  }
}
</style>
<footer class="footer">
		<div class="container-fluid bg-lite pt-4 mb-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<h5 class="mb-0"><b>We're Always Here To Help</b></h5>
						<p>Reach out to us through any of these support channels</p>
					</div>
					<div class="col-lg-6 col-md-12 help-line">
						<div class="row">
							<div class="col-md-4">
								<a href="https://help.abiyat.com/hc/en-us" target="_blank">
	    							<div class="icon float-left mr-2">
	    								<i class="fa fa-info"></i>
	    							</div>
	    							<div class="text">
	    								<p class="mb-0">HELP CENTER</p>
	    								<h6><b>help.abiyat.com</b></h6>
	    							</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="#" href="mailto:care@abiyat.com" target="_blank" >
	    							<div class="icon float-left mr-2">
	    								<i class="fa fa-envelope-o"></i>
	    							</div>
	    							<div class="text">
	    								<p class="mb-0">EMAIL SUPPORT</p>
	    								<h6><b>care@abiyat.com</b></h6>
	    							</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="tel:80038888" target="_blank">
	    							<div class="icon float-left mr-2">
	    								<i class="fa fa-phone"></i>
	    							</div>
	    							<div class="text">
	    								<p class="mb-0">PHONE SUPPORT</p>
	    								<h6><b>01725844744</b></h6>
	    							</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-4 col-sm-6 mb-3">
					<div class="footer-link">
						<h5>ELECTRONICS</h5>
						<a href="#">Mobiles</a>
						<a href="#">Tablets</a>
						<a href="#">Laptops</a>
						<a href="#">Home Appliances</a>
						<a href="#">Camera, Photo & Video</a>
						<a href="#">Televisions</a>
						<a href="#">Headphones</a>
						<a href="#">Video Games</a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6  mb-3">
					<div class="footer-link">
						<h5>FASHION</h5>
						<a href="#">Women's Fashion</a>
						<a href="#">Men's Fashion</a>
						<a href="#">Girls' Fashion</a>
						<a href="#">Boys' Fashion</a>
						<a href="#">Watches</a>
						<a href="#">Jewellery</a>
						<a href="#">Women's Handbags</a>
						<a href="#">Men's Eyewear</a>

					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6  mb-3">
					<div class="footer-link">
						<h5>HOME AND KITCHEN</h5>
						<a href="#">Bath</a>
						<a href="#">Home Decor</a>
						<a href="#">Kitchen & Dining</a>
						<a href="#">Tools & Home Improvement</a>
						<a href="#">Audio & Video</a>
						<a href="#">Furniture</a>
						<a href="#">Patio, Lawn & Garden</a>
						<a href="#">Pet Supplies</a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 mb-3">
					<div class="footer-link">
						<h5>BEAUTY</h5>
						<a href="#">Fragrance</a>
						<a href="#">Make-Up</a>
						<a href="#">Haircare</a>
						<a href="#">Skincare</a>
						<a href="#">Personal Care</a>
						<a href="#">Tools & Accessories</a>
						<a href="#">Men's Grooming</a>
						<a href="#">Health Care Essentials</a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 mb-3">
					<div class="footer-link">
						<h5>BABY</h5>

						<a href="#">Strollers & Prams</a>
						<a href="#">Car Seats</a>
						<a href="#">Feeding</a>
						<a href="#">Bathing & Skincare</a>
						<a href="#">Diapering</a>
						<a href="#">Baby Clothing & Shoes</a>
						<a href="#">Baby & Toddler Toys</a>
						<a href="#">Baby Foods</a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 mb-3">
					<div class="footer-link">
						<h5>TOP BRANDS</h5>
						<a href="#">Mothercare</a>
						<a href="#">Apple</a>
						<a href="#">Nike</a>
						<a href="#">Samsung</a>
						<a href="#">Tefal</a>
						<a href="#">L'Oreal Paris</a>
						<a href="#">Skechers</a>
						<a href="#">Silsal</a>
					</div>
				</div>
			</div>
			<div class="row mt-5 mb-3">
				<div class="col-md-6 mb-3">
					<div class="text-center">

						<h5 class="h5 mb-0">SHOP ON THE GO</h5>
						<a href="#">
							<img src="{{ asset('public_assets/images/google-play.svg')}}" width="100" height="50" alt="" title="">
						</a>
						<a href="#"><img src="{{asset('public_assets/images/app-store.svg')}}" width="100" height="50" alt="" title=""></a>

					</div>
				</div>
				<div class="col-md-6">
					<div class="text-center sociallinks">
						<h5 class="h5 mb-1">CONNECT WITH US</h5>
						<a href="#">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="#">
							<i class="fa fa-twitter"></i>
						</a>
						<a href="#">
							<i class="fa fa-instagram"></i>
						</a>
						<a href="#">
							<i class="fa fa-linkedin"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid bg-lite pt-4 mb-3">
			<div class="row">
				<div class="col-md-3">
					<p>© 2020 <a href="#">abiyat.com</a> All Rights Reserved</p>
				</div>
				<div class="col-md-3">
					<img src="{{asset('public_assets/images/card.png')}}" alt="" title="" width="150">
				</div>
				<div class="col-md-6">
					<!-- <a href="#" class="mr-3">Careers</a>-->
					<a href="{{ URL('return_policy') }}" class="mr-3">Return Policy</a>
					<!-- <a href="#" class="mr-3">Sell with us</a>  -->
					<a href="{{ URL('terms_condition') }}" class="mr-3">Terms of Use</a>
					<!-- <a href="#" class="mr-3">Terms of Sale</a> -->
					<a href="{{ URL('privacy_policy') }}" class="mr-3">Privacy Policy</a>
					<!-- <a href="#" class="mr-3">Consumer Rights</a> -->
					<a href="{{ URL('https://abiyat.online/') }}" class="mr-3">Careers</a>
					<!-- <a href="#" class="mr-3">Consumer Rights</a> -->
				</div>
			</div>
		</div>
     </footer>

	 <!-- <script>
		function initFreshChat() {
			window.fcWidget.init({
			token: "e9870c52-005f-423f-b367-43e8520b22d1",
			host: "https://wchat.eu.freshchat.com"
			});
		}
		function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.eu.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
	</script> -->
<script>
	$(function () {
		$('#myaccclick').click(function () {
		// Using an if statement to check the class
			if ($("#myacc").hasClass('hidme')) {
			// The box that we clicked has a class of bad so let's remove it and add the good class
			$("#myacc").removeClass('hidme');
			
			} else {
			// The user obviously can't follow instructions so let's alert them of what is supposed to happen next
			//   alert("You can proceed!");
			}
		});

		$('.checkout-btn').click(function(){
			if($('#terms_condition').prop("checked") == false){
			alert('Please accept terms and condition to proceed');
			return false;
			}
			window.location.href = "{{ route('checkout.index') }}";
			
		});
	});
</script>
    