<!doctype html>

<head lang="en">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="title" content="Abiyat online shopping ">
    <meta name="description"
        content="Abiyat is the Bangladesh online marketplace. Buy your favourite fashion, electronics, beauty, home & baby products online in Bangladesh">
    <meta name="keywords"
        content="Bangladesh shop, online shop Bangladesh, latest product Bangladesh, new brand shopping, abiyat shopping, electric product, latest phone,  computer item, fast delivery, অনলাইন সপ, নতুন সমাহার, বাংলাদেশ অনলাইন শপিং মল,">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="IBRAHIM SALIM">

    <!-- Google Font -->
    <!-- <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"> -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
    <!-- Font awesome -->
    <script src="https://kit.fontawesome.com/d15b038ad9.js" crossorigin="anonymous"></script>
    <!-- Line awesome -->
    <link rel="stylesheet"
        href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custome style sheet -->
    <link rel="stylesheet" href="{{asset('public_assets/css/stylecustom.css')}}" type="text/css">
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('public_assets/images/abiyat-favicon.png')}}" type="image/gif" sizes="25x25">
    <!-- Select2 style sheet -->
    <link rel="stylesheet" href="{{asset('public_assets/css/select2.css')}}" type="text/css">

    <link rel="stylesheet" href="{{asset('public_assets/owl_carousel/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('public_assets/owl_carousel/css/owl.theme.min.css')}}" type="text/css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://kit.fontawesome.com/d15b038ad9.js" crossorigin="anonymous"></script>
    <!--  -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!--  -->
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpaWOv_Ghwh9tiOhkWmCa7X6ug8q749gY&callback=initMap&libraries=places&v=weekly"
        async></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{asset('public_assets/owl_carousel/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/front.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <title>ABIYAT</title>
    <style>
    .floatLeft {
        display: inline-block;
    }

    .shadow-sm-category {
        /* border: 1px solid #000000; */
        border-radius: 50%;
        padding: 0%;
        background-color: #e5efe5;
    }

    .categories-section::-webkit-scrollbar {
        width: 0px;
        background: transparent;
        /*/ make scrollbar transparent / */
    }

    .hide {
        display: none;
    }

    .logo-website {
        display: block;
    }

    .logo-mob {
        display: none;
    }

    @media (max-width:991px) {
        .logo-website {
            display: none;
        }

        .logo-mob {
            display: block;
        }
    }
    </style>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153644175-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-153644175-2');
    </script>
</head>

<body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
    <div class="super_container">
        <!-- Header -->
        <header class="header">
            <!-- Top Bar -->
            <!-- <div class="top_bar">
					<div class="container-fluid">
						<div class="row">
							<div class="col d-flex flex-row">
								<div class="top_bar_contact_item">
									<div class="top_bar_icon"><i class="fab fa-whatsapp"></i><a href="#" class="text-white pl-2 ">+91 9823 132 111</a></div>
								</div>
								<div class="top_bar_contact_item">
									<div class="top_bar_icon"><i class="far fa-envelope"></i><a href="#" class="text-white pl-2 ">contact@abiyat.com</a></div>
								</div>
								<div class="top_bar_content ml-auto">
									<div class="top_bar_menu">
										<ul class="standard_dropdown top_bar_dropdown">
											<li style="display: none;"> <a href="#" class="text-white">English<i class="fas fa-chevron-down"></i></a>
												<ul>
													<li><a href="#">Italian</a></li>
													<li><a href="#">Spanish</a></li>
													<li><a href="#">Japanese</a></li>
												</ul>
											</li>
											<li class="text-white"> ৳
											</li>
											@if(auth()->check())
                        <li><a class="text-white" href="{{ route('accounts', ['tab' => 'profile']) }}"><i class="fa fa-home"></i> My Account</a></li>
                        <li><a class="text-white" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                        @else
												<li><a class="text-white" href="{{ route('login') }}"> <i class="fas fa-lock"></i> Login</a></li>
												<li><a class="text-white" href="{{ route('register') }}"> <i class="fas fa-sign-in"></i> Register</a></li>
                        @endif
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>  -->
            <!-- Header Main -->
            <!-- <div class="header_main">
					<div class="container-fluid">
						<div class="row"> -->
            <!-- Logo -->
            <!-- <div class="col-lg-2 col-sm-10 col-3 order-1">
								<div class="logo_container  menu_burger">
								
								
									<div class="logo " style="display: inline-block;"><a href="{{route('home')}}" class="text-white" ><img  src="{{ asset('public_assets/images/abiyat-png.png')}}" /></a></div>
								</div>
							
							</div>  -->
            <!-- Search -->
            <!-- <div class="col-lg-6 col-9 order-lg-2 order-3 text-lg-left text-right">
								<div class="header_search">
									<div class="header_search_content">
										<div class="header_search_form_container">
											<form action="#" class="header_search_form clearfix"> <input type="search" required="required" class="header_search_input" placeholder="Search for products...">
												<div class="custom_dropdown" style="display: none;">
													<div class="custom_dropdown_list"> <span class="custom_dropdown_placeholder clc">All Categories</span> <i class="fas fa-chevron-down"></i>
														<ul class="custom_list clc">
															<li><a class="clc" href="#">All Categories</a></li>
															<li><a class="clc" href="#">Computers</a></li>
															<li><a class="clc" href="#">Laptops</a></li>
															<li><a class="clc" href="#">Cameras</a></li>
															<li><a class="clc" href="#">Hardware</a></li>
															<li><a class="clc" href="#">Smartphones</a></li>
														</ul>
													</div>
												</div> <button type="submit" class="header_search_button trans_300" value="Submit"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918770/search.png" alt=""></button>
											</form>
										</div>
									</div>
								</div>
							</div>  -->
            <!-- Wishlist -->
            <!-- <div class="col-lg-4 col-2 order-lg-3 order-2 text-lg-left text-right wishlist_cart">
								<div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
									<div class="wishlist d-flex flex-row align-items-center justify-content-end">
										<div class="wishlist_icon"><img src="{{ asset('public_assets/images/wishlisticon.png')}}" alt=""></div>
										<div class="wishlist_content">
											<div class="wishlist_text"><a href="#">Wishlist</a></div> -->
            <!-- <div class="wishlist_count">10</div> -->
            <!-- </div>
									</div>  -->
            <!-- Cart -->
            <!-- <a href="{{ route('cart.index') }}">
									<div class="cart">
										<div class="cart_container d-flex flex-row align-items-center justify-content-end">
											<div class="cart_icon"> <img src="{{ asset('public_assets/images/carticon.png')}}" alt="">
												<div class="cart_count"><span>{{ $cartCount }}</span></div>
											</div>
											<div class="cart_content">
												<div class="cart_text"><a href="#">Cart</a></div> -->
            <!-- <div class="cart_price"></div> -->
            <!-- </div>
										</div>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>  -->
            <!-- Main Navigation -->
            <!-- Banner section NEW Main with search-->
            <header class="withsearch fixednot">
                <div class="container-fluid p-1">
                    <!-- menu -->
                    <nav class="navbar navbar-expand-md navbar-light header-menu">
                        <a class="navbar-brand logo-website" href="{{route('home')}}"><img
                                src="{{ asset('public_assets/images/abiyat-png.png')}}" class="logo-img" alt=""></a>
                        <a class="navbar-brand logo-mob" href="{{route('home')}}"><img
                                src="{{ asset('public_assets/images/abiyat-png-mob.png')}}" class="logo-img" alt=""></a>
                        <!-- Search  -->
                        <div class="search-wrapper ">
                            <div class="input-wrapper">
                                <input type="text" placeholder="what are you looking for?">
                            </div>
                            <!-- Search button -->
                            <button type="button" class="btn btn-main-search"> <i class="fas fa-search"></i></button>
                        </div>
                        <!-- Menu items -->
                        <div class=" cart-like" id="navbarSupportedContent">
                            <ul class="navbar-nav ">



                                @if(auth()->check())
                                <li class="nav-item">
                                    <div class="btn-group">
                                        <button type="button" class="btn dropdown-toggle main-account-btn"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            My Account
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="{{ route('accounts', ['tab' => 'orders']) }}">Orders</a>
                                            <a class="dropdown-item"
                                                href="{{ route('accounts', ['tab' => 'addresses']) }}">Addresses</a>
                                            <a class="dropdown-item"
                                                href="{{ route('accounts', ['tab' => 'profile']) }}">Profile</a>        
                                            <a class="dropdown-item" href="{{ route('logout') }}"><i
                                                    class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                </li>


                                @else
                                <li class="nav-item"
                                    style="padding: 5px 35px 0px 35px;border-right: 1px solid white; margin-right: 10px;">
                                    <a class="text-white" href="{{ route('login') }}">
                                        Sign In <i class="las la-user" style="margin-left:8px;font-size:20px;"></i>
                                    </a>
                                </li>
                                <!-- <li><a class="text-white" href="{{ route('register') }}"> <i class="fas fa-sign-in"></i> Register</a></li> -->
                                @endif


                                <li class="nav-item border-right ">
                                    <a href="{{ route('cart.index') }}" class="cart">
                                        <div
                                            class="cart_container d-flex flex-row align-items-center justify-content-end">
                                            <div class="cart_icon wishlist-head">
                                                Wishlist
                                                <i class="far fa-heart"></i>

                                                @if($cartCount != 0 )<div class="cart_count">
                                                    <span>{{ $cartCount }}</span>
                                                </div>@endif
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{ route('cart.index') }}" class="cart">
                                        <div
                                            class="cart_container d-flex flex-row align-items-center justify-content-end">
                                            <div class="cart_icon">
                                                Cart
                                                <i class="las la-shopping-cart"></i>
                                                <!-- <img src="{{ asset('public_assets/images/carticon.png')}}" alt=""> -->

                                                @if($cartCount != 0 )<div class="cart_count">
                                                    <span>{{ $cartCount }}</span>
                                                </div>@endif
                                            </div>
                                            <div class="cart_content">
                                                <!-- <div class="cart_text"><a href="#">Cart</a></div> -->
                                                <!-- <div class="cart_price"></div> -->
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>




            <div class="header_main mobile_header sr-only">
                <div class="container-fluid">
                    <div class="row">
                        <!-- Logo -->
                        <div class="col-lg-2 col-sm-10 col-3 order-1">
                            <div class="logo_container  menu_burger">


                                <div class="logo " style="display: inline-block;">
                                    <!-- website logo -->
                                    <a href="{{route('home')}}" class="logo-website"><img
                                            src="{{ asset('public_assets/images/abiyat-png.png')}}" /></a>
                                    <!-- mobile -->
                                    <a href="{{route('home')}}" class="logo-mob"><img
                                            src="{{ asset('public_assets/images/abiyat-png-mob.png')}}" /></a>
                                </div>
                            </div>

                        </div> <!-- Search -->
                        <div class="col-lg-6 col-9 order-lg-2 order-3 text-lg-left text-right">
                            <div class="header_search">
                                <div class="header_search_content">
                                    <div class="header_search_form_container">
                                        <form action="#" class="header_search_form clearfix"> <input type="search"
                                                required="required" class="header_search_input"
                                                placeholder="What are you looking for?">
                                            <div class="custom_dropdown" style="display: none;">
                                                <div class="custom_dropdown_list"> <span
                                                        class="custom_dropdown_placeholder clc">All Categories</span> <i
                                                        class="fas fa-chevron-down"></i>
                                                    <ul class="custom_list clc">
                                                        <li><a class="clc" href="#">All Categories</a></li>
                                                        <li><a class="clc" href="#">Computers</a></li>
                                                        <li><a class="clc" href="#">Laptops</a></li>
                                                        <li><a class="clc" href="#">Cameras</a></li>
                                                        <li><a class="clc" href="#">Hardware</a></li>
                                                        <li><a class="clc" href="#">Smartphones</a></li>
                                                    </ul>
                                                </div>
                                            </div> <button type="submit" class="header_search_button trans_300"
                                                value="Submit"><img
                                                    src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918770/search.png"
                                                    alt=""></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Navigation -->




            <div class="main_header_area animated">
                <div class="container-fluid">
                    <nav id="navigation1" class="navigation">
                        <!-- Logo Area Start -->
                        <div class="nav-header">
                            <a class="nav-brand" href="index-minimal.html#"></a>
                            <div class="nav-toggle sr-only"></div>
                        </div>
                        <!-- Search panel Start -->

                        <!-- Main Menus Wrapper -->
                        <div class="nav-menus-wrapper">
                            <ul class="nav-menu">

                                <li><a href="#">All Categoires</a>
                                    <ul class="nav-dropdown">
                                        @foreach ($categories as $category)
                                        <li><a
                                                href="{{ route('front.category.slug', str_slug($category->slug)) }}">{{ $category->name }}</a>
                                            @endforeach
                                    </ul>
                                </li>
                                <li><a href="{{route('home')}}">Home</a>
                                </li>
                                <li><a href="{{ route('front.category.slug', 'electronics' ) }}">Electronics</a>
                                </li>
                                <li><a href="{{ route('front.category.slug', 'baby-essential' ) }}">Baby Essential</a>
                                </li>
                                <!-- <li><a href="index-minimal.html#">Portfolio</a>
			              <div class="megamenu-panel">
			                <div class="megamenu-lists">
			                  <ul class="megamenu-list list-col-5">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Masonary</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-2-column.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-3-column.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-4-column.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-6-column.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-full-width.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-5">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Masonary No Gutter</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-2-column-no-gutter.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-3-column-no-gutter.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-4-column-no-gutter.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-6-column-no-gutter.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-masonary-full-width-no-gutter.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-5">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Grid</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-2-column.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-3-column.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-4-column.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-6-column.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-full-width.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-5">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Grid No Gutter</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-2-column-no-gutter.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-3-column-no-gutter.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-4-column-no-gutter.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-6-column-no-gutter.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-grid-full-width-no-gutter.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-5">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Portfolio Details</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-embed-video.html" target="_blank">Embed Video</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-masonary-thumb.html" target="_blank">Masonary Thumb</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-single-thumb.html" target="_blank">Single Thumb</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-slider-gallery.html" target="_blank">Slider Gallery</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-youtube-video.html" target="_blank">YouTube Video</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/portfolio-details-vimeo-video.html" target="_blank">Vimeo Video</a></li>
			                  </ul>
			                </div>
			              </div>
			            </li>
			            <li><a href="index-minimal.html#">Blog</a>
			              <div class="megamenu-panel">
			                <div class="megamenu-lists">
			                  <ul class="megamenu-list list-col-4">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Masonary Layout</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-2c.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-3c.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-4c.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-6c.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-full-width.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-4">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Grid Layout</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-2c.html" target="_blank">2 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-3c.html" target="_blank">3 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-4c.html" target="_blank">4 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-6c.html" target="_blank">6 Column</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-full-width.html" target="_blank">Full Width</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-4">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Sidebar Layout</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-with-left-sb.html" target="_blank">Left Sidebar Grid</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-with-left-sidebar.html" target="_blank">Left Sidebar Masonary</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-grid-with-right-sb.html" target="_blank">Right Sidebar Grid</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-masonary-with-right-sidebar.html" target="_blank">Right Sidebar Masonary</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-without-sidebar.html" target="_blank">No Sidebar</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/blog-with-left-right-sidebar.html" target="_blank">Both Side Sidebar</a></li>
			                  </ul>
			                  <ul class="megamenu-list list-col-4">
			                    <li class="megamenu-list-title"><a href="index-minimal.html#">Blog Details</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-1.html" target="_blank">Single Blog Layout 1</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-2.html" target="_blank">Single Blog Layout 2</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-3.html" target="_blank">Single Blog Layout 3</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-4.html" target="_blank">Single Blog Layout 4</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-5.html" target="_blank">Single Blog Layout 5</a></li>
			                    <li><a href="http://demo.designing-world.com/classy-baao/classy-multipage/single-blog-6.html" target="_blank">Single Blog Layout 6</a></li>
			                  </ul>
			                </div>
			              </div>
			            </li>
			            <li><a href="../classy-shortcodes/index.html" target="_blank">Shortcodes <span class="label label-pill label-primary">New</span></a></li>
			            <li><a href="../classy-onepage/all-demo-pages.html" target="_blank">One Page</a></li> -->
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="bottom-menu-wrapper ">

                @if(!auth()->check())
                <div id="myacc" class="myaccount-wrapper hidme">
                    <div class="welcom-top">
                        <div class="top-text">
                            <h4>Hello! Nice to meet you</h4>
                            <p>You are currently <span>not</span> signed in</p>
                        </div>
                    </div>
                    <!-- if not login -->
                    <div class="accounts-wrapper">
                        <a href="{{ route('accounts', ['tab' => 'profile']) }}" class="circle-cards">
                            <div class="avtr"><i class="las la-user-circle"></i></div>
                            <h6>Sign In</h6>
                        </a>
                        <a href="{{ route('register') }}" class="circle-cards">
                            <div class="avtr avt-up"><i class="las la-user-circle"></i></div>
                            <h6>Sign Up</h6>
                        </a>
                    </div>
                   
                    


                    <div class="title">
                        <h4>SETTING</h4>
                    </div>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="fas fa-globe"></i>
                            <h6>country</h6>
                        </div>
                        <div class="go-title">
                            <h6>BD</h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-flag"></i>
                            <h6>language</h6>
                        </div>
                        <div class="go-title">
                            <h6>BD</h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>

                    <div class="title">
                        <h4>REACH OUT TO US</h4>
                    </div>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-phone-volume"></i>
                            <h6>contact us</h6>
                        </div>
                        <div class="go-title">
                            <h6></h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-info-circle"></i>
                            <h6>help center</h6>
                        </div>
                        <div class="go-title">
                            <h6></h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <div class="always-contact">
                        <h3>We Are Always Here</h3>
                        <a href="#" class="always-help">
                            <i class="las la-info-circle"></i>
                            <div class="text">
                                <p>HELP CENTER</p>
                                <h6>help.abiyat.com</h6>
                            </div>
                        </a>
                        <a href="#" class="always-help">
                            <i class="las la-envelope"></i>
                            <div class="text">
                                <p>EMAIL SUPPORT</p>
                                <h6>care@abiyat.com</h6>
                            </div>
                        </a>
                    </div>

                    <div class="mob-botm-copyright">
                        <div class="row mt-5 mb-3">
                            <div class="col-md-6 mb-3">
                                <div class="text-center">

                                    <h5 class="h5 mb-2">SHOP ON THE GO</h5>
                                    <a href="#">
                                        <img src="{{ asset('public_assets/images/google-play.svg')}}" width="100"
                                            height="50" alt="" title="">
                                    </a>
                                    <a href="#"><img src="{{asset('public_assets/images/app-store.svg')}}" width="100"
                                            height="50" alt="" title=""></a>

                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="text-center sociallinks">
                                    <h5 class="h5 mb-3">CONNECT WITH US</h5>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <p>© 2020 <a href="#">abiyat.com</a> All Rights Reserved</p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <img src="{{asset('public_assets/images/card.png')}}" alt="" title="" width="150">
                                </div>
                                <div class="col-md-6 text-center mt-3">
                                    <!-- <a href="#" class="mr-3">Careers</a>-->
                                    <a href="{{ URL('return_policy') }}" class="mr-3">Return Policy</a>
                                    <!-- <a href="#" class="mr-3">Sell with us</a>  -->
                                    <a href="{{ URL('terms_condition') }}" class="mr-3">Terms of Use</a>
                                    <!-- <a href="#" class="mr-3">Terms of Sale</a> -->
                                    <br>
                                    <a href="{{ URL('privacy_policy') }}" class="mr-3">Privacy Policy</a>
                                    <!-- <a href="#" class="mr-3">Consumer Rights</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>

                @else
                <div id="myacc" class="myaccount-wrapper hidme">
                    <div class="welcom-top">
                        <div class="top-text">
                            <h4>Hello! {{Auth::user()->name}} </h4>
                            <p>{{Auth::user()->email}}</p>
                        </div>
                    </div>
                    <div class="accounts-wrapper">
                        <a href="{{ route('accounts', ['tab' => 'orders']) }}" class="circle-cards">
                            <div class="avtr"><i class="las la-user-circle"></i></div>
                            <h6>Orders</h6>
                        </a>
                        <a href="" class="circle-cards">
                            <div class="avtr"><i class="las la-undo-alt"></i></div>
                            <h6>Returns</h6>
                        </a>
                        <a href="#" class="circle-cards">
                            <div class="avtr"><i class="las la-credit-card"></i></div>
                            <h6>Credits</h6>
                        </a>
                        <a href="#" class="circle-cards">
                            <div class="avtr"><i class="lab la-app-store"></i></div>
                            <h6>Store</h6>
                        </a>
                        
                    </div>

                    <div class="title">
                        <h4>MY ACCOUNT</h4>
                    </div>
                    <a href="{{ route('accounts', ['tab' => 'addresses']) }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-map-marked-alt"></i>
                            <h6>Addresses</h6>
                        </div>
                        <div class="go-title">
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="{{ route('accounts', ['tab' => 'payments']) }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-credit-card"></i>
                            <h6>Payments</h6>
                        </div>
                        <div class="go-title">
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="{{ route('accounts', ['tab' => 'claims']) }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-user-cog"></i>
                            <h6>Claims</h6>
                        </div>
                        <div class="go-title">
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="{{ route('accounts', ['tab' => 'preferences']) }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-id-card-alt"></i>
                            <h6>Prefrences</h6>
                        </div>
                        <div class="go-title">
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="{{ route('accounts', ['tab' => 'profile']) }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-address-card"></i>
                            <h6>Profile</h6>
                        </div>
                        <div class="go-title">
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <div class="title">
                        <h4>SETTING</h4>
                    </div>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="fas fa-globe"></i>
                            <h6>country</h6>
                        </div>
                        <div class="go-title">
                            <h6>BD</h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-flag"></i>
                            <h6>language</h6>
                        </div>
                        <div class="go-title">
                            <h6>BD</h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>

                    <div class="title">
                        <h4>REACH OUT TO US</h4>
                    </div>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-phone-volume"></i>
                            <h6>contact us</h6>
                        </div>
                        <div class="go-title">
                            <h6></h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>
                    <a href="#" class="click-go">
                        <div class="go-title">
                            <i class="las la-info-circle"></i>
                            <h6>help center</h6>
                        </div>
                        <div class="go-title">
                            <h6></h6>
                            <i class="las la-angle-right"></i>
                        </div>
                    </a>

                    <a href="{{ route('logout') }}" class="click-go">
                        <div class="go-title">
                            <i class="las la-info-circle"></i>
                            <h6>Sign Out</h6>
                        </div>
                    </a>


                    <div class="always-contact">
                        <h3>We Are Always Here</h3>
                        <a href="#" class="always-help">
                            <i class="las la-info-circle"></i>
                            <div class="text">
                                <p>HELP CENTER</p>
                                <h6>help.abiyat.com</h6>
                            </div>
                        </a>
                        <a href="#" class="always-help">
                            <i class="las la-envelope"></i>
                            <div class="text">
                                <p>EMAIL SUPPORT</p>
                                <h6>care@abiyat.com</h6>
                            </div>
                        </a>
                    </div>

                    <div class="mob-botm-copyright">
                        <div class="row mt-5 mb-3">
                            <div class="col-md-6 mb-3">
                                <div class="text-center">

                                    <h5 class="h5 mb-2">SHOP ON THE GO</h5>
                                    <a href="#">
                                        <img src="{{ asset('public_assets/images/google-play.svg')}}" width="100"
                                            height="50" alt="" title="">
                                    </a>
                                    <a href="#"><img src="{{asset('public_assets/images/app-store.svg')}}" width="100"
                                            height="50" alt="" title=""></a>

                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="text-center sociallinks">
                                    <h5 class="h5 mb-3">CONNECT WITH US</h5>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <p>© 2020 <a href="#">abiyat.com</a> All Rights Reserved</p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <img src="{{asset('public_assets/images/card.png')}}" alt="" title="" width="150">
                                </div>
                                <div class="col-md-6 text-center mt-3">
                                    <!-- <a href="#" class="mr-3">Careers</a>-->
                                    <a href="{{ URL('return_policy') }}" class="mr-3">Return Policy</a>
                                    <!-- <a href="#" class="mr-3">Sell with us</a>  -->
                                    <a href="{{ URL('terms_condition') }}" class="mr-3">Terms of Use</a>
                                    <!-- <a href="#" class="mr-3">Terms of Sale</a> -->
                                    <br>
                                    <a href="{{ URL('privacy_policy') }}" class="mr-3">Privacy Policy</a>
                                    <!-- <a href="#" class="mr-3">Consumer Rights</a> -->
                                    <a href="{{ URL('https://abiyat.online/') }}" class="mr-3">Careers</a>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                @endif

                


                <div class="navbar bottom_navbar">
                    <a href="{{ route('front.categories') }}"><img
                            src="{{ asset('public_assets/images/category-icon.svg')}}" class="bottom-icon"
                            alt="">Categories</a>
                    <a href="{{route('home')}}" class=""><img src="{{ asset('public_assets/images/chat-icon.svg')}}"
                            class="bottom-icon" alt="">Chat</a>
                    <a href="{{route('home')}}" class="active bottom-logo"><img
                            src="{{ asset('public_assets/images/abiyat-favicon.png')}}" alt=""></a>
                    <!-- <i class="las la-tags"></i>  -->
                    <a href="javascript:;" id="myaccclick" style="margin-left: 57px;"><img
                            src="{{ asset('public_assets/images/account-icon.svg')}}" class="bottom-icon" alt="">My
                        Account</a>
                    <a href="{{route('home')}}" class="active bottom-logo"><img
                            src="{{ asset('public_assets/images/abiyat-favicon.png')}}" alt=""></a>
                    <a href="{{ route('cart.index') }}">@if($cartCount != 0 ) <div class="cart_count">
                            <span>{{ $cartCount }}</span>
                        </div> @endif <img src="{{ asset('public_assets/images/cart-icon.svg')}}" class="bottom-icon"
                            alt="">Cart</a>
                </div>
            </div>

        </header>

        @yield('content')

        <div class="mob-botm-copyright">
            <div class="row mt-5 mb-3">
                <div class="col-md-6 mb-3">
                    <div class="text-center">

                        <h5 class="h5 mb-2">SHOP ON THE GO</h5>
                        <a href="#">
                            <img src="{{ asset('public_assets/images/google-play.svg')}}" width="100"
                                height="50" alt="" title="">
                        </a>
                        <a href="#"><img src="{{asset('public_assets/images/app-store.svg')}}" width="100"
                                height="50" alt="" title=""></a>

                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="text-center sociallinks">
                        <h5 class="h5 mb-3">CONNECT WITH US</h5>
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 text-center">
                        <p>© 2020 <a href="#">abiyat.com</a> All Rights Reserved</p>
                    </div>
                    <div class="col-md-3 text-center">
                        <img src="{{asset('public_assets/images/card.png')}}" alt="" title="" width="150">
                    </div>
                    <div class="col-md-6 text-center mt-3">
                        <!-- <a href="#" class="mr-3">Careers</a>-->
                        <a href="{{ URL('return_policy') }}" class="mr-3">Return Policy</a>
                        <!-- <a href="#" class="mr-3">Sell with us</a>  -->
                        <a href="{{ URL('terms_condition') }}" class="mr-3">Terms of Use</a>
                        <!-- <a href="#" class="mr-3">Terms of Sale</a> -->
                        <br>
                        <a href="{{ URL('privacy_policy') }}" class="mr-3">Privacy Policy</a>
                        <!-- <a href="#" class="mr-3">Consumer Rights</a> -->
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('layouts.public.footer')
    @stack('scripts')



    <script type="text/javascript">
    !(function(n, i, e, a) {
        (n.navigation = function(t, s) {
            var o = {
                    responsive: !0,
                    mobileBreakpoint: 991,
                    showDuration: 200,
                    hideDuration: 200,
                    showDelayDuration: 0,
                    hideDelayDuration: 0,
                    submenuTrigger: "hover",
                    effect: "fade",
                    submenuIndicator: !0,
                    submenuIndicatorTrigger: !1,
                    hideSubWhenGoOut: !0,
                    visibleSubmenusOnMobile: !1,
                    fixed: !1,
                    overlay: !0,
                    overlayColor: "rgba(0, 0, 0, 0.5)",
                    hidden: !1,
                    hiddenOnMobile: !1,
                    offCanvasSide: "left",
                    offCanvasCloseButton: !0,
                    animationOnShow: "",
                    animationOnHide: "",
                    onInit: function() {},
                    onLandscape: function() {},
                    onPortrait: function() {},
                    onShowOffCanvas: function() {},
                    onHideOffCanvas: function() {}
                },
                r = this,
                u = Number.MAX_VALUE,
                d = 1,
                l = "click.nav touchstart.nav",
                f = "mouseenter focusin",
                c = "mouseleave focusout";
            r.settings = {};
            var t = (n(t), t);
            n(t).find(".nav-search").length > 0 &&
                n(t)
                .find(".nav-search")
                .find("form")
                .prepend(
                    "<span class='nav-search-close-button' tabindex='0'>&#10005;</span>"
                ),
                (r.init = function() {
                    (r.settings = n.extend({}, o, s)),
                    r.settings.offCanvasCloseButton &&
                        n(t)
                        .find(".nav-menus-wrapper")
                        .prepend(
                            "<span class='nav-menus-wrapper-close-button'>&#10005;</span>"
                        ),
                        "right" == r.settings.offCanvasSide &&
                        n(t)
                        .find(".nav-menus-wrapper")
                        .addClass("nav-menus-wrapper-right"),
                        r.settings.hidden &&
                        (n(t).addClass("navigation-hidden"),
                            (r.settings.mobileBreakpoint = 99999)),
                        v(),
                        r.settings.fixed && n(t).addClass("navigation-fixed"),
                        n(t)
                        .find(".nav-toggle")
                        .on("click touchstart", function(n) {
                            n.stopPropagation(),
                                n.preventDefault(),
                                r.showOffcanvas(),
                                s !== a && r.callback("onShowOffCanvas");
                        }),
                        n(t)
                        .find(".nav-menus-wrapper-close-button")
                        .on("click touchstart", function() {
                            r.hideOffcanvas(), s !== a && r.callback("onHideOffCanvas");
                        }),
                        n(t)
                        .find(".nav-search-button, .nav-search-close-button")
                        .on("click touchstart keydown", function(i) {
                            i.stopPropagation(), i.preventDefault();
                            var e = i.keyCode || i.which;
                            "click" === i.type ||
                                "touchstart" === i.type ||
                                ("keydown" === i.type && 13 == e) ?
                                r.toggleSearch() :
                                9 == e && n(i.target).blur();
                        }),
                        n(t).find(".megamenu-tabs").length > 0 && y(),
                        n(i).resize(function() {
                            r.initNavigationMode(C()), O(), r.settings.hiddenOnMobile && m();
                        }),
                        r.initNavigationMode(C()),
                        r.settings.hiddenOnMobile && m(),
                        s !== a && r.callback("onInit");
                });
            var h = function() {
                    n(t)
                        .find(".nav-submenu")
                        .hide(0),
                        n(t)
                        .find("li")
                        .removeClass("focus");
                },
                v = function() {
                    n(t)
                        .find("li")
                        .each(function() {
                            n(this).children(".nav-dropdown,.megamenu-panel").length > 0 &&
                                (n(this)
                                    .children(".nav-dropdown,.megamenu-panel")
                                    .addClass("nav-submenu"),
                                    r.settings.submenuIndicator &&
                                    n(this)
                                    .children("a")
                                    .append(
                                        "<span class='submenu-indicator'><span class='submenu-indicator-chevron'></span></span>"
                                    ));
                        });
                },
                m = function() {
                    n(t).hasClass("navigation-portrait") ?
                        n(t).addClass("navigation-hidden") :
                        n(t).removeClass("navigation-hidden");
                };
            (r.showSubmenu = function(i, e) {
                C() > r.settings.mobileBreakpoint &&
                    n(t)
                    .find(".nav-search")
                    .find("form")
                    .fadeOut(),
                    "fade" == e ?
                    n(i)
                    .children(".nav-submenu")
                    .stop(!0, !0)
                    .delay(r.settings.showDelayDuration)
                    .fadeIn(r.settings.showDuration)
                    .removeClass(r.settings.animationOnHide)
                    .addClass(r.settings.animationOnShow) :
                    n(i)
                    .children(".nav-submenu")
                    .stop(!0, !0)
                    .delay(r.settings.showDelayDuration)
                    .slideDown(r.settings.showDuration)
                    .removeClass(r.settings.animationOnHide)
                    .addClass(r.settings.animationOnShow),
                    n(i).addClass("focus");
            }),
            (r.hideSubmenu = function(i, e) {
                "fade" == e
                    ?
                    n(i)
                    .find(".nav-submenu")
                    .stop(!0, !0)
                    .delay(r.settings.hideDelayDuration)
                    .fadeOut(r.settings.hideDuration)
                    .removeClass(r.settings.animationOnShow)
                    .addClass(r.settings.animationOnHide) :
                    n(i)
                    .find(".nav-submenu")
                    .stop(!0, !0)
                    .delay(r.settings.hideDelayDuration)
                    .slideUp(r.settings.hideDuration)
                    .removeClass(r.settings.animationOnShow)
                    .addClass(r.settings.animationOnHide),
                    n(i)
                    .removeClass("focus")
                    .find(".focus")
                    .removeClass("focus");
            });
            var p = function() {
                    n("body").addClass("no-scroll"),
                        r.settings.overlay &&
                        (n(t).append("<div class='nav-overlay-panel'></div>"),
                            n(t)
                            .find(".nav-overlay-panel")
                            .css("background-color", r.settings.overlayColor)
                            .fadeIn(300)
                            .on("click touchstart", function(n) {
                                r.hideOffcanvas();
                            }));
                },
                g = function() {
                    n("body").removeClass("no-scroll"),
                        r.settings.overlay &&
                        n(t)
                        .find(".nav-overlay-panel")
                        .fadeOut(400, function() {
                            n(this).remove();
                        });
                };
            (r.showOffcanvas = function() {
                p(),
                    "left" == r.settings.offCanvasSide ?
                    n(t)
                    .find(".nav-menus-wrapper")
                    .css("transition-property", "left")
                    .addClass("nav-menus-wrapper-open") :
                    n(t)
                    .find(".nav-menus-wrapper")
                    .css("transition-property", "right")
                    .addClass("nav-menus-wrapper-open");
            }),
            (r.hideOffcanvas = function() {
                n(t)
                    .find(".nav-menus-wrapper")
                    .removeClass("nav-menus-wrapper-open")
                    .on(
                        "webkitTransitionEnd moztransitionend transitionend oTransitionEnd",
                        function() {
                            n(t)
                                .find(".nav-menus-wrapper")
                                .css("transition-property", "none")
                                .off();
                        }
                    ),
                    g();
            }),
            (r.toggleOffcanvas = function() {
                C() <= r.settings.mobileBreakpoint &&
                    (n(t)
                        .find(".nav-menus-wrapper")
                        .hasClass("nav-menus-wrapper-open") ?
                        (r.hideOffcanvas(), s !== a && r.callback("onHideOffCanvas")) :
                        (r.showOffcanvas(), s !== a && r.callback("onShowOffCanvas")));
            }),
            (r.toggleSearch = function() {
                "none" ==
                n(t)
                    .find(".nav-search")
                    .find("form")
                    .css("display") ?
                    (n(t)
                        .find(".nav-search")
                        .find("form")
                        .fadeIn(200),
                        n(t)
                        .find(".nav-search")
                        .find("input")
                        .focus()) :
                    (n(t)
                        .find(".nav-search")
                        .find("form")
                        .fadeOut(200),
                        n(t)
                        .find(".nav-search")
                        .find("input")
                        .blur());
            }),
            (r.initNavigationMode = function(i) {
                r.settings.responsive ?
                    (i <= r.settings.mobileBreakpoint &&
                        u > r.settings.mobileBreakpoint &&
                        (n(t)
                            .addClass("navigation-portrait")
                            .removeClass("navigation-landscape"),
                            S(),
                            s !== a && r.callback("onPortrait")),
                        i > r.settings.mobileBreakpoint &&
                        d <= r.settings.mobileBreakpoint &&
                        (n(t)
                            .addClass("navigation-landscape")
                            .removeClass("navigation-portrait"),
                            k(),
                            g(),
                            r.hideOffcanvas(),
                            s !== a && r.callback("onLandscape")),
                        (u = i),
                        (d = i)) :
                    (n(t).addClass("navigation-landscape"),
                        k(),
                        s !== a && r.callback("onLandscape"));
            });
            var b = function() {
                    n("html").on("click.body touchstart.body", function(i) {
                        0 === n(i.target).closest(".navigation").length &&
                            (n(t)
                                .find(".nav-submenu")
                                .fadeOut(),
                                n(t)
                                .find(".focus")
                                .removeClass("focus"),
                                n(t)
                                .find(".nav-search")
                                .find("form")
                                .fadeOut());
                    });
                },
                C = function() {
                    return (
                        i.innerWidth || e.documentElement.clientWidth || e.body.clientWidth
                    );
                },
                w = function() {
                    n(t)
                        .find(".nav-menu")
                        .find("li, a")
                        .off(l)
                        .off(f)
                        .off(c);
                },
                O = function() {
                    if (C() > r.settings.mobileBreakpoint) {
                        var i = n(t).outerWidth(!0);
                        n(t)
                            .find(".nav-menu")
                            .children("li")
                            .children(".nav-submenu")
                            .each(function() {
                                n(this)
                                    .parent()
                                    .position().left +
                                    n(this).outerWidth() >
                                    i ?
                                    n(this).css("right", 0) :
                                    n(this).css("right", "auto");
                            });
                    }
                },
                y = function() {
                    function i(i) {
                        var e = n(i)
                            .children(".megamenu-tabs-nav")
                            .children("li"),
                            a = n(i).children(".megamenu-tabs-pane");
                        n(e).on("click.tabs touchstart.tabs", function(i) {
                            i.stopPropagation(),
                                i.preventDefault(),
                                n(e).removeClass("active"),
                                n(this).addClass("active"),
                                n(a)
                                .hide(0)
                                .removeClass("active"),
                                n(a[n(this).index()])
                                .show(0)
                                .addClass("active");
                        });
                    }
                    if (n(t).find(".megamenu-tabs").length > 0)
                        for (var e = n(t).find(".megamenu-tabs"), a = 0; a < e.length; a++)
                            i(e[a]);
                },
                k = function() {
                    w(),
                        h(),
                        navigator.userAgent.match(/Mobi/i) ||
                        navigator.maxTouchPoints > 0 ||
                        "click" == r.settings.submenuTrigger ?
                        n(t)
                        .find(".nav-menu, .nav-dropdown")
                        .children("li")
                        .children("a")
                        .on(l, function(e) {
                            if (
                                (r.hideSubmenu(
                                        n(this)
                                        .parent("li")
                                        .siblings("li"),
                                        r.settings.effect
                                    ),
                                    n(this)
                                    .closest(".nav-menu")
                                    .siblings(".nav-menu")
                                    .find(".nav-submenu")
                                    .fadeOut(r.settings.hideDuration),
                                    n(this).siblings(".nav-submenu").length > 0)
                            ) {
                                if (
                                    (e.stopPropagation(),
                                        e.preventDefault(),
                                        "none" ==
                                        n(this)
                                        .siblings(".nav-submenu")
                                        .css("display"))
                                )
                                    return (
                                        r.showSubmenu(n(this).parent("li"), r.settings.effect),
                                        O(),
                                        !1
                                    );
                                if (
                                    (r.hideSubmenu(n(this).parent("li"), r.settings.effect),
                                        "_blank" === n(this).attr("target") ||
                                        "blank" === n(this).attr("target"))
                                )
                                    i.open(n(this).attr("href"));
                                else {
                                    if (
                                        "#" === n(this).attr("href") ||
                                        "" === n(this).attr("href") ||
                                        "javascript:void(0)" === n(this).attr("href")
                                    )
                                        return !1;
                                    i.location.href = n(this).attr("href");
                                }
                            }
                        }) :
                        n(t)
                        .find(".nav-menu")
                        .find("li")
                        .on(f, function() {
                            r.showSubmenu(this, r.settings.effect), O();
                        })
                        .on(c, function() {
                            r.hideSubmenu(this, r.settings.effect);
                        }),
                        r.settings.hideSubWhenGoOut && b();
                },
                S = function() {
                    w(),
                        h(),
                        r.settings.visibleSubmenusOnMobile ?
                        n(t)
                        .find(".nav-submenu")
                        .show(0) :
                        (n(t)
                            .find(".submenu-indicator")
                            .removeClass("submenu-indicator-up"),
                            r.settings.submenuIndicator && r.settings.submenuIndicatorTrigger ?
                            n(t)
                            .find(".submenu-indicator")
                            .on(l, function(i) {
                                return (
                                    i.stopPropagation(),
                                    i.preventDefault(),
                                    r.hideSubmenu(
                                        n(this)
                                        .parent("a")
                                        .parent("li")
                                        .siblings("li"),
                                        "slide"
                                    ),
                                    r.hideSubmenu(
                                        n(this)
                                        .closest(".nav-menu")
                                        .siblings(".nav-menu")
                                        .children("li"),
                                        "slide"
                                    ),
                                    "none" ==
                                    n(this)
                                    .parent("a")
                                    .siblings(".nav-submenu")
                                    .css("display") ?
                                    (n(this).addClass("submenu-indicator-up"),
                                        n(this)
                                        .parent("a")
                                        .parent("li")
                                        .siblings("li")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        n(this)
                                        .closest(".nav-menu")
                                        .siblings(".nav-menu")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        r.showSubmenu(
                                            n(this)
                                            .parent("a")
                                            .parent("li"),
                                            "slide"
                                        ),
                                        !1) :
                                    (n(this)
                                        .parent("a")
                                        .parent("li")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        void r.hideSubmenu(
                                            n(this)
                                            .parent("a")
                                            .parent("li"),
                                            "slide"
                                        ))
                                );
                            }) :
                            n(t)
                            .find(".nav-menu, .nav-dropdown")
                            .children("li")
                            .children("a")
                            .on(l, function(e) {
                                if (
                                    (e.stopPropagation(),
                                        e.preventDefault(),
                                        r.hideSubmenu(
                                            n(this)
                                            .parent("li")
                                            .siblings("li"),
                                            r.settings.effect
                                        ),
                                        r.hideSubmenu(
                                            n(this)
                                            .closest(".nav-menu")
                                            .siblings(".nav-menu")
                                            .children("li"),
                                            "slide"
                                        ),
                                        "none" ==
                                        n(this)
                                        .siblings(".nav-submenu")
                                        .css("display"))
                                )
                                    return (
                                        n(this)
                                        .children(".submenu-indicator")
                                        .addClass("submenu-indicator-up"),
                                        n(this)
                                        .parent("li")
                                        .siblings("li")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        n(this)
                                        .closest(".nav-menu")
                                        .siblings(".nav-menu")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        r.showSubmenu(n(this).parent("li"), "slide"),
                                        !1
                                    );
                                if (
                                    (n(this)
                                        .parent("li")
                                        .find(".submenu-indicator")
                                        .removeClass("submenu-indicator-up"),
                                        r.hideSubmenu(n(this).parent("li"), "slide"),
                                        "_blank" === n(this).attr("target") ||
                                        "blank" === n(this).attr("target"))
                                )
                                    i.open(n(this).attr("href"));
                                else {
                                    if (
                                        "#" === n(this).attr("href") ||
                                        "" === n(this).attr("href") ||
                                        "javascript:void(0)" === n(this).attr("href")
                                    )
                                        return !1;
                                    i.location.href = n(this).attr("href");
                                }
                            }));
                };
            (r.callback = function(n) {
                s[n] !== a && s[n].call(t);
            }),
            r.init();
        }),
        (n.fn.navigation = function(i) {
            return this.each(function() {
                if (a === n(this).data("navigation")) {
                    var e = new n.navigation(this, i);
                    n(this).data("navigation", e);
                }
            });
        });
    })(jQuery, window, document);



    (function($) {
        'use strict';

        var $window = $(window);

        if ($.fn.navigation) {
            $("#navigation1").navigation();
            $("#always-hidden-nav").navigation({
                hidden: true
            });
        }

        $window.on('load', function() {
            $('#preloader').fadeOut('slow', function() {
                $(this).remove();
            });
        });

    })(jQuery);
    </script>

    <script>
    $(document).ready(function() {
        "use strict";

        var menuActive = false;
        var header = $('.header');
        setHeader();
        initCustomDropdown();
        initPageMenu();

        function setHeader() {

            if (window.innerWidth > 991 && menuActive) {
                closeMenu();
            }
        }

        function initCustomDropdown() {
            if ($('.custom_dropdown_placeholder').length && $('.custom_list').length) {
                var placeholder = $('.custom_dropdown_placeholder');
                var list = $('.custom_list');
            }

            placeholder.on('click', function(ev) {
                if (list.hasClass('active')) {
                    list.removeClass('active');
                } else {
                    list.addClass('active');
                }

                $(document).one('click', function closeForm(e) {
                    if ($(e.target).hasClass('clc')) {
                        $(document).one('click', closeForm);
                    } else {
                        list.removeClass('active');
                    }
                });

            });

            $('.custom_list a').on('click', function(ev) {
                ev.preventDefault();
                var index = $(this).parent().index();

                placeholder.text($(this).text()).css('opacity', '1');

                if (list.hasClass('active')) {
                    list.removeClass('active');
                } else {
                    list.addClass('active');
                }
            });


            $('select').on('change', function(e) {
                placeholder.text(this.value);

                $(this).animate({
                    width: placeholder.width() + 'px'
                });
            });
        }

        /*

        4. Init Page Menu

        */

        function initPageMenu() {
            if ($('.page_menu').length && $('.page_menu_content').length) {
                var menu = $('.page_menu');
                var menuContent = $('.page_menu_content');
                var menuTrigger = $('.menu_trigger');

                //Open / close page menu
                menuTrigger.on('click', function() {
                    if (!menuActive) {
                        openMenu();
                        return false;
                    } else {
                        closeMenu();
                        return false;
                    }
                });

                //Handle page menu
                if ($('.page_menu_item').length) {
                    var items = $('.page_menu_item');
                    items.each(function() {
                        var item = $(this);
                        if (item.hasClass("has-children")) {
                            item.on('click', function(evt) {
                                evt.preventDefault();
                                evt.stopPropagation();
                                var subItem = item.find('> ul');
                                if (subItem.hasClass('active')) {
                                    subItem.toggleClass('active');
                                    TweenMax.to(subItem, 0.3, {
                                        height: 0
                                    });
                                } else {
                                    subItem.toggleClass('active');
                                    TweenMax.set(subItem, {
                                        height: "auto"
                                    });
                                    TweenMax.from(subItem, 0.3, {
                                        height: 0
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }

        function openMenu() {
            var menu = $('.page_menu');
            var menuContent = $('.page_menu_content');
            TweenMax.set(menuContent, {
                height: "auto"
            });
            TweenMax.from(menuContent, 0.3, {
                height: 0
            });
            menuActive = true;
        }

        function closeMenu() {
            var menu = $('.page_menu');
            var menuContent = $('.page_menu_content');
            TweenMax.to(menuContent, 0.3, {
                height: 0
            });
            menuActive = false;
        }

        $("#recommended-for-you,#recommended-for-you-2,#recommended-for-you-3").owlCarousel({
            items:7,
            itemsDesktop: [1199, 6],
            itemsDesktopSmall : [980,4],
            itemsTablet: [768, 4],
            itemsMobile: [479, 2],
            // itemsTabletSmall: false,
            pagination: false,
            navigation: true,
            navigationText: ['<i class="fa fa-chevron-left"></i>',
                '<i class="fa fa-chevron-right"></i>'
            ],
            autoPlay: false,
            stopOnHover: true,
        });


    });

    $('.menu_trigger').click(function() {
        $('.page_menu_content').addClass('showDiv');
    });


    $('.hidePanle').click(function() {
        $('.page_menu_content').removeClass('showDiv');
    });
    </script>
</body>

</html>