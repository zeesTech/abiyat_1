$(document).ready(function () {
    $('#is_free').on('change', function () {
        console.log($(this).val());
        if ($(this).val() == 0) {
            $('#delivery_cost').fadeIn();
        } else {
            $('#delivery_cost').fadeOut();
        }
    });
    $('.select2').select2({
        placeholder: 'Select'
    });
    $('.table').DataTable({
        'info' : false,
        'paging' : false,
        'searching' : false,
        'columnDefs' : [
            {
                'orderable': false, 'targets' : -1
            }
        ],
        'sorting' : []
    });


    $('#discount,#price').on("change",function(){
        discount = $('#discount').val();
        if(discount >= 0 && discount <= 100){
        price = $('#price').val();
        console.log(discount);    
        now_price = (price * discount) / 100;
        console.log(now_price);
        $('#sale_price').val(price - now_price);
        }else{
            alert('Discount percentage between 1 to 100');
        }
    });
});
