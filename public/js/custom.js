$(document).ready(function () {
    $("#brand-logo").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items: 6,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [979, 6]
    });

    $('.select2').select2();

    if ($('#thumbnails li img').length > 0) {
        $('#thumbnails li img').on('click', function () {
            $('#main-image')
                .attr('src', $(this).attr('src') +'?w=400')
                .attr('data-zoom', $(this).attr('src') +'?w=1200');
        });
    }

    $(".img-orderDetail").mouseover(function() {
      $(this).css({ width: '150px', height: '150px' });
    }).mouseout(function() {
      $(".img-orderDetail").css({ width: '50px', height: '50px'});
    });

    $("#myaccclick").click(function () {
      $(".hidme").toggleClass("showme");
  $(".fixednot").toggleClass(".fixednow");
  });
  

  initMap();
});

function confirm_location()
{
    var address1 = $('#address_1').val();
    if(!address1)
    {
        alert('select location')
    }else{
          $("#showAddressText").text(address1);
          $("#addressMap").hide();
          $("#addressBox").show(); 
    }

}

    var map;
    var geocoder;   

    function initMap() {
        var input = document.getElementById('address_1');
        const autocomplete =  new google.maps.places.Autocomplete(input);
        
        var mapOptions = { center: new google.maps.LatLng(0.0, 0.0), zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP };
        var myOptions = {
                center: new google.maps.LatLng(36.835769, 10.247693 ),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
 
            geocoder = new google.maps.Geocoder();
            var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
                autocomplete.bindTo("bounds", map);
                const infowindow = new google.maps.InfoWindow();
                
                autocomplete.addListener("place_changed", () => {
                 //infowindow.close();
                    const marker = new google.maps.Marker({
                    map,
                    anchorPoint: new google.maps.Point(0, -29),
                    });
                    const place = autocomplete.getPlace();

                    if (!place.geometry || !place.geometry.location) {
                    
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                    }
                
                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                    } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                    }
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);
                
                    //infowindow.open(map, marker);
                });

                infoWindow = new google.maps.InfoWindow();
                 const locationButton = document.getElementById("locationButton");
        //   locationButton.textContent = "Pan to Current Location";
        //   locationButton.classList.add("custom-map-control-button");
        //   map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);

                locationButton.addEventListener("click", () => {
        //map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
                const marker = new google.maps.Marker({
                            map,
                            anchorPoint: new google.maps.Point(0, -29),
                        });


                        if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            infoWindow.setPosition(pos);
                            //infoWindow.setContent("Location found.");
                            marker.setPosition(pos);
                                marker.setVisible(true);
                            // infoWindow.open(map);
                            //map.setCenter(pos);

                            map.setCenter(pos);
                            map.setZoom(17);
                            placeMarker(pos);
                            //console.log(positionlatLng)
                            },
                            () => {
                            handleLocationError(true, infoWindow, map.getCenter());
                            }
                        );
                        } else {
                        // Browser doesn't support Geolocation
                        handleLocationError(false, infoWindow, map.getCenter());
                        }
                    });
                
            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
            });

            var marker;
            



        function placeMarker(location) {
            if(marker){ 
                marker.setPosition(location); 
            }else{
                marker = new google.maps.Marker({
                    position: location, 
                    map: map
                });
            }
            //document.getElementById('lat').value=location.lat();
            //document.getElementById('lng').value=location.lng();
            
            getAddress(location);
            
        }

      function getAddress(latLng) {
        geocoder.geocode( {'latLng': latLng},
          function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
              if(results[0]) {
                document.getElementById("address_1").value = results[0].formatted_address;
              }
              else {
                document.getElementById("address_1").value = "No results";
              }
            }
            else {
              document.getElementById("address_1").value = status;
            }
          });
        }

        google.maps.event.addDomListener(window, 'load', initMap);
      }
      