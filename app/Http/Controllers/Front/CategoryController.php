<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     * @var BrandRepositoryInterface
     */
    private $categoryRepo;
    private $brandRepo;


    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository,BrandRepositoryInterface $brandRepository)
    {
        $this->categoryRepo = $categoryRepository;
        $this->brandRepo = $brandRepository;
    }

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getCategory(string $slug)
    {
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $repo = new CategoryRepository($category);

        $products = $repo->findProducts()->where('status', 1)->all();

        $cat_list = $this->categoryRepo->listCategories('name','ASC');
        $brand_list = $this->brandRepo->listBrands(['*'],'name','asc');
        
        //dd($cat_list);

        // foreach($cat_list as $cat){
        //     // print_r($cat->children());
        //    echo $cat['name'];echo '<br>';
        // }
        // die;
        return view('public.categories.category', [
            'category' => $category,
            'products' => $repo->paginateArrayResults($products, 20),
            'categories' => $cat_list,
            'brands' => $brand_list
        ]);
    }

    public function showAllCategories(){
        $category = Category::where('slug', 'appliances')->first();
        
        //dd(CategoryResource::collection($category));
        $cat_list = $this->categoryRepo->rootCategories('name','ASC');
        //dd($cat_list);
        return view('public.categories.categories_mobile',['cat_list' => $cat_list]);
    }

    public function getSubCategories(string $slug){
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);
        //dd($category->id);
        $child_categories = Category::where('parent_id',$category->id)->get();
        dd($child_categories);
        //return view('public.categories.categories_mobile',['cat_list' => $cat_list,'child_categories' => $child_categories]);  
    }
}
