<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;

class HomeController
{
    /**
     * @var CategoryRepositoryInterface
     * @var ProductRepositoryInterface
     */
    private $categoryRepo;
    private $productRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository
        )
    {
        $this->categoryRepo = $categoryRepository;
        $this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        
        $cat_list1 = $this->categoryRepo->listCategoriesShowHome();
        
        foreach ($cat_list1 as $key => $value) {
            $repo1 = new CategoryRepository($value);
            $pro1 = $repo1->findProducts()->where('status', 1)->all();
            $cat_list1[$key]['products'] = $pro1;
        }
        $products = $this->productRepo->listProducts();
        $cat_list = $this->categoryRepo->rootCategories('name','ASC');
        // dd($pro1);
        //return view('front.index', compact('cat1', 'cat2'));
        return view('public.index', compact('cat_list','cat_list1','products'));
    }
    public function sendSMS()
    {
    $numbers = $_GET['phone'];
    $code = rand(1000,9999);
    $username = "sakibtanoy";
    $hash = "f61094cc3ed7b825e8d9b75e96c8de12"; 
    // $numbers = "+919823132111"; //Recipient Phone Number multiple number must be separated by comma
    $message = "Here is the verification code : " . $code;


    $params = array('app'=>'ws', 'u'=>$username, 'h'=>$hash, 'op'=>'pv', 'unicode'=>'1','to'=>$numbers, 'msg'=>$message);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://alphasms.biz/index.php?".http_build_query($params, "", "&"));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "Accept:application/json"));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $response = curl_exec($ch);
    curl_close ($ch);
    return json_encode($code); 
    // dd($response);
    }
}
