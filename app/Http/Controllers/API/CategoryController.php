<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Shop\Categories\Category;
use Validator;
use App\Http\Resources\Category as CategoryResource;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryController extends BaseController
{
    private $categoryRepo;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
    
        return $this->sendResponse(CategoryResource::collection($categories), 'Category retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->first();
  
        if (is_null($category)) {
            return $this->sendError('category not found.');
        }
   
        return $this->sendResponse(new CategoryResource($category), 'category retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
     
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

    }

    public function parent_categories()
    {
        $cat_list = $this->categoryRepo->rootCategories('name','ASC');
        //$categories = Category::all();
      
        
        return $this->sendResponse(new CategoryResource($cat_list), 'category retrieved successfully.');

    }

    

    public function category_products($slug)
    {
   
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);
        $repo = new CategoryRepository($category);
        $products = $repo->findProducts()->where('status', 1)->all();
        return $this->sendResponse(new CategoryResource($products), 'category retrieved successfully.');

    }

    public function category_home()
    {
        $home_categories = array();
        $categories = $this->categoryRepo->listCategoriesShowHome();
        
        foreach($categories as $category){
            $repo1 = new CategoryRepository($category);
            $pro1 = $repo1->findProducts()->where('status', 1)->all();
            $home_categories[] = array('id' => $category->id, 
            'id' => $category->id, 
            'name' => $category->name, 
            'description' => $category->description, 
            'cover' => $category->cover, 
            'products' => $pro1);
        }

        
        // foreach ($cat_list1 as $key => $value) {
        //     $repo1 = new CategoryRepository($value);
        //     $pro1 = $repo1->findProducts()->where('status', 1)->all();
        //     $cat_list1[$key]['products'] = $pro1;
        // }
        return $this->sendResponse(new CategoryResource($home_categories), 'category retrieved successfully.');

    }

   
    public function child_categories($slug)
    {
   
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);
        $repo = new CategoryRepository($category);
        $childs = $repo->findChildren()->where('status', 1)->all();
        //$products = $repo->findProducts()->where('status', 1)->all();
        return $this->sendResponse(new CategoryResource($childs), 'category retrieved successfully.');

    }
}