<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
      
        if($message == 'Unauthorised.' || $message == 'Updated.' || $message == 'Deleted.'){
            $success = true;
      
        
    	$response = [
            'success' => $success,
            'data'    => [],
            'message' => $message,
        ];


        return response()->json($response, 200,[], JSON_FORCE_OBJECT);
       
    }else{
            $success = true;
            $response = [
                'success' => $success,
                'data'    => $result,
                'message' => $message,
            ];
    
            return response()->json($response, 200);

        }
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
            'data' => $errorMessages
        ];


        $response = json_encode($response,JSON_FORCE_OBJECT);
        return response($response, $code);
    }
}