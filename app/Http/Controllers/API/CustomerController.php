<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Shop\Customers\Customer;
use App\Http\Resources\Customer as CustomerResource;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Customers\Exceptions\CustomerNotFoundException;


class CustomerController extends BaseController
{
    private $customerRepo;

    public function __construct(CustomerRepositoryInterface $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    public function changephone(Request $request, $id)
    {   
        
        Customer::where('id', $id)->update(['phone' => $request['phone']]);
        $object = json_encode(json_decode("{}"));
        return $this->sendResponse($object, 'Updated.');


     
    }



}