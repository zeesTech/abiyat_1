<?php

namespace App\Http\Controllers\API;

use App\Shop\Cart\Requests\CartCheckoutRequest;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Products\Product;


use App\Shop\Orders\Order;

use App\Shop\Checkout\CheckoutRepository;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use Exception;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;




class CheckoutController extends BaseController
{


    use ProductTransformable;



    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    private $shippingFee = 0;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepository,
        OrderRepositoryInterface $orderRepository
    ) {

        $this->customerRepo = $customerRepository;
        $this->productRepo = $productRepository;
        $this->orderRepo = $orderRepository;
    }



    public function cod(Request $request)
    {
        //dd($request); 

        // $products = '{"products": [{"id": 1,"quantity": 3}, {"id": 2,"quantity": 5}]}';

        $checkoutRepo = new CheckoutRepository;
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus);
        $os = $orderStatusRepo->findByName('ordered');

        //comma seperated items & qty
        $product_ids = $request->input('product_ids');
        $product_qty = $request->input('product_qty');
        
        
        // $product_ids = '1';
        // $product_qty = '3';

        $ids = explode(",", $product_ids);
        $qty = explode(",", $product_qty);

    
        if(Product::whereIn('id', $ids)->exists()){
         
        
        $products = Product::whereIn('id', $ids)->get();
        //print_r($products); die;  
        $subtotal = 0;
        //dd($products);die;
        //dd($qty);
        foreach ($products as $key => $product) {

            //$subtotal += number_format($product->price, 2)*$qty[$key];
            $subtotal += $product->price * $qty[$key];
        }
        
        $order = new Order;
        $order->reference = Str::uuid()->toString();
        $order->courier_id = 1; // @deprecated
        $order->customer_id = $request->input('customer_id');
        //$order->customer_id = 2;
        $order->address_id = $request->input('address_id');
        //$order->address_id = 1;
        $order->order_status_id = $os->id;
        $order->payment = strtolower('Cash on Delivery');
        $order->discounts =  $request->input('total_discount');
        $order->total_products = $subtotal;
        $order->total_shipping = $this->shippingFee;
        //$order->total_shipping = 1;
        //$order->tax = 1;
        $order->total_paid = $request->input('total_paid');
        $order->tax = $request->input('tax');
        $order->total = $subtotal+$request->input('tax')+ $this->shippingFee - $request->input('total_discount') - $request->input('total_paid');
        //$order->total = 0;
        
        $order->save();


        foreach ($products as $key => $product) {

            $quantity = $qty[$key];
            $order->products()->attach($product, [
                'quantity' => $quantity,
                'product_name' => $product->name,
                'product_sku' => $product->sku,
                'product_description' => $product->description,
                'product_price' => $product->price,
                'product_attribute_id' => isset($data['product_attribute_id']) ? $data['product_attribute_id'] : null,
            ]);
            $product->quantity = ($product->quantity - $quantity);
            $product->save();
        }


        $success['orderId'] =  $order->id;
        return $this->sendResponse($success, 'Ordered successfully.');
    }

    }
}
