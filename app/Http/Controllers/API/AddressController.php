<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Shop\Addresses\Address;
use App\Shop\Countries\Country;

use Validator;
use App\Http\Resources\Address as AddressResource;


class AddressController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_address($id)
    {
        $addresses = Address::where('customer_id', '=', $id)->get();
        return $this->sendResponse(AddressResource::collection($addresses), 'Addresses retrieved successfully.');
    }


    public function single($id)
    {
        $address = Address::where('id', '=', $id)->first();
        return $this->sendResponse(new AddressResource($address), 'Address retrieved successfully.');
    }

    public function add_address(Request $request)
    {
        $verification_code = rand(1000, 9999);
        $request->merge(['verification_code' => $verification_code]);

        $address =  Address::create($request->all());
        return $this->sendResponse(new AddressResource($address), 'Address created successfully.');
    }


    public function deleteit($customer_id, $id)
    {

        Address::where('id', '=', $id)->where('customer_id', '=', $customer_id)->delete();
        $object = json_encode(json_decode("{}"));
        return $this->sendResponse($object, 'Deleted.');

        //return $this->sendResponse(new AddressResource($address), 'Address deleted successfully.');
    }

    public function verify($id)
    {


        Address::where('id', $id)->update(['phone_verified' => 1]);

        $object = json_encode(json_decode("{}"));
        return $this->sendResponse($object, 'Updated.');
    }

    public function countries()
    {
        $countries = Country::all();
        return $this->sendResponse(AddressResource::collection($countries), 'Countries retrieved successfully.');
    }

    public function changephone(Request $request, $id)
    {   
        
        Address::where('id', $id)->update(['phone' => $request['phone'], 'phone_verified' => 0]);
        $object = json_encode(json_decode("{}"));
        return $this->sendResponse($object, 'Updated.');


     
    }

}
