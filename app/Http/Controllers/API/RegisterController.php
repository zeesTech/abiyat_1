<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Auth;
use Validator;
   
class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:customers',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        
   
        if($validator->fails()){
            $object = json_encode (json_decode ("{}"));
            return $this->sendResponse($object, 'Unauthorised.');
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = Customer::create($input);
        $success['id'] =  $user->id;
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        $success['email'] =  $user->email;
        $success['phone'] =  $user->phone;

        return $this->sendResponse($success, 'Customer register successfully.');
    }


    public function register_phone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:customers',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

       
        if($validator->fails()){
            $object = json_encode (json_decode ("{}")); 
            return $this->sendResponse($object, 'Unauthorised.');   
 
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = Customer::create($input);
        $success['id'] =  $user->id;

        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        $success['phone'] =  $user->phone;

        return $this->sendResponse($success, 'Customer register successfully.');
    }




    public function sms($number)
    {
        
    $code = rand(1000,9999);
    $username = "sakibtanoy";
    $hash = "f61094cc3ed7b825e8d9b75e96c8de12"; 
    // $numbers = "+919823132111"; //Recipient Phone Number multiple number must be separated by comma
    $message = "Here is the verification code : " . $code;


    $params = array('app'=>'ws', 'u'=>$username, 'h'=>$hash, 'op'=>'pv', 'unicode'=>'1','to'=>$number, 'msg'=>$message);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://alphasms.biz/index.php?".http_build_query($params, "", "&"));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "Accept:application/json"));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $response = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
        if ($err) {
           // echo "cURL Error #:" . $err;
           $success['error'] = $err;
        }else{
            $success['code'] =  $code;
        }
    
    return $this->sendResponse($success, 'OTP sent successfully.');

    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

                   
        // check whether login type is email or phone
        $loginType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
      
        // credentials array
        $credentials = array($loginType => $request['email'], 'password' => $request['password'], 'status'=> 1);

         if(Auth::attempt($credentials)){ 
             $user = Auth::user(); 
             $success['token'] =  $user->createToken('MyApp')->accessToken; 
             $success['name'] =  $user->name;
             $success['email'] =  $user->email;
             $success['phone'] =  $user->phone;
             return $this->sendResponse($success, 'User login successfully.');
         } 
        else{ 
        
                    $object = json_encode (json_decode ("{}"));    
                    return $this->sendResponse($object, 'Unauthorised.');
                
        } 
    }
}