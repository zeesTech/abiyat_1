<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Shop\Customers\Customer;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\API\BaseController as BaseController;



class LoginController extends BaseController
{

    /**
     * Login the admin
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
      
        $details = $request->only('email', 'password');
        //$details['status'] = 1;
        //dd($details);
        // check whether login type is email or phone
        $loginType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
    
        // credentials array
        $credentials = array($loginType => $details['email'], 'password' => $details['password'], 'status'=> 1);
       

        if(Auth::attempt($credentials)){ 
            $user = Auth::user(); 
            $success['id'] =  $user->id;
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['phone'] =  $user->phone;
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            $object = json_encode (json_decode ("{}"));
            return $this->sendResponse($object, 'Unauthorised.');
 
        } 
    }




}
