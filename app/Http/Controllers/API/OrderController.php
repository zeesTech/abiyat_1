<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Shop\Orders\Order;
use App\Http\Resources\Order as OrderResource;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Orders\Exceptions\OrderNotFoundException;


class OrderController extends BaseController
{
    private $orderRepo;

    public function __construct(OrderRepositoryInterface $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    public function customerOrders($id)
    {   
        
        $order = Order::where('customer_id', $id)->with(['products', 'customer', 'address', 'orderStatus'])->orderBy('id','DESC')->get();     
       return $this->sendResponse(new OrderResource($order), 'Orders retrieved successfully.');


     
    }

    public function findOrderById(int $id) : Order
    {
        try {
            $order = $this->orderRepo->findOneOrFail($id);
            $order['products'] = $order->products()->get();
            $order['customer'] = $order->customer()->get();
            $order['address'] = $order->address()->get();
            $order['orderStatus'] = $order->orderStatus()->get();
            return $order;
            //return $this->sendResponse(new OrderResource($order), 'Order retrieved successfully.');

        } catch (ModelNotFoundException $e) {
            throw new OrderNotFoundException($e);
        }
    }

}