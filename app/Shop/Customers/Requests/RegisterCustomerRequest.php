<?php

namespace App\Shop\Customers\Requests;

use App\Shop\Base\BaseFormRequest;

class RegisterCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->request->all()['register_with'] == 'phone'){
            session()->flash('phone', 'phone');    
            return [
                'name' => 'required|string|max:255',
                'phone' => 'required|string|max:20|unique:customers',
                'code' => 'required|string',
                'password' => 'required|string|min:6|confirmed',
            ];
        }else{
            session()->flash('email', 'email');
            return [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:customers',
                'password' => 'required|string|min:6|confirmed',
            ];
        }
    }
}
