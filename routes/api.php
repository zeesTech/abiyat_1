<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\AddressController;
use App\Http\Controllers\API\CheckoutController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);
Route::post('register-phone', [RegisterController::class, 'register_phone']);

Route::get('send-sms/{number}', [RegisterController::class, 'sms']);



//Route::post('login', [RegisterController::class, 'login']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('products', 'API\ProductController');
Route::get('recommended-products', 'API\ProductController@recommended_products');


Route::resource('categories', 'API\CategoryController');
Route::get('parent-categories', 'API\CategoryController@parent_categories');
Route::get('child-categories/{slug}', 'API\CategoryController@child_categories');


Route::get('category-products/{slug}', 'API\CategoryController@category_products');

Route::get('category-home', 'API\CategoryController@category_home');

Route::get('find-order-by-id/{id}', 'API\OrderController@findOrderById');
Route::get('customer-orders/{id}', 'API\OrderController@customerOrders');
Route::put('customer-change-phone/{id}', 'API\CustomerController@changephone');


Route::prefix('address')->group(function () {   
    Route::get('/customer/{id}', [AddressController::class, 'customer_address']);
    Route::get('/single/{id}', [AddressController::class, 'single']);
    Route::get('/delete/{cid}/{id}', [AddressController::class, 'deleteit']);
    Route::post('new', [AddressController::class, 'add_address']);
    Route::get('verify/{id}', [AddressController::class, 'verify']);
    Route::put('change-phone/{id}', [AddressController::class, 'changephone']);

});
Route::get('countries', [AddressController::class, 'countries']);

Route::post('cod-checkout', [CheckoutController::class, 'cod']);
